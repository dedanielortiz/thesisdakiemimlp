package randomizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import SynergyDemo3.EnergyMarket;

public class EnvironmentDay {
	
	private List<EnvironmentItem> items;
	
	public List<EnvironmentItem> getItems() {
		return items;
	}

	public EnvironmentDay(int timesteps, int sunrise, int sunset, int zone, EnergyMarket em, int day) {

		items = new LinkedList<>();
		Random r1 = new Random(day);
		int windVariance = 0;
		int sunVariance = 0;
		int cityVariance = 0;
		int windMean = 0;
		int sunMean = 0;
		int cityMean = 0;
		
		// Create 23 EnvironmentalItems, one per timestep.
		for(int i=0; i<timesteps; i++) {
			if(em.getContracts().isEmpty()) {
				int studyCase = 2;
				windMean = 0;
				sunMean = 0;
				cityMean = 0;
				windVariance = 0;
				if(studyCase == 2) {
					windVariance = 0;
				}
				//				 THIRD STUDY CASE
				if(studyCase == 3&&i==7&&zone == 2) {
					sunMean = -3000;
				}
				// FIRST STUDY CASE
				if(studyCase == 1&&i==5&&zone == 1) {
					windVariance = 500;
				}
//				// SECOND STUDY CASE
				if(studyCase == 2&&i==5&&zone == 1) {
					windMean = -1000;
				}
				
				// VARIANCE CASE STUDY
				if(studyCase == 5) {
						
					int mean;
					while((mean= (int) ((r1.nextDouble()-0.5)*30)) == 0);
					int variance = 0;
					windMean = mean;
					windVariance = variance;
					sunMean = mean;
					sunVariance = variance;
					cityMean = mean;
					cityVariance = variance;
				}
				
//				// Plan
//				if(i%2 == 0) {
//					value = -50;
//				}
				if(studyCase == 6) {
					double r11 = (Math.random()-0.5)*5;
					windMean = (int) r11;
					r11 = (Math.random()-0.5)*3;
					sunMean = (int) r11;
					r11 = (Math.random()-0.5)*3;
					cityMean = (int) r11;
					
				}	
			}
			else {
				// contracts study case
				int studyCase = 4;
				if(studyCase == 4) {
					windVariance = 0;
					sunVariance = 0;
					cityVariance = 1;
				}
				
			}
		
			EnvironmentItem wi = new EnvironmentItem(i, zone, windMean,windVariance,sunMean,sunVariance,cityMean,cityVariance,day);
			items.add(wi);
		}
		
	}
	

}
