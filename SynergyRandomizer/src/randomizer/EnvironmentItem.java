package randomizer;

import java.util.Random;

public class EnvironmentItem {

	
	public EnvironmentItem() {
		super();
	}

	private int windMean;
	private int windVariance;
	private int sunMean;
	private int sunVariance;
	private int cityMean;
	private int cityVariance;
	public int wind;
	public int sun; 
	public int city; 
	private boolean simulated;
	private Random r;
	public EnvironmentItem(int timestep, int zone, int windMean, int windVariance, int sunMean, int sunVariance, int cityMean, int cityVariance, int day) {
		super();
		this.windMean = windMean;
		this.windVariance = windVariance;
		this.sunMean = sunMean;
		this.sunVariance = sunVariance;
		this.cityMean = cityMean;
		this.cityVariance = cityVariance;
		r = new Random((4*zone+timestep)*(day+2));
		
		simulated=false;
	}
	
	// returns the prediction for city
	public int getCity() {
		if(!simulated) {
			simulate();
		}
		return city;
	}
	public int getCityMean() {
		return cityMean;
	}
	// returns the prediction for wind
	public int getWind() {
		if(!simulated) {
			simulate();
		}
		return wind;
	}
	// returns the prediction for sun
	public int getSun() {
		if(!simulated) {
			simulate();
		}
		return sun;
	}
	public int getWindMean() {
		return windMean;
	}
	public int getSunMean() {
		return sunMean;
	}
	
	// simulates the item
	public void simulate() {
		wind = (int) (r.nextGaussian()*windVariance+windMean);
		sun =  (int) (r.nextGaussian()*sunVariance+sunMean);
		city = (int) (r.nextGaussian()*cityVariance+cityMean);
		simulated = true;
	}
	
	// returns the prediction
	public double[] predict() {
		double res[] = {windMean,sunMean,cityMean};
		return res;
	}
}
