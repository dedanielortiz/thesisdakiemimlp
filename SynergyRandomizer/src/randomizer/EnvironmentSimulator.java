package randomizer;

import java.util.LinkedList;
import java.util.List;

import SynergyDemo3.EnergyMarket;

public class EnvironmentSimulator {
	
	private List<EnvironmentDay> todayInZones; 
	private List<EnvironmentDay> tomorrowInZones; 
	private int numOfZones;
	private int timesteps;
	private int dayNumber;
	EnergyMarket em;
	public List<EnvironmentDay> getTodayInZones() {
		return todayInZones;
	}
	
	public List<EnvironmentDay> getTomorrowInZones() {
		return tomorrowInZones;
	}

	public EnvironmentSimulator(int numOfZones, int timesteps, EnergyMarket em) {
		// The means are taken directly from the plan. The planers plan
		// the production and consumption, so they know the expectancy
		this.numOfZones=numOfZones;
		this.timesteps = timesteps;
		this.em = em;
		todayInZones = new LinkedList<>();
		dayNumber = 0;
		
		// Create a dayobject for each zone for today
		for(int i=0; i<numOfZones; i++) {

			EnvironmentDay day = new EnvironmentDay(timesteps, 7, 18, i,  em, dayNumber);
			todayInZones.add(day);
		}
		
		// Create a day object for each zone for tomorrow
		tomorrowInZones = new LinkedList<>();
		for(int i=0; i<numOfZones; i++) {

			EnvironmentDay day = new EnvironmentDay(timesteps, 7, 18, i,  em, (dayNumber+1));
			tomorrowInZones.add(day);
		}
	}
	
	// when the day is due, tomorrow is today
	public void updateEnvironment() {
		dayNumber++;
		todayInZones = tomorrowInZones;
		tomorrowInZones = new LinkedList<>();
		for(int i=0; i<numOfZones; i++) {

			EnvironmentDay day = new EnvironmentDay(timesteps, 7, 18, i,  em, (dayNumber+1));
			tomorrowInZones.add(day);
		}
	}

}
