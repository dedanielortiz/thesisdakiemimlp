package randomizer;

import java.util.HashMap;
import java.util.Random;

import org.apache.log4j.Logger;
import SynergyDemo3.City;
import SynergyDemo3.Contract;
import SynergyDemo3.EnergyGrid;
import SynergyDemo3.EnergyMarket;
import SynergyDemo3.Factory;
import SynergyDemo3.PowerLine;
import SynergyDemo3.PowerNode;
import SynergyDemo3.Producer;
import SynergyDemo3.SolarPlant;
import SynergyDemo3.SynergyDemo;
import SynergyDemo3.WindPlant;

public class Randomizer {
	
	private EnvironmentSimulator simulator; 
	public Randomizer(int numOfZones, int timesteps, EnergyMarket energyMarket, int seed) {
		simulator = new EnvironmentSimulator(numOfZones, timesteps, energyMarket);
		new Random(seed);
	}
	


	public EnvironmentSimulator getSimulator() {
		return simulator;
	}



	/**
	 * Apply the simulations of the environment. Change the Wind and Solar Plants. Change the cities
	 * 
	 * @param sd
	 * @param timestep
	 * @param day
	 */
	public void randomize(SynergyDemo sd, int timestep, int day) {
		HashMap<PowerNode, Contract> contractsNow = new HashMap<>();
		for(Contract c: sd.getMarket().getContracts()) {
			if(c.getNode() instanceof Factory || c.getNode() instanceof WindPlant || c.getNode() instanceof SolarPlant || c.getNode() instanceof City) {
				if(timestep>=c.getStartTime()&&timestep<=c.getEndTime()&&c.getDay()==day) {
					contractsNow.put(c.getNode(), c);
				}
			}
		}
		EnergyGrid eg = sd.getGrid();
		for(PowerNode node : eg.getNodes()) {
			if(node instanceof WindPlant) {
				double simulated;
				double simulatedCurrent;
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the simulation is the contract + simulation.
					simulated = contractsNow.get(node).getCapacity();
					simulatedCurrent= contractsNow.get(node).getCapacity();
					
				}
				else {
					simulated = (int) (((Producer) node).getMaxCapacity()+getWindSimulation(node.getZone(),timestep,day));
					simulatedCurrent= (int) (((Producer) node).getCurrentCapacity()+getWindSimulation(node.getZone(),timestep,day));
					
				}
				if(simulated<0) {
					simulated = 0;
				}
				if(simulatedCurrent<0) {
					simulatedCurrent = 0;
				}
				if(simulated>((Producer) node).getMaxProduction()) {
					simulated = (int) ((Producer) node).getMaxProduction();
				}
//				log("T = "+timestep+": Wind Plant "+node.getName()+" has a sim. max production of "+simulated);
				node.setMaxCapacity(simulated);
				node.setCurrentCapacity(simulatedCurrent);
				if(node.getMaxCapacity()<node.getCurrentCapacity()) {
					node.setCurrentCapacity(node.getMaxCapacity());
				}
				if(getWindSimulation(node.getZone(),timestep,day)!=0) {
					log("T = "+timestep+": Wind Plant "+node.getName()+" has a sim. change of "+getWindSimulation(node.getZone(),timestep,day)+". New Value: "+(int)node.getCurrentCapacity());
				}
			}
			else if(node instanceof SolarPlant) {
				double simulated;
				double simulatedCurrent;
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the simulation is the contract + simulation.
					simulated = contractsNow.get(node).getCapacity()+getSunSimulation(node.getZone(),timestep,day);
					simulatedCurrent= contractsNow.get(node).getCapacity()+getSunSimulation(node.getZone(),timestep,day);
					
				}
				else {
					simulated = (int) (((Producer) node).getMaxCapacity()+getSunSimulation(node.getZone(),timestep,day));
					simulatedCurrent= (int) (((Producer) node).getCurrentCapacity()+getSunSimulation(node.getZone(),timestep,day));
					
				}
				if(simulated<0) {
					simulated = 0;
				}
				if(simulatedCurrent<0) {
					simulatedCurrent = 0;
				}
				if(simulated>((Producer) node).getMaxProduction()) {
					simulated = (int) ((Producer) node).getMaxProduction();
				}
//				log("T = "+timestep+": Solar Plant "+node.getName()+" has a sim. max production of "+simulated);

				node.setMaxCapacity(simulated);
				node.setCurrentCapacity(simulatedCurrent);
				if(node.getMaxCapacity()<node.getCurrentCapacity()) {
					node.setCurrentCapacity(node.getMaxCapacity());
				}
				if(getSunSimulation(node.getZone(),timestep,day)!=0) {
					log("T = "+timestep+": Solar Plant "+node.getName()+" has a sim. change of "+getSunSimulation(node.getZone(),timestep,day)+". New Value: "+(int)node.getCurrentCapacity());
				}
			}
			else if(node instanceof City) {
				double simulated;
				double simulatedCurrent;
				double simulatedChange = getCitySimulation(node.getZone(),timestep,day);
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the simulation is the contract + simulation.
					simulated = contractsNow.get(node).getCapacity()+simulatedChange;
					simulatedCurrent= contractsNow.get(node).getCapacity()+simulatedChange;
					
				}
				else {
					simulated = (int) node.getCurrentCapacity()+simulatedChange;
					simulatedCurrent= (int) node.getCurrentCapacity()+simulatedChange;
					
				}
				if(simulated>0) {
					simulated = 0;
				}
				if(simulatedCurrent>0) {
					simulatedCurrent = 0;
				}
//				log("T = "+timestep+": Wind Plant "+node.getName()+" has a sim. max production of "+simulated);
				node.setCurrentCapacity(simulatedCurrent);
				if(simulatedChange!=0) {
					log("T = "+timestep+": City "+node.getName()+" has a sim. change of "+simulatedChange+". New Value: "+(int)node.getCurrentCapacity());
				}
			}
			else if(node instanceof Factory && contractsNow.containsKey(node)) {
				node.setCurrentCapacity(contractsNow.get(node).getCapacity());
			}
			
			
		}
		//only for study case: Failure in transmission lines
//		randomizeFailures(sd, timestep);
	}
	
	/**
	 * To simulate failures in lines
	 * 
	 * @param sd
	 * @param timestep
	 */
	public void randomizeFailures(SynergyDemo sd, int timestep) {
		if(timestep == 4) {
			for(PowerLine line : sd.getGrid().getLines()) {
				if(line.getId().equals("l4t2t5")) {
					log("FAILURE IN LINE "+ line.getId());
					line.setActivated(false);
					break;
				}
			}
			
		}
	}

	
	
	/**
	 * Apply the predictions to the graph. This is used in the proactive planing. instead of
	 * applying the simulated changes (with variance), here must be applied the prediction
	 * 
	 * @param sd
	 * @param timestep
	 * @param day
	 */
	public void predict(SynergyDemo sd, int timestep, int day) {
		HashMap<PowerNode, Contract> contractsNow = new HashMap<>();
		for(Contract c: sd.getMarket().getContracts()) {
			if(c.getNode() instanceof Factory || c.getNode() instanceof WindPlant || c.getNode() instanceof SolarPlant || c.getNode() instanceof City) {
				if(timestep>=c.getStartTime()&&timestep<=c.getEndTime()&&c.getDay()==day) {
					contractsNow.put(c.getNode(), c);
				}
			}
		}
		EnergyGrid eg = sd.getGrid();
		for(PowerNode node : eg.getNodes()) {
			if(node instanceof WindPlant) {
				double predicted;
				double predictedCurrent;
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the prediction is the contract.
					// We suppose that the contract was made with statistical data
					predicted = contractsNow.get(node).getCapacity();
					predictedCurrent= contractsNow.get(node).getCapacity();
					
				}
				else {
					predicted = ((Producer) node).getMaxCapacity()+getWindPrediction(sd, node.getZone(),timestep,day);
					predictedCurrent= ((Producer) node).getCurrentCapacity()+getWindPrediction(sd, node.getZone(),timestep,day);
					
				}
				if(predicted<0) {
					predicted = 0;
				}
				if(predictedCurrent<0) {
					predictedCurrent = 0;
				}
				if(predicted>((Producer) node).getMaxProduction()) {
					predicted = ((Producer) node).getMaxProduction();
				}
//				log("T = "+timestep+": Wind Plant "+node.getName()+" has a predicted max production of "+predicted);
				node.setMaxCapacity(predicted);
				node.setCurrentCapacity(predictedCurrent);
				if(node.getMaxCapacity()<node.getCurrentCapacity()) {
					node.setCurrentCapacity(node.getMaxCapacity());
				}
				log("T = "+timestep+": Wind Plant "+node.getName()+" has a predicted change of "+getWindPrediction(sd, node.getZone(),timestep,day)+". New Value: "+(int)node.getCurrentCapacity());
				
			}
			else if(node instanceof SolarPlant) {
				double predicted;
				double predictedCurrent;
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the prediction is the contract.
					// We suppose that the contract was made with statistical data
					predicted = contractsNow.get(node).getCapacity();
					predictedCurrent= contractsNow.get(node).getCapacity();
					
				}
				else {
				  predicted = ((Producer) node).getMaxCapacity()+getSunPrediction(sd, node.getZone(),timestep,day);
				       predictedCurrent= ((Producer) node).getCurrentCapacity()+getSunPrediction(sd, node.getZone(),timestep,day);
				}
				if(predicted<0) {
					predicted = 0;
				}
				if(predictedCurrent<0) {
					predictedCurrent = 0;
				}
				if(predicted>((Producer) node).getMaxProduction()) {
					predicted = ((Producer) node).getMaxProduction();
				}

				node.setMaxCapacity(predicted);
				node.setCurrentCapacity(predictedCurrent);
				if(node.getMaxCapacity()<node.getCurrentCapacity()) {
					node.setCurrentCapacity(node.getMaxCapacity());
				}
				log("T = "+timestep+": Solar Plant "+node.getName()+" has a predicted change of "+getSunPrediction(sd, node.getZone(),timestep,day)+". New Value: "+(int)node.getCurrentCapacity());
				
			}
			else if(node instanceof City) {
				double predicted;
				double predictedCurrent;
				if(contractsNow.containsKey(node)) {
					// if there is a contract, the prediction is the contract
					predicted = contractsNow.get(node).getCapacity();
					predictedCurrent= contractsNow.get(node).getCapacity();
					
				}
				else {
					predicted = (int) node.getCurrentCapacity()+getCityPrediction(sd, node.getZone(),timestep,day);
					predictedCurrent= (int) node.getCurrentCapacity()+getCityPrediction(sd,node.getZone(),timestep,day);
					
				}
				if(predicted>0) {
					predicted = 0;
				}
				if(predictedCurrent>0) {
					predictedCurrent = 0;
				}
				node.setCurrentCapacity(predictedCurrent);
				log("T = "+timestep+": City "+node.getName()+" has a predicted value of: "+(int)node.getCurrentCapacity());
			}
			else if(node instanceof Factory && contractsNow.containsKey(node)) {
				node.setCurrentCapacity(contractsNow.get(node).getCapacity());
			}
		}
	}
	
	private static void log(final String message) {
		Logger.getRootLogger().info("Randomizer: " + message);
	}
	
	public int getWindPrediction(SynergyDemo sd,int zone, int timestep, int day) {
		if(sd.getDay()==0) {
			return simulator.getTodayInZones().get(zone).getItems().get(timestep).getWindMean();
		}
		else {
			return simulator.getTomorrowInZones().get(zone).getItems().get(timestep).getWindMean();
		}
		
	}
	public int getSunPrediction(SynergyDemo sd, int zone, int timestep, int day) {
		if(sd.getDay()==0) {
			return simulator.getTodayInZones().get(zone).getItems().get(timestep).getSunMean();
		}
		else {
			return simulator.getTomorrowInZones().get(zone).getItems().get(timestep).getSunMean();
		}
	}
	public int getCityPrediction(SynergyDemo sd, int zone, int timestep, int day) {
		if(sd.getDay()==0) {
			return simulator.getTodayInZones().get(zone).getItems().get(timestep).getCityMean();
		}
		else {
			return simulator.getTomorrowInZones().get(zone).getItems().get(timestep).getCityMean();
		}
	}
	public int getWindSimulation(int zone, int timestep, int day) {
		return simulator.getTodayInZones().get(zone).getItems().get(timestep).getWind();
	}
	public int getSunSimulation(int zone, int timestep, int day) {
		return simulator.getTodayInZones().get(zone).getItems().get(timestep).getSun();
	}
	public int getCitySimulation(int zone, int timestep, int day) {
		return simulator.getTodayInZones().get(zone).getItems().get(timestep).getCity();
	}
	

}
