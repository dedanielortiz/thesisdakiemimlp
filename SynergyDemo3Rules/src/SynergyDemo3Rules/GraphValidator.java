package SynergyDemo3Rules;

import java.io.File;
import org.eclipse.emf.common.util.URI;
import SynergyDemo3.SynergyDemo;
import SynergyDemo3Rules.api.SynergyDemo3RulesDemoclesApp;

public class GraphValidator extends SynergyDemo3RulesDemoclesApp {
  public GraphValidator(SynergyDemo synergyDemo) {
    // Determine the path to the workspace root for loading models
    File root = new File(GraphValidator.class.getResource(".").getFile());
    workspacePath = root.getParentFile().getParentFile()
                        .getParent() + File.separatorChar;
    
    // Add the synergyDemo be monitored by the pattern matcher
    if(synergyDemo == null) {
    	createModel(URI.createURI("first_instance.xmi"));
    	resourceSet.getResources().get(0).getContents().add(synergyDemo);
    }
    else {
    	resourceSet = synergyDemo.eResource().getResourceSet();
    }
  }
}