package Simulator;

import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.knowm.jspice.netlist.Netlist;
import org.knowm.jspice.netlist.NetlistBuilder;
import org.knowm.jspice.simulate.dcoperatingpoint.DCOperatingPoint;
import org.knowm.jspice.simulate.dcoperatingpoint.DCOperatingPointResult;

import SynergyDemo3.Consumer;
import SynergyDemo3.PowerLine;
import SynergyDemo3.PowerNode;
import SynergyDemo3.Producer;
import SynergyDemo3.Bus;
import gurobi.GRB;
import gurobi.GRBEnv;
import gurobi.GRBException;
import gurobi.GRBLinExpr;
import gurobi.GRBModel;
import gurobi.GRBVar;
import scpsolver.problems.LPSolution;
import scpsolver.problems.LPWizard;
import scpsolver.problems.LPWizardConstraint;
import SynergyDemo3.EnergyGrid;

public class Simulator {
	
	private static void log(final String message) {
		Logger.getRootLogger().info("Simulator: " + message);
	}
	
	/**
	 * Simulates with LPSolve
	 * 
	 * @param grid
	 */
	public static void LPWizardSolve(EnergyGrid grid) {
		
		long time = System.nanoTime();
		
		LPWizard lpw = new LPWizard();
	
	    Map<String,PowerNode> elements = new HashMap<>();
	    Map<String,PowerLine> lines = new HashMap<>();
		
		// Minimize absolute value of balance.
		LPWizardConstraint balSubstR = lpw.addConstraint( "balSR",0,"<=");
		LPWizardConstraint balSubstL = lpw.addConstraint( "balSL",0,"<=");
		LPWizardConstraint lastBalanceConstrain = lpw.addConstraint( "lbc",Math.abs(grid.getBalance()),">=");
		
		balSubstR.plus("Ubal",1.0).plus("bal",-1.0);
		balSubstL.plus("Ubal",1.0).plus("bal",+1.0);
		lpw.plus("Ubal",1.0);
		
		lastBalanceConstrain.plus("Ubal",1.0);
		
	 	
		for(PowerLine line: grid.getLines()) {
			if(line.isActivated()&&line.getSource().isActivated()&&line.getSink().isActivated()) {
				lines.put(line.getId(), line);
				
			}
		}
		
		LPWizardConstraint lpwbalL = lpw.addConstraint( "lpwbalL",0,">=");
		LPWizardConstraint lpwbalR = lpw.addConstraint( "lpwbalR",0,"<=");
		for(PowerNode node: grid.getNodes()) {
			if(!node.isActivated()) {
				continue;
			}
			if(node instanceof Bus) {
				// In each bus, the sum of incoming and outgoing must be 0
				elements.put(node.getName(), node);
				LPWizardConstraint lpwcP = lpw.addConstraint( "kP"+node.getName(),0,">=");
				LPWizardConstraint lpwcM = lpw.addConstraint( "kM"+node.getName(),0,"<=");
				//System.out.print("0 = ");
				for(PowerLine line: node.getIncomingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}	
					lpwcP.plus(line.getId(),1.0);
					lpwcM.plus(line.getId(),1.0);
				}
				for(PowerLine line: node.getOutgoingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
	
					lpwcP.plus(line.getId(),-1.0);
					lpwcM.plus(line.getId(),-1.0);
				}
	
				if(grid.getReference() == node) {
					System.out.print("- bal");
					lpwcP.plus("bal",-1.0);
					lpwcM.plus("bal",-1.0);
				}
			}
			if(node instanceof Producer) {
				boolean addConstrain = false;
				
				for(PowerLine line: node.getOutgoingLines()) {
					if(line.isActivated()) {
						addConstrain = true;
						break;
					}
				}
				if(!addConstrain) {
					continue;
				}
				elements.put(node.getName(), node);
				
							
				// Capacity of Producers between 0 and max
				LPWizardConstraint lpwcP = lpw.addConstraint( "kP"+node.getName(),node.getMaxCapacity(),">=");
				LPWizardConstraint lpwcM = lpw.addConstraint( "kM"+node.getName(),0,"<=");
				
				// Producers can only with limited steps go up and down
				LPWizardConstraint stepR = lpw.addConstraint( "stepR"+node.getName(),((Producer) node).getPowerUpSpeed()+node.getCurrentCapacity(),">=");
				LPWizardConstraint stepL = lpw.addConstraint( "stepL"+node.getName(),node.getCurrentCapacity()-((Producer) node).getPowerDownSpeed(),"<=");
	
				for(PowerLine line: node.getOutgoingLines()) {
					lpwcP.plus(line.getId(),1.0);
					lpwcM.plus(line.getId(),1.0);
					lpwbalL.plus(line.getId(),1.0);
					lpwbalR.plus(line.getId(),1.0);
					stepR.plus(line.getId(),1.0);
					stepL.plus(line.getId(),1.0);
				}
			}
			if(node instanceof Consumer) {
				boolean addConstrain = false;
				for(PowerLine line: node.getIncomingLines()) {
					if(line.isActivated()) {
						addConstrain = true;
						break;
					}
				}
				
				if(!addConstrain) {
					continue;
				}
				
				elements.put(node.getName(), node);
				LPWizardConstraint lpwcP = lpw.addConstraint( "kP"+node.getName(),node.getCurrentCapacity(),">=");
				LPWizardConstraint lpwcM = lpw.addConstraint( "kM"+node.getName(),node.getCurrentCapacity(),"<=");
				for(PowerLine line: node.getIncomingLines()) {
					lpwcP.plus(line.getId(),-1.0);
					lpwcM.plus(line.getId(),-1.0);
					lpwbalL.plus(line.getId(),-1.0);
					lpwbalR.plus(line.getId(),-1.0);
				}
			}
		}
	
		lpwbalL.plus("bal",-1.0);
		lpwbalR.plus("bal",-1.0);
		
		for(PowerLine line: grid.getLines()) {
			if(!line.isActivated()) {
				continue;
			}
			if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
				continue;
			}
			if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
	
				LPWizardConstraint lpwcP = lpw.addConstraint( "kPl"+line.getId(),0.0,">=");
				LPWizardConstraint lpwcM = lpw.addConstraint( "kMl"+line.getId(),0.0,"<=");
				
				lpwcP.plus(line.getId(),line.getReactance()).plus("v"+line.getSource().getName(),1.0).plus("v"+line.getSink().getName(),-1.0);
				lpwcM.plus(line.getId(),line.getReactance()).plus("v"+line.getSource().getName(),1.0).plus("v"+line.getSink().getName(),-1.0);
				
				// Absolute Value in Constrain Function: substitution
				LPWizardConstraint lineSubstR = lpw.addConstraint( line.getId()+"SR",0,"<=");
				LPWizardConstraint lineSubstL = lpw.addConstraint( line.getId()+"SL",0,"<=");
				
				lineSubstR.plus("U"+line.getId(),1.0).plus(line.getId(),-1.0);
				lineSubstL.plus("U"+line.getId(),1.0).plus(line.getId(),+1.0);
				
				if(Math.abs(line.getCurrentCapacity()/line.getMaxCapacity())>0.795) {
					lpw.plus("U"+line.getId(),line.getCurrentCapacity()/line.getMaxCapacity()*100); // Vielleicht add different weights? the lines are different... JA
				}
				else {
					// Do changes, without making the rest overloaded
					LPWizardConstraint lineSubstR2 = lpw.addConstraint( line.getId()+"SR2",0,"<=");
					LPWizardConstraint lineSubstL2 = lpw.addConstraint( line.getId()+"SL2",0,"<=");
					
					lineSubstR2.plus("U"+line.getId(),1.0).plus(line.getId(),-1.0);
					lineSubstL2.plus("U"+line.getId(),1.0).plus(line.getId(),+1.0);
					
					LPWizardConstraint lineSubst3 = lpw.addConstraint( line.getId()+"SL3",0.795*line.getMaxCapacity(),">=");
					lineSubst3.plus("U"+line.getId(),1.0); // Vielleicht add different weights? the lines are different... JA
	
				}
			}
		}
	
		lpw.setMinProblem(true);
		
		long time2 = System.nanoTime();
		LPSolution solution = lpw.solve();
		log("Time for LPSolve Solution: "+(System.nanoTime()-time2)/1000+"us");

		
		for(PowerLine line : lines.values()) {
			double capacity = solution.getDouble(line.getId());
			if(capacity < 0.0&&!(line.getSource() instanceof Producer)) {
				swapFlowDirection(line);
				capacity = -capacity;
			}
			if(line.getSource() instanceof Consumer||line.getSink() instanceof Consumer){
					line.setMaxCapacity(capacity);
			}
				line.setCurrentCapacity(capacity);
		}
		for(PowerNode node: grid.getNodes()) {
			if(node instanceof Producer&&node.isActivated()&&!isIsolated(node)) {
				double capacity = solution.getDouble(node.getOutgoingLines().get(0).getId());
				node.setCurrentCapacity(capacity);
			}
		}
		grid.setBalance((int)solution.getDouble("bal"));
		log("Time for LPSolve Algorithm: "+(System.nanoTime()-time)/1000+"us");
	}
			
	/**
	 * Calculates the balance of the network
	 * 
	 * @param grid
	 * @param toShutDown
	 * @return
	 */
	public static int calculateBalance(EnergyGrid grid, PowerLine toShutDown) {
		// Calculate Balance
		int gridBalance = 0;
		for(PowerNode node: grid.getNodes()) {
			double lastGridBalance = gridBalance;
			for(PowerLine line : node.getOutgoingLines()) {
				if(line.isActivated()&&node.isActivated()&&line!=toShutDown) {
					gridBalance+=node.getCurrentCapacity();
					break;
				}
			}
			if (lastGridBalance!=gridBalance){
				continue;
			}
			for(PowerLine line : node.getIncomingLines()) {
				if(line.isActivated()&&node.isActivated()&&line!=toShutDown) {
					gridBalance+=node.getCurrentCapacity();
					break;
				}
			}
		}
		// Substract the isolated transformers
		for(PowerNode node: grid.getNodes()) {
			if(node instanceof Bus && isIsolated(node)) {
				for(PowerLine line : node.getOutgoingLines()) {
						if(line.isActivated()&&node.isActivated()&&line!=toShutDown) {
							gridBalance-=line.getSink().getCurrentCapacity();
						}
				}
				for(PowerLine line : node.getIncomingLines()) {
					if(line.isActivated()&&node.isActivated()&&line!=toShutDown) {
						gridBalance-=line.getSource().getCurrentCapacity();
					}
				}
			}
		}
		return gridBalance;
	}
	
	/**
	 * determines if the network has unconnected busses or not
	 * 
	 * @param line
	 * @param toShutDown
	 * @return
	 */
	public static boolean hasDanglingBus(PowerLine line, PowerLine toShutDown) {
		if(line.getSource() instanceof Bus) {
			int connectedLines = 0;
			for(PowerLine cline : line.getSource().getOutgoingLines()) {
				if(cline.isActivated()&&cline!=toShutDown) {
					connectedLines++;
				}
			}
			for(PowerLine cline : line.getSource().getIncomingLines()) {
				if(cline.isActivated()&&cline!=toShutDown) {
					connectedLines++;
				}
			}
			if(connectedLines < 2) {
				return true;
			}
		}
		if(line.getSink() instanceof Bus) {
			int connectedLines = 0;
			for(PowerLine cline : line.getSink().getOutgoingLines()) {
				if(cline.isActivated()&&cline!=toShutDown) {
					connectedLines++;
				}
			}
			for(PowerLine cline : line.getSink().getIncomingLines()) {
				if(cline.isActivated()&&cline!=toShutDown) {
					connectedLines++;
				}
			}
			if(connectedLines < 2) {
				return true;
			}
		}
		return false;
		
	}
		
	/**
	 * makes the simulation with JSpice
	 * 
	 * @param grid
	 */
	public static void jspiceSolve(EnergyGrid grid) {
		
		log("Starting Simulation with jSpice");
		long time = System.nanoTime();
		NetlistBuilder builder2 = new NetlistBuilder();
		double reactanceReference = 1000000000f;
		grid.setReference(null);
		
		Map<String, PowerLine> gridLines = new HashMap<>();
		Map<String, PowerLine> transformerLines = new HashMap<>();
		Map<String, PowerNode> gridBuss = new HashMap<>();
		
		for(PowerLine line : grid.getLines()) {
			// Reset Capacities of lines. They must be calculated again.
			line.setCurrentCapacity(0);
			
			// Classify Lines in transformer or normal
			gridLines.put(line.getId(), line);
			if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
				transformerLines.put(line.getId(), line);
			}
		}
		
		// Collect all transformers
		for(PowerNode node : grid.getNodes()) {
			if(node instanceof Bus) {
				gridBuss.put(node.getName(), node);
			}
		}
		
		// The balance can be calculated without the simulation
		grid.setBalance(calculateBalance(grid,null));
		//grid.setCost(calculatePrice(grid));
		
		// Set a reference for the grid.
		if(grid.getReference() == null||isIsolated(grid.getReference())) {
			for(PowerNode node: grid.getNodes()) {
				if(node instanceof Bus && !isIsolated(node)) {
					grid.setReference(node);
					break;
				}
			}
		}
		
		// Set a grid reference if theres none
		if(grid.getReference() == null) {
			for(PowerNode node: grid.getNodes()) {
				if(node instanceof Bus) {
					grid.setReference(node);
					break;
				}
			}
		}
		
		
		// Create the circuit
		for(PowerLine line: grid.getLines()) {
			if(!line.isActivated()) {
				//line.setCurrentCapacity(0);
				continue;
			}
			if(line.getSource() instanceof Consumer || line.getSink() instanceof Producer) {
				swapFlowDirection(line); // Consumer always sink. Producer always source
			}
			// Buss not connected to anything but just one other transformer have 0 current.
			if(hasDanglingBus(line,null)) {
				continue;
			}
			
			// add a resistance for each line, a resistance for each consumer, a generator for each producer
			addNetComponent(line,builder2,reactanceReference);
		}
		
		long time2 = System.nanoTime();
		Netlist netlist2 = builder2.build();
		DCOperatingPointResult dcOpResult = new DCOperatingPoint(netlist2).run();
		log("Time for JSpice Solution: "+(System.nanoTime()-time2)/1000+"us");

		for(String s : dcOpResult.getDeviceLabels2Value().keySet()) {
			if(s.charAt(2)=='l') {
				PowerLine line = gridLines.get(s.substring(2,s.length()-1));
				double capacity = dcOpResult.getDeviceLabels2Value().get(s).doubleValue();
				if(Math.round(1000*capacity)/1000<0) {
					swapFlowDirection(line);
				}
				line.setCurrentCapacity(Math.abs(capacity));
			}
		}
		
		log("Time for JSpice Algorithm: "+(System.nanoTime()-time)/1000+"us");
		
	}
	
	/**
	 * Makes the simulation with gurobi
	 * 
	 * @param grid
	 * @return
	 * @throws GRBException
	 */
	public static int GurobiSolve(EnergyGrid grid) throws GRBException {
		
		log("Starting Simulation with Gurobi");
		long time = System.nanoTime();
		// Create empty environment, set options, and start
		GRBEnv env = new GRBEnv(true);
	    env.set("logFile", "mip1.log");
	    env.set(GRB.IntParam.OutputFlag, 0);
	    env.set(GRB.IntParam.LogToConsole, 0);
	
	    env.start();
	
	    // Create empty model
	    GRBModel  model = new GRBModel(env);

	    Map<String,PowerNode> elements = new HashMap<>();
	    Map<String,PowerLine> lines = new HashMap<>();
	    
		// Create variable
								 
	    model.update();
	    
	    GRBLinExpr objectiveGRB = new GRBLinExpr();
	    
		for(PowerLine line: grid.getLines()) {
			if(line.isActivated()&&line.getSource().isActivated()&&line.getSink().isActivated()) {
				lines.put(line.getId(), line);
				if(line.getSource() instanceof Consumer) {
					Simulator.swapFlowDirection(line);
				}
				if(line.getSink() instanceof Producer) {
					Simulator.swapFlowDirection(line);
				}
				if(line.getSource() instanceof Producer)  {

					// Elements in the network dont change for the simulation
					model.addVar(line.getSource().getCurrentCapacity(), line.getSource().getCurrentCapacity(), 0, GRB.CONTINUOUS, line.getId());
	  
				}
				else if(line.getSink() instanceof Consumer) {
					model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, line.getId());
				}
				else {
					// We let the values for the lines open. they must be determined.
					model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, line.getId());
				}
			}
			else {
				// If line is not connected to the graph or is not active, set 0 directly
				line.setCurrentCapacity(0);
			}
		}
		model.update();
		model.setObjective(objectiveGRB, GRB.MINIMIZE);
	
		// The reference for the adjustment on the consumer resistance
		GRBVar cReference = model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "cReference");
		for(PowerNode node: grid.getNodes()) {
			if(!node.isActivated()) {
				continue;
			}
			if(node instanceof Bus) {
				elements.put(node.getName(), node);
				//Add the variables for the potential on the buses
				model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "V"+node.getName());
				GRBLinExpr transformerGRB = new GRBLinExpr();
				for(PowerLine line: node.getIncomingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					// everything that goes in the bus
					transformerGRB.addTerm(1.0, model.getVarByName(line.getId()));
				}
				for(PowerLine line: node.getOutgoingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					// minus everything that goes out the bus
					transformerGRB.addTerm(-1.0, model.getVarByName(line.getId()));
				}
				
				if(grid.getReference() == null||!grid.getReference().isActivated()) {
					// Set as reference the first bus found
					grid.setReference(node);
				}
				// in minus out must be 0. Kirchhoffs law
				model.addConstr(transformerGRB,GRB.EQUAL, 0.0, "k"+node.getName());
			}
			else {
				boolean addConstrain = false;
				
				// If Producer, it has outgoing line
				for(PowerLine line: node.getOutgoingLines()) {
					if(line.isActivated()&&line.getSink().isActivated()) {
						addConstrain = true;
					}
				}
				
				// If consumer, it has incoming line
				for(PowerLine line: node.getIncomingLines()) {
					if(line.isActivated()&&line.getSource().isActivated()) {
						GRBVar c = model.getVarByName(line.getId());
						GRBLinExpr consumers = new GRBLinExpr();
						consumers.addTerm(1,c);	
						consumers.addTerm(-node.getCurrentCapacity(),cReference);	
						model.addConstr(consumers, GRB.EQUAL, 0.0, "consumer"+node.getName());
						node.getCurrentCapacity();
						addConstrain = true;
					}
				}
				if(addConstrain) {
					elements.put(node.getName(), node);
				}
	
				
			}
		}
		model.update();
		
		// Ohms law
		for(PowerLine line: grid.getLines()) {
			if(!line.isActivated()) {
				continue;
			}
			if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
				continue;
			}
			if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
	
				GRBLinExpr ohmLine = new GRBLinExpr();
				ohmLine.addTerm(-line.getReactance(), model.getVarByName(line.getId()));
				ohmLine.addTerm(1, model.getVarByName("V"+line.getSource().getName()));
				ohmLine.addTerm(-1, model.getVarByName("V"+line.getSink().getName()));
				model.addConstr(ohmLine, GRB.EQUAL, 0.0, "ohmLine"+line.getId());
 
					 
			}
		}
		long time2 = System.nanoTime();
		model.optimize();
		log("Time for Gurobi Solution: "+(System.nanoTime()-time2)/1000+"us");
		int status = model.get(GRB.IntAttr.Status);
		if(status == 3) {
			model.dispose();
			env.dispose();
			log("Infeasable");
			return -1;
		}
	

		// Update the values of the lines
		
		for(PowerLine line : lines.values()) {
			GRBVar var = model.getVarByName(line.getId());
			double capacity = Math.round(1000*var.get(GRB.DoubleAttr.X))/1000;
			if(capacity < 0.0) {
				Simulator.swapFlowDirection(line);
				capacity = -capacity;
			}
			line.setCurrentCapacity(capacity);
			if(line.getSource() instanceof Consumer||line.getSink() instanceof Consumer
					||line.getSource() instanceof Producer||line.getSink() instanceof Producer){
				//line.setMaxCapacity(capacity);
			}
		}
		int balance = 0;
		
		// calculate balance with the active nodes
		for(PowerNode node : elements.values()) {
			balance+=node.getCurrentCapacity();
		}
		grid.setBalance(balance);
		
		 // Dispose of model and environment
		model.dispose();
		env.dispose();
		log("Time for Gurobi Algorithm: "+(System.nanoTime()-time)/1000+"us");
		

		return 1;
	}

	/**
	 * Gives, whether the node is not connected to the network. 
	 * 
	 * @param node
	 * @param toShutDown
	 * @return
	 */
	private static boolean isIsolated(PowerNode node) {
		for(PowerLine line : node.getOutgoingLines()) {
			if(line.getSink() instanceof Bus && line.isActivated()) {
				return false;
			}
		}
		for(PowerLine line : node.getIncomingLines()) {
			if(line.getSource() instanceof Bus && line.isActivated()) {
				return false;
			}
		}
		return true;
	}
	
	
	
	/**
	 * Adds the corresponding element to the circuit
	 * 
	 * @param line
	 * @param builder2
	 * @param reactanceReference
	 */
	public static void addNetComponent(PowerLine line, NetlistBuilder builder2, double reactanceReference) {
		if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
			return;
		}
		if(line.getSource() instanceof Producer) {
			builder2.addNetlistDCCurrent(line.getId(), line.getSource().getCurrentCapacity(), "0", line.getSink().getName());
		}
		else if(line.getSink() instanceof Consumer) {
			builder2.addNetlistResistor(line.getId(), reactanceReference/line.getSink().getCurrentCapacity(), line.getSource().getName(), "0");
		}
		else {
			builder2.addNetlistResistor(line.getId(), line.getReactance(), line.getSource().getName(), line.getSink().getName());
		}
	}
	
	public static void swapFlowDirection(PowerLine pl) {
		
		PowerNode aux = pl.getSource();
		pl.setSource(pl.getSink());
		pl.setSink(aux);
	}
	
	/**
	 * Returns the graphviz code to print the graph
	 * 
	 * @param grid
	 * @return
	 */
	public static String getGraphvizCode(EnergyGrid grid) {
		String output = "digraph depgraph {\n\r";
		
		HashMap<PowerNode,Integer> nodes = new HashMap<>();
		PowerNode toConnectBalance = grid.getReference();
		int nodeId=0;
		
		for(PowerNode node : grid.getNodes()) {
			String options="";
			if(node instanceof Bus) {
				if(node.isActivated()) {
					options = ", fillcolor=\"#00ff00\",  style=filled, fixedsize=true, width=0.5, shape=doublecircle";
				}
				else {
					options = ", fillcolor=\"#888888\",  style=filled, fixedsize=true, width=0.5, shape=doublecircle";
				}
			}
			else {
				options= ", fixedsize=true, shape=circle, width=0.3, fontsize=9";
				if(node.getCurrentCapacity()==0) {
					options+=", fillcolor=\"#888888\", style=filled";
				}
			}
			output+= "n"+nodeId+" [pos = \""+node.getXPos()+","+node.getYPos()+"!\", fontsize=11, label=\""+node.getName()+"\""+options+"];\r\n";
			nodes.put(node,nodeId);
			nodeId++;
		}
		
		for(PowerLine line : grid.getLines()) {
			if(line.getSource() instanceof Producer ||
					line.getSource() instanceof Consumer ||
					line.getSink() instanceof Producer ||
					line.getSink() instanceof Consumer ) {
				output+="n"+nodes.get(line.getSource())+"->"+"n"+nodes.get(line.getSink())+"[";
				if(line.isActivated()) {
					output+="fontsize=11, penwidth=0.5, label=\""+Math.round(1000*(Math.abs(line.getCurrentCapacity()))/1000)+"\"";
				}
				else {
					output+="arrowhead=none, style=dashed, fontsize=11, penwidth=0.5, label=\""+Math.round(1000*(Math.abs(line.getCurrentCapacity())))/1000+"\"";
				}
				output+="]"+"\r\n";
				continue;
			}
			double load = line.getCurrentCapacity()/line.getMaxCapacity();
			String loadColor="#0000ff";
			if(load>1) {
				loadColor="#ff0000";
			}
			else if(line.isMultipleConnection()&&load>=0.8) {
				loadColor="#ffff00";
			}
			else if(line.getCurrentCapacity() == 0) {
				loadColor="#888888";
			}
			double capacity = Math.min(4000, line.getMaxCapacity())*0.001f+0.9f;

			output+="n"+nodes.get(line.getSource())+"->"+"n"+nodes.get(line.getSink())+"[";
			if(!line.isActivated()) {
				loadColor="#888888";
				output+="arrowhead=none, style=dashed, fontsize=11, penwidth="+capacity+", color=\""+loadColor+"\", label=\""+(Math.round((load*100)))+"%\"";
			}
			else {
				output+="fontsize=11, penwidth="+capacity+", color=\""+loadColor+"\", label=\""+(Math.round((load*100)))+"%\"";
				if(line.isMultipleConnection()&&load>=0.8) {
					output+=", fontname=\"times bold\", fontcolor = \"#ff0000\" ";
				}
			}
			
			output+="]"+"\r\n";
		}
		
		if(grid.getBalance()!=0) {
			output+= "n"+nodeId+" [fontsize=11, label=\"BAL\", fixedsize=true, shape=doublecircle, width=0.3];\r\n";
			if(grid.getBalance()>0) {
				output+="n"+nodes.get(toConnectBalance)+"->"+"n"+nodeId+"[fontsize=11, penwidth=0.5,label=\""+Math.round(1000*grid.getBalance())/1000+"\"]"+"\r\n";			
			}
			else {
				output+="n"+nodeId+"->"+"n"+nodes.get(toConnectBalance)+"[fontsize=11, penwidth=0.5,label=\""+Math.round(-1000*grid.getBalance())/1000+"\"]"+"\r\n";			
			}
			nodeId++;
		}
		
		output+="}";
		return output;
	}
	
}
