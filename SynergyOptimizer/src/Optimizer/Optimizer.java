package Optimizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import gurobi.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.daniel.synergydemo3.synergygui.rules.SynergyDemoRules;
import org.moflon.core.utilities.eMoflonEMFUtil;
import randomizer.Randomizer;
import Simulator.Simulator;
import SynergyDemo3.Consumer;
import SynergyDemo3.Contract;
import SynergyDemo3.EnergyGrid;
import SynergyDemo3.EnergyMarket;
import SynergyDemo3.Offering;
import SynergyDemo3.PowerLine;
import SynergyDemo3.PowerNode;
import SynergyDemo3.Producer;
import SynergyDemo3.ScheduledChange;
import SynergyDemo3.SolarPlant;
import SynergyDemo3.SynergyDemo;
import SynergyDemo3.SynergyDemo3Factory;
import SynergyDemo3.Bus;
import SynergyDemo3.City;
import SynergyDemo3.WindPlant;

public class Optimizer {
	
	public static int reactiveOptimizationCount;

	public static void resetReactiveOptimizationCount() {
		reactiveOptimizationCount = 0;
	}
	
	/**
	 * Performs the Proactive Optimization on the given System
     *
	 * @param sd               The System where the optimization is done
	 * @param timestepsLimit   Maximal number of timesteps for the Optimization without AbLaV
	 * @param fillPlan         Wether or not to add the proactive optimization changes to the Plan
	 * @param applyAllChanges  Wether or not to apply directly the changes to the Grid
	 * @param nodesToMatch     Additional Adjustments for the optimization to match certain values on certain nodes
	 * @return
	 */
	public static int performProactiveOptimization(SynergyDemo sd, int timestepsLimit, boolean fillPlan, boolean applyAllChanges, HashMap<String,Double> nodesToMatch) {

		// Counter for the timesteps
		int timesteps = 1;
		// Variable to determine the success of the AbLaV algorithm. 1 is success. 0 is No solution
		// This variable should never take the value 0, since there is always a solution for AbLaV
		int abLaVResult = -1;
		
		// Variable to save the number of timesteps needed for the solution. This variable is 0,
		// if there is no solution to the optimization and if so, the number of timesteps needed for that solution.
		int timestepsNeeded = 0;

		while(timesteps <= timestepsLimit) {
			try {
				timestepsNeeded = proactiveOptimizationGurobi(sd,timestepsLimit, fillPlan, applyAllChanges, nodesToMatch);
			} catch (GRBException e1) {
				e1.printStackTrace();
				// This can happen, if the connection with Gurobi doesnt work
				log("There was a problem with the proactive optimization with Gurobi... Abort");
				return -1;
			}
			// Get out if a solution is found
			if(timestepsNeeded >= 0) {
				break;
			}
			else {
				log("Solution not found for "+timesteps+" timesteps");
				timesteps++;
			}
		}
		
		// If solution cannot be reached in less than the limit, the AbLaV Algorithm should be fired
		if(timestepsNeeded == -1) {
			log("Solution cannot be reached in "+timestepsLimit+" steps... perform AbLaV Algorithm");
			
			try {
				abLaVResult = abschaltbareLastenGurobi(sd,1, true, applyAllChanges);
			} catch (GRBException e) {
				log("There was a problem with the AbLaV optimization with Gurobi... Abort");
				e.printStackTrace();
				return -1;
			}
			
			if(abLaVResult==1) {
				log("AbLaV Scheduled!");
			}
			else {
				// This should never be reached. AbLaV has always a solution. 
				// If reached, there is a problem in the net topology (not connected elements, impossible connections...)
				log("Error in AbLaV");
			}
			
		}
		else {
			// Sometimes the solution has the same configuration that the network itself.
			if(timestepsNeeded==0&&sd.getPowerPlan().getReactiveChanges().isEmpty()) {
				log("No proactive changes must be done.");
			}
			else {
				log("Solution can be reached in "+timestepsNeeded+" timesteps");
				return timestepsNeeded;
			}
		}
		return timestepsNeeded;
	}
	
	/**
	 * @param sd               the system to performe the optimization
	 * @param fillPlan         whether or not to plan the changes in PowerPlan
	 * @param timestepsLimit   max. number of timesteps for the optimization.
	 * @param applyAllChanges  whether or not to apply the changes directly to the graph
	 * @param print            show info of optimization
	 */
	public static void performReactiveOptimization(SynergyDemo sd, boolean fillPlan, int timestepsLimit, boolean applyAllChanges, boolean print) {

		int timesteps = 1;
		int chopResult = -1;
		double balanceTolerance = sd.getGrid().getBalanceTolerance();
		
		int timestepsNeeded = -1;
		
		while(timesteps <= timestepsLimit) {
			try {
				timestepsNeeded = reactiveOptimizationGurobi(sd,null,timesteps,-1, fillPlan, balanceTolerance, applyAllChanges, print);			} catch (GRBException e1) {
				e1.printStackTrace();
			}
			if(timestepsNeeded != -1) {
				break;
			}
			else {
				log("Solution not found for "+timesteps+" timesteps");
				timesteps++;
			}
		}
		if(timestepsNeeded == -1) {
			
			log("Solution cannot be reached in "+timestepsLimit+" steps... chop");
			
			try {
				chopResult = abschaltbareLastenGurobi(sd,1,fillPlan, applyAllChanges);
			} catch (GRBException e) {
				e.printStackTrace();
			}
			
			if(chopResult==1) {
				log("Chop Scheduled!");
			}
			
		}
		else {
			if(sd.getPowerPlan().getReactiveChanges().isEmpty()) {
				log("Solution can be reached in "+timestepsNeeded+" timesteps");
				//log("No solution found... no changes?");
			}
			else {
				// Das muss geändert werden, im moment is timestepsNeeded always 1
				log("Solution can be reached in "+timestepsNeeded+" timesteps");
			}
		}
	}

	/**
	 * Performs Abschaltbare Lasten Verfahren Optimization
	 * 
	 * @param sd
	 * @param timesteps
	 * @param fillPlan
	 * @param applyAllChanges
	 * @return                    1 if solution found. 
	 * @throws GRBException
	 */
	public static int abschaltbareLastenGurobi(SynergyDemo sd, int timesteps, boolean fillPlan, boolean applyAllChanges) throws GRBException {
		// Create empty environment, set options, and start
			log("Starting AbLaV...");
			GRBEnv env = new GRBEnv(true);
		    env.set("logFile", "mip1.log");
		    env.set(GRB.IntParam.OutputFlag, 0);
		    env.set(GRB.IntParam.LogToConsole, 0);
		
		    env.start();
		
		    GRBModel  model = new GRBModel(env);
		   
			
			EnergyGrid grid = sd.getGrid();
		

			// Create variables forr the balance
		    double balanceTolerance = sd.getGrid().getBalanceTolerance();
		    GRBVar bal = model.addVar(-balanceTolerance, balanceTolerance, 0, GRB.CONTINUOUS, "bal");
		    // balAbs = |bal|
		    GRBVar balAbs = model.addVar(0, balanceTolerance, 0, GRB.CONTINUOUS, "balAbs");
		    model.update();
		    model.addGenConstrAbs(balAbs, bal, "balAbsConst");
		    GRBLinExpr objectiveGRB = new GRBLinExpr();
		    
		    
		    for(PowerNode node: grid.getNodes()) {
		    	if(!node.isActivated()) {
		    		continue;
		    	}
		    	// x_n variables define if the node is cut (0) or stays in the network (1)
	    		GRBVar x_n;
	    		if(node instanceof Bus) {
	    			x_n=  model.addVar(1, 1, 0, GRB.INTEGER, "x"+node.getName());
	    			objectiveGRB.addTerm(1, x_n);
	    		}
	    		else if(node instanceof Producer) {
	    			x_n= model.addVar(0, 1, 0, GRB.INTEGER, "x"+node.getName());
	    			double factor = -1;//-1/(node.getCurrentCapacity()*((Producer)node).getPriceProMW());
	    		
	    			objectiveGRB.addTerm(factor, x_n);
	    			
	    		}else if(node instanceof Consumer){
	    			double factor = -1;//(node.getCurrentCapacity()*((Consumer)node).getPriceProMW());
	    			x_n= model.addVar(0, 1, 0, GRB.INTEGER, "x"+node.getName());
	    			objectiveGRB.addTerm(factor, x_n);
	    		}
	    		
		    }
		    model.update();
			

			for(PowerLine line: grid.getLines()) {
				if(line.isActivated()&&line.getSource().isActivated()&&line.getSink().isActivated()) {
					// zxi = xsource * xsink
					GRBVar xsource = model.getVarByName("x"+line.getSource().getName());
					GRBVar xsink = model.getVarByName("x"+line.getSink().getName());
					GRBVar zxi = model.addVar(0, 1, 0, GRB.INTEGER, "zx"+line.getId());
					GRBLinExpr zVx = new GRBLinExpr();
					GRBLinExpr zVy = new GRBLinExpr();
					GRBLinExpr zVz = new GRBLinExpr();
					zVx.addTerm(1.0, zxi);
					zVx.addTerm(-1.0, xsource);
					model.addConstr(zVx,GRB.LESS_EQUAL, 0, "zVx"+line.getId());
					zVy.addTerm(1.0, zxi);
					zVy.addTerm(-1.0, xsink);
					model.addConstr(zVy,GRB.LESS_EQUAL, 0, "zVy"+line.getId());
					zVz.addTerm(1.0, zxi);
					zVz.addTerm(-1.0, xsource);
					zVz.addTerm(-1.0, xsink);
					model.addConstr(zVz,GRB.GREATER_EQUAL, -1, "zVz"+line.getId());
					if(line.getSource() instanceof Producer)  {
						
						// Set the low and upper bounds for the producer
						double powerChanged = timesteps==0?0:((Producer)line.getSource()).getPowerChanged();
						double maxUpChange = Math.max(0,timesteps*((Producer)line.getSource()).getPowerUpSpeed() - powerChanged);
						double maxDownChange = timesteps*((Producer)line.getSource()).getPowerDownSpeed() + powerChanged;
						double lb = Math.max(0,line.getSource().getCurrentCapacity()-maxDownChange);
						double ub = Math.min(line.getSource().getMaxCapacity(),line.getSource().getCurrentCapacity()+maxUpChange);
						
						GRBVar pi = model.addVar(lb, ub, 0, GRB.CONTINUOUS, line.getId());
						GRBVar zi = model.addVar(0, ub, 0, GRB.CONTINUOUS, "Z"+line.getId());
						
						// lb*zxi <= zi <= ub*zxi
						GRBLinExpr z1 = new GRBLinExpr();
						z1.addTerm(1.0, zi);
						z1.addTerm(-ub, zxi);
						model.addConstr(z1,GRB.LESS_EQUAL, 0.0, "z1"+line.getId());
						GRBLinExpr z11 = new GRBLinExpr();
						z11.addTerm(-1.0, zi);
						z11.addTerm(lb, zxi);
						model.addConstr(z11,GRB.LESS_EQUAL, 0.0, "z11"+line.getId());
						// min(0,lb) <= zi <= ub
						GRBLinExpr z0 = new GRBLinExpr();
						z0.addTerm(1.0, zi);
						model.addConstr(z0,GRB.GREATER_EQUAL, Math.min(0, lb), "z0"+line.getId());
						GRBLinExpr z01 = new GRBLinExpr();
						z0.addTerm(1.0, zi);
						model.addConstr(z01,GRB.LESS_EQUAL, ub, "z01"+line.getId());
						
						// p_i - (1-zxi)ub <= zi <= p_i - (1-zxi)lb
						GRBLinExpr z3 = new GRBLinExpr();
						z3.addTerm(1.0, zi);
						z3.addTerm(-1,pi);
						z3.addTerm(-lb, zxi);
						model.addConstr(z3,GRB.LESS_EQUAL, -lb, "z3"+line.getId());
						GRBLinExpr z2 = new GRBLinExpr();
						z2.addTerm(-1.0, zi);
						z2.addTerm(1,pi);
						z2.addTerm(ub, zxi);
						
						// z <= p_i + (1-zxi)ub
						model.addConstr(z2,GRB.LESS_EQUAL, ub, "z2"+line.getId());
						GRBLinExpr z4 = new GRBLinExpr();
						z4.addTerm(1.0, zi);
						z4.addTerm(-1,pi);
						z4.addTerm(ub, zxi);
						model.addConstr(z4,GRB.LESS_EQUAL, ub, "z4"+line.getId());
					}
					else if(line.getSink() instanceof Consumer) {
						double capacity = Math.pow(10, 9);
						GRBVar pi = model.addVar(0, capacity, 0, GRB.CONTINUOUS, line.getId());
						GRBVar zi = model.addVar(0, capacity, 0, GRB.CONTINUOUS, "Z"+line.getId());
						
						GRBLinExpr z1 = new GRBLinExpr();
						z1.addTerm(1.0, zi);
						z1.addTerm(-capacity, zxi);
						model.addConstr(z1,GRB.LESS_EQUAL, 0.0, "z1"+line.getId());
						GRBLinExpr z2 = new GRBLinExpr();
						z2.addTerm(1.0, zi);
						z2.addTerm(-1,pi);
						model.addConstr(z2,GRB.LESS_EQUAL, 0.0, "z2"+line.getId());
						GRBLinExpr z3 = new GRBLinExpr();
						z3.addTerm(1.0, zi);
						z3.addTerm(-1,pi);
						z3.addTerm(-capacity, zxi);
						model.addConstr(z3,GRB.GREATER_EQUAL, -capacity, "z3"+line.getId());
					}
					else {
						
						//	model.addVar(-line.getMaxCapacity()*0.795, line.getMaxCapacity()*0.795, 0, GRB.CONTINUOUS, line.getId());
						GRBVar pi;
						GRBVar zi;
						if(line.isMultipleConnection()) {
							pi = model.addVar(-line.getMaxCapacity()*0.795, line.getMaxCapacity()*0.795, 0, GRB.CONTINUOUS, line.getId());
							zi = model.addVar(-line.getMaxCapacity()*0.795, line.getMaxCapacity()*0.795, 0, GRB.CONTINUOUS, "Z"+line.getId());
						}
						else {
							pi = model.addVar(-line.getMaxCapacity(), line.getMaxCapacity(), 0, GRB.CONTINUOUS, line.getId());
							zi = model.addVar(-line.getMaxCapacity(), line.getMaxCapacity(), 0, GRB.CONTINUOUS, "Z"+line.getId());
						}
						
						
						/* -aup<= a <=M, and  zx  is binary, then:
						-aup*x1<= z<= aup*zx
						z <= a - -aup*(1 - zx)
						z >= a - aup*(1 - zx)*/
						double aup = line.getMaxCapacity();
						GRBLinExpr z1 = new GRBLinExpr();
						z1.addTerm(1.0, zi);
						z1.addTerm(aup, zxi);
						model.addConstr(z1,GRB.GREATER_EQUAL, 0, "z1"+line.getId());
						GRBLinExpr z2 = new GRBLinExpr();
						z2.addTerm(1.0, zi);
						z2.addTerm(-aup, zxi);
						model.addConstr(z2,GRB.LESS_EQUAL, 0, "z2"+line.getId());
						GRBLinExpr z4 = new GRBLinExpr();
						z4.addTerm(1.0, zi);
						z4.addTerm(-1.0, pi);
						z4.addTerm(aup, zxi);
						model.addConstr(z4,GRB.LESS_EQUAL, aup, "z4"+line.getId());
						GRBLinExpr z5 = new GRBLinExpr();
						z5.addTerm(1.0, zi);
						z5.addTerm(-1.0, pi);
						z5.addTerm(-aup, zxi);
						model.addConstr(z5,GRB.GREATER_EQUAL,-aup, "z5"+line.getId());
					}
				}

			}
			model.update();
			
			// Next setp: Kirchhoffs
			GRBLinExpr sumOfAllActivZero = new GRBLinExpr();
			GRBVar cRef = model.addVar(-Double.MAX_VALUE, 0, 0, GRB.CONTINUOUS, "cRef");
			for(PowerNode node: grid.getNodes()) {
				if(!node.isActivated()) {
					continue;
				}
				if(node instanceof Bus) {
					//Add the V variable for the voltage
					model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "V"+node.getName());
					
					// Kirchhoffs Law: sum of the currents = 0
					GRBLinExpr transformerGRB = new GRBLinExpr();
					for(PowerLine line: node.getIncomingLines()) {
						GRBVar z = model.getVarByName("Z"+line.getId());
						if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
							continue;
						}
						transformerGRB.addTerm(1.0, z);
					}
					for(PowerLine line: node.getOutgoingLines()) {
						if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
							continue;
						}
						GRBVar z = model.getVarByName("Z"+line.getId());
						transformerGRB.addTerm(-1.0, z);
					}
		
					if(grid.getReference() == null||!grid.getReference().isActivated()) {
						grid.setReference(node);
					}											 		 
					model.addConstr(transformerGRB,GRB.EQUAL, 0.0, "k"+node.getName());
				}
				else {
					// sum of all activ = 0. Produced = consumed + balance
					
					for(PowerLine line: node.getOutgoingLines()) {
						if(line.isActivated()&&line.getSink().isActivated()) {
							GRBVar z = model.getVarByName("Z"+line.getId());
							sumOfAllActivZero.addTerm(1.0, z);
						}
					}
					for(PowerLine line: node.getIncomingLines()) {
						if(line.isActivated()&&line.getSource().isActivated()) {
							GRBVar zx = model.getVarByName("zx"+line.getId());
							GRBVar c = model.getVarByName(line.getId());
							GRBLinExpr consumers = new GRBLinExpr();
							consumers.addTerm(1,c);	
							consumers.addTerm(-node.getCurrentCapacity(),cRef);	
							model.addConstr(consumers, GRB.EQUAL, 0.0, "consumer"+node.getName());
							sumOfAllActivZero.addTerm(node.getCurrentCapacity(),zx);
						}
					}

		
				}
			}
			model.update();
			sumOfAllActivZero.addTerm(-1.0, model.getVarByName("bal"));
			model.addConstr(sumOfAllActivZero, GRB.EQUAL, 0.0, "sumOfAllActivZero");
			
			// final step: ohms law
			for(PowerLine line: grid.getLines()) {
				if(!line.isActivated()) {
					continue;
				}
				if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
					continue;
				}
				if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
					// Ohms Law
					GRBVar v1 = model.getVarByName("V"+line.getSource().getName());
					GRBVar v2 = model.getVarByName("V"+line.getSink().getName());
					GRBLinExpr ohmLine = new GRBLinExpr();
					ohmLine.addTerm(-line.getReactance(), model.getVarByName("Z"+line.getId()));
					ohmLine.addTerm(1, v1);
					ohmLine.addTerm(-1, v2);
					model.addConstr(ohmLine, GRB.EQUAL, 0.0, "ohmLine"+line.getId());
					
				}
			}
		
			// Optimize
			model.setObjective(objectiveGRB, GRB.MINIMIZE);
			model.update();
			
			log("Connecting with Gurobi");
			long conn = System.nanoTime();
			model.optimize();
			log("Gurobi needed "+((System.nanoTime()-conn)/1000)+"us");

			int status = model.get(GRB.IntAttr.Status);
			if(status == 3) {
				// This should never be reached. There is always a solution in AbLaV
				log("INFEASABLE");
				model.dispose();
				env.dispose();
				resetChanges(sd);
				return -1;
			}
	 		
			 // Dispose of model and environment
			 model.dispose();
			 env.dispose();
													
			return 1;
	}
	
	// Returns a copy of the model
	/**
	 * Copies a SynergyDemo Object Manually (deep copy). Much more efficient than saving the model to xmi and loading it again.
	 * 
	 * @param sd The Object to copy
	 * @return
	 */
	public static SynergyDemo copyGraph(SynergyDemo sd) {
		
		// Load a basic model to save operations. This takes very little time
		String temp2 = "C:\\Users\\dedan\\Documents\\Masterarbeit\\runtime-workspace\\SynergyGUI3\\temp\\basicModel.xmi";
		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		SynergyDemo sd_copy2 = (SynergyDemo) eMoflonEMFUtil.loadModel(temp2);
		
		sd_copy2.setCopy(true);
		sd_copy2.setDay(0);
		EnergyMarket em = sd_copy2.getMarket();
		EnergyGrid eg = sd_copy2.getGrid();
		eg.getNodes().clear();
		eg.getLines().clear();
		eg.setBalance(sd.getGrid().getBalance());
		eg.setBalanceTolerance(sd.getGrid().getBalanceTolerance());
		
		HashMap<PowerNode, PowerNode> nodeMap = new HashMap<>();
		for(PowerNode node1 : sd.getGrid().getNodes()) {
			PowerNode newNode;
			if(node1 instanceof Producer) {
				Producer p = (Producer)node1;
				
				if(!(node1 instanceof SolarPlant || node1 instanceof WindPlant)) {
					newNode = factory.createCoalPlant();
				}
				else {
					if(node1 instanceof WindPlant)newNode= factory.createWindPlant();
					else newNode= factory.createWindPlant();
				}
				Producer pn = (Producer)newNode;
				pn.setMaxProduction(p.getMaxProduction());
				pn.setPowerChanged(p.getPowerChanged());
				pn.setPowerDownSpeed(p.getPowerDownSpeed());
				pn.setPowerUpSpeed(p.getPowerUpSpeed());
				pn.setPriceProMW(p.getPriceProMW());
			}
			else if (node1 instanceof Consumer) {
				Consumer p = (Consumer)node1;
				
				if(node1 instanceof City) {
					newNode = factory.createCity();
				}
				else {
					newNode= factory.createFactory();
				}
				Consumer pn = (Consumer)newNode;
				pn.setPriceProMW(p.getPriceProMW());
			}
			else {
				newNode= factory.createBus();
			}
			newNode.setActivated(node1.isActivated());
			newNode.setCurrentCapacity(node1.getCurrentCapacity());
			newNode.setMaxCapacity(node1.getMaxCapacity());
			newNode.setName(node1.getName());
			newNode.setGrid(eg);
			newNode.setZone(node1.getZone());
			nodeMap.put(node1, newNode);
		}

		for(PowerLine pl : sd.getGrid().getLines()) {
			PowerLine pln = factory.createPowerLine();
			pln.setActivated(pl.isActivated());
			pln.setCurrentCapacity(pl.getCurrentCapacity());
			pln.setGrid(eg);
			pln.setId(pl.getId());
			pln.setMaxCapacity(pl.getMaxCapacity());
			pln.setMultipleConnection(pl.isMultipleConnection());
			pln.setNMinusOneSecure(pl.isNMinusOneSecure());
			pln.setReactance(pl.getReactance());
			pln.setSink(nodeMap.get(pl.getSink()));
			pln.setSource(nodeMap.get(pl.getSource()));
			nodeMap.get(pl.getSink()).getIncomingLines().add(pln);
			nodeMap.get(pl.getSource()).getOutgoingLines().add(pln);
		}
		if(sd.getMarket()!=null) {
			for(Contract c : sd.getMarket().getContracts()) {
				Contract c_copy = factory.createContract();
				c_copy.setCapacity(c.getCapacity());
				c_copy.setDay(c.getDay());
				c_copy.setEndTime(c.getEndTime());
				em.getContracts().add(c_copy);
				c_copy.setMarket(em);
				c_copy.setNode(nodeMap.get(c.getNode()));
				c_copy.setStartTime(c.getStartTime());
			}
			for(Offering o : sd.getMarket().getOfferings()) {
				Offering o2 = factory.createOffering();
				o2.setCapacity(o.getCapacity());
				o2.setDay(o.getDay());
				o2.setEndTime(o.getEndTime());
				em.getOfferings().add(o2);
				o2.setNode(nodeMap.get(o.getNode()));
				o2.setStartTime(o.getStartTime());
				o2.setActive(o.isActive());
				o2.setPrice(o.getPrice());
				o2.setMarket(em);
			}
		}
		return sd_copy2;
	}
	
	
	/**
	 * Checks if a live Optimization is needed because of conflicts
	 * 
	 * @param synergyDemoRules     The API to update the matches and check
	 * @param sd                   The Network to check
	 * @param timeValue            
	 * @param randomizer
	 */
	public static void checkOptimization(SynergyDemoRules synergyDemoRules, SynergyDemo sd) {
		if(synergyDemoRules.isConflictive(sd)) {
			log("Problem found. Perform reactive optimization");
			performReactiveOptimization(sd,true,1,false,false);
			reactiveOptimizationCount ++;
			log("Analysing model...");
			synergyDemoRules.analyseGraph(sd);
		}
		else {
			log("There are no conflicts in the network. No optimization needed");
		}
	}
	
	
	/**
	 * Checks if there are planned changes in the past for the given timestepsNum. Used to check if the proactive 
	 * opitimization can be done or not.
	 * 
	 * @param sd
	 * @param timestepsNum
	 * @param day
	 * @param timestep
	 * @return
	 */
	public static boolean areTherePastChanges(SynergyDemo sd, int timestepsNum, int day, int timestep) {
		HashSet<ScheduledChange> proactives = new HashSet<>();
		HashSet<ScheduledChange> reactives = new HashSet<>();
				
		for(int i=0; i<timestepsNum; i++) {
			for(ScheduledChange change : sd.getPowerPlan().getProactiveChanges()) {
				if(change.getDay()==day&&change.getTimeUnit()==timestep) {
					proactives.add(change);
				}
			}
			for(ScheduledChange change : sd.getPowerPlan().getReactiveChanges()) {
				if(change.getDay()==day&&change.getTimeUnit()==timestep) {
					reactives.add(change);
				}
			}
			if(timestep == 0) {
				day--;
			}
			timestep--;
		}
		
		return (proactives.size()+reactives.size())>0;
		

			
		
	}
	
	/**
	 * Calculates the plan for the next day in the given network
	 * 
	 * @param sd               the network
	 * @param randomizer       the randomizer to predict the changes in the network
	 */
	/**
	 * @param sd
	 * @param randomizer
	 */
	public static void calculateDayPlan(SynergyDemo sd, Randomizer randomizer) {
		
		// We need to store copies of the graph for the past 2 Timesteps. In case that it is needed for proactive
		String copyInt23 = "C:\\Users\\dedan\\Documents\\Masterarbeit\\runtime-workspace\\SynergyGUI3\\temp\\"+System.identityHashCode(sd)+"\\modelAt23.xmi";
		String copyIntMinus1 = "C:\\Users\\dedan\\Documents\\Masterarbeit\\runtime-workspace\\SynergyGUI3\\temp\\"+System.identityHashCode(sd)+"\\modelAtMinus1.xmi";
		String copyIntMinus2 = "C:\\Users\\dedan\\Documents\\Masterarbeit\\runtime-workspace\\SynergyGUI3\\temp\\"+System.identityHashCode(sd)+"\\modelAtMinuw2.xmi";
		SynergyDemo sd_copy = null;
		SynergyDemo sd_t1 = null;
		SynergyDemo sd_t2 = null;
		
		// if != -1, than make the plan for the next day (it is 15:00)
		if(sd.getTimeUnit()!=-1) {
			// Load the graphs from last plan
			try {
				sd_copy = (SynergyDemo) eMoflonEMFUtil.loadModel(copyInt23);
				sd_t1 = (SynergyDemo) eMoflonEMFUtil.loadModel(copyIntMinus1);
				sd_t2 = (SynergyDemo) eMoflonEMFUtil.loadModel(copyIntMinus2);
				sd_copy.setDay(sd.getDay()+1);
			}
			catch(Exception e) {
				log(e.getMessage());
			}
		}
		// First timestep of all t=-1, make copy of the graph
		else {
			sd_copy = copyGraph(sd);
			sd_copy.setTimeUnit(-1);
			sd_copy.setDay(0);

		}
		
		int timeValue = -1;
		
		// There shouldnt be any programmed changes in the copies
		sd_copy.getPowerPlan().getReactiveChanges().clear();
		sd_copy.getPowerPlan().getProactiveChanges().clear();
		if(sd_t1!=null) {
			sd_t1.getPowerPlan().getReactiveChanges().clear();
			sd_t1.getPowerPlan().getProactiveChanges().clear();
		}
		if(sd_t2!=null) {
			sd_t2.getPowerPlan().getReactiveChanges().clear();
			sd_t2.getPowerPlan().getProactiveChanges().clear();
		}
		
		// Mapping of nodes for accesing the nodes of the original graph easily
		HashMap<String,PowerNode> nodeMap = new HashMap<>();
		for(PowerNode n : sd.getGrid().getNodes()) {
			nodeMap.put(n.getName(), n);
		}
		
		// API for the IGPM
		SynergyDemoRules synergyDemoRules = new SynergyDemoRules(sd_copy);
		synergyDemoRules.analyseGraph(sd_copy); 
		
		long time = System.nanoTime();
		long timestepMean = 0;
		
		// Make this for all timesteps of next day in the copy
		while(timeValue<23){
			
			long timeTimestep = System.nanoTime();
			timeValue++;
			sd_copy.setTimeUnit(timeValue);
			System.out.println();
			
			log("Prediction for Timestep "+timeValue);
			log("Reset due Offerings");
			resetOfferings(sd_copy, timeValue);
			
			// Apply Environment Changes
    		randomizer.predict(sd_copy,timeValue, sd_copy.getDay());
    		
    		// Simulate after the predictions to generate new values in Lines
			try {
				Simulator.GurobiSolve(sd_copy.getGrid());
			} catch (GRBException e) {
				e.printStackTrace();
			}

			// Analyse the graph to see if there are conflicts
			synergyDemoRules.analyseGraph(sd_copy);
			boolean conflictive = synergyDemoRules.isConflictive(sd_copy);
			if(conflictive) {
				log("Network is conflictive");
			}
			HashMap<String,Double> nodesToMatch = new HashMap<>();
			// We have to make an Optimization here no matter if there are conflicts or not, because 
			// if there are contracts, we have to change the values to match them
			int timestepsneeded = Optimizer.performProactiveOptimization(sd_copy,3,false,false,nodesToMatch);
			// For each timestepneeded > 1 must be done a correction in the past
			// If the network is in the first day, the first timestep, and we need to change in the past,
			// we optimize directly without restrictions because the past doesnt exist.
			if (timestepsneeded > 1 && sd_copy.getDay() == 0 && sd_copy.getTimeUnit() == 0) {
				nodesToMatch.clear();
				Optimizer.performProactiveOptimization(sd_copy,3,false,true,nodesToMatch);
				// Predict or randomize the main graph to match the copy
				randomizer.predict(sd,timeValue, sd_copy.getDay());
				Optimizer.performProactiveOptimization(sd,3,false,true,nodesToMatch);
			}
			else if (timestepsneeded > 2 && sd_copy.getDay() == 0 && sd_copy.getTimeUnit() == 1) {
				// If 2nd Timestep of first day and we need 3 timesteps, just correct it because is impossible
				nodesToMatch.clear();
				Optimizer.performProactiveOptimization(sd_copy,3,false,true,nodesToMatch);
				randomizer.predict(sd,timeValue, sd_copy.getDay());
				Optimizer.performProactiveOptimization(sd,3,false,true,nodesToMatch);
			}
			else if (timestepsneeded == 3 && !areTherePastChanges(sd, 3, sd_copy.getDay(), sd_copy.getTimeUnit())) {
				// Redo in the past
				log("Redoing from timestep "+(timeValue-2));
				log("Reset due Offerings");
				// Load the network 2 timesteps ago.
				try {
					sd_t2 = (SynergyDemo) eMoflonEMFUtil.loadModel(copyIntMinus2);
				}
				catch(Exception e) {
					log(e.getMessage());
				}
				// Make t -2
				resetOfferings(sd_t2, sd_t2.getTimeUnit());
 				Optimizer.performProactiveOptimization(sd_t2,1,true,false,nodesToMatch);
				copyProactiveChanges(sd, sd_t2, timeValue, nodeMap);
				SynergyDemoRules synergyDemoRules1 = new SynergyDemoRules(sd_t2);
				synergyDemoRules1.analyseGraph(sd_t2);
				incrementTimeUnit(sd_t2);
				
				// make t-1
				log("Redoing from timestep "+(timeValue-1));
				log("Reset due Offerings");
				resetOfferings(sd_t2, sd_t2.getTimeUnit());
				randomizer.predict(sd_t2,sd_t2.getTimeUnit(), sd_copy.getDay()); // make it quiet
				Optimizer.performProactiveOptimization(sd_t2,1,true,false,nodesToMatch);
				copyProactiveChanges(sd, sd_t2, timeValue, nodeMap);
				synergyDemoRules1.analyseGraph(sd_t2);
				incrementTimeUnit(sd_t2);
				
				// make t
				log("Redoing from timestep "+(timeValue));
				log("Reset due Offerings");
				resetOfferings(sd_t2, sd_t2.getTimeUnit());
				randomizer.predict(sd_t2,sd_t2.getTimeUnit(), sd_copy.getDay()); // make it quiet
				nodesToMatch.clear();
				Optimizer.performProactiveOptimization(sd_t2,1,true,false,nodesToMatch);
				synergyDemoRules1.analyseGraph(sd_t2);
				copyProactiveChanges(sd, sd_t2, timeValue, nodeMap);
				// After doing the changes, sd_t2 is now sd_copy
				sd_copy = sd_t2;
				sd_t2 = null;
				sd_t1 = null;
				synergyDemoRules = synergyDemoRules1;
			}
			else if (timestepsneeded == 2 && !areTherePastChanges(sd, 2, sd_copy.getDay(), sd_copy.getTimeUnit())) {
				// The same as above but this time only one step to the past
				log("Redoing from timestep "+(timeValue-1));
				log("Reset due Offerings");
				try {
					sd_t1 = (SynergyDemo) eMoflonEMFUtil.loadModel(copyIntMinus1);
				}
				catch(Exception e) {
					log(e.getMessage());
				}
				resetOfferings(sd_t1, sd_t1.getTimeUnit());
 				Optimizer.performProactiveOptimization(sd_t1,1,true,false,nodesToMatch);
				copyProactiveChanges(sd, sd_t1, timeValue, nodeMap);
				nodesToMatch.clear();
				SynergyDemoRules synergyDemoRules1 = new SynergyDemoRules(sd_t1);
				synergyDemoRules1.analyseGraph(sd_t1);
				incrementTimeUnit(sd_t1);
				log("Redoing from timestep "+(timeValue));
				log("Reset due Offerings");
				resetOfferings(sd_t1, sd_t1.getTimeUnit());
				randomizer.predict(sd_t1,sd_t1.getTimeUnit(), sd_copy.getDay()); // make it quiet
				nodesToMatch.clear();
				Optimizer.performProactiveOptimization(sd_t1,1,true,false,nodesToMatch);
				synergyDemoRules1.analyseGraph(sd_t1);
				copyProactiveChanges(sd, sd_t1, timeValue, nodeMap);
				sd_copy = sd_t1;
				sd_t1 = null;
			}
			// if only one timestep, the optimization is already done
			else {
				// Save the model t-1 as t-2
				Path from = Path.of(copyIntMinus1); //convert from File to Path
				Path to = Path.of(copyIntMinus2); //convert from String to Path
				try {
					Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
				}					
				// Save the current model as t-1
				eMoflonEMFUtil.saveModel(sd_copy,copyIntMinus1);
				
				nodesToMatch.clear();
				//Optimizer.performProactiveOptimization(sd_copy, 1, true,false,nodesToMatch);
				copyProactiveChanges(sd, sd_copy, timeValue, nodeMap);
			}
			synergyDemoRules.analyseGraph(sd_copy);
						
			if(timeValue==23) {
				// Save the graph for the next day
				eMoflonEMFUtil.saveModel(sd_copy,copyInt23);
			}
			timestepMean+=(System.nanoTime()-timeTimestep)/1000;
			
		}
		log("Time for Planing time: "+(System.nanoTime()-time)/1000+"us");
		log("Mean of timestep for Planing time: "+(timestepMean/24)+"us");
		
		// At the end, Copy all the proactive changes in the main graph
		copyProactiveChanges(sd, sd_copy, timeValue, nodeMap);
	}
	
	/**
	 * Resets the due offerings
	 * 
	 * @param sd
	 * @param timeValue
	 */
	public static void resetOfferings(SynergyDemo sd, int timeValue) {
		for(Offering o : sd.getMarket().getOfferings()) {
			if(o.isActive()&&(o.getDay()<sd.getDay()||o.getEndTime()<sd.getTimeUnit())) {
				o.setActive(false);
				log("Offering of "+o.getNode().getName()+" ended. Reset to "+(o.getNode().getCurrentCapacity()-o.getCapacity()));
				o.getNode().setCurrentCapacity(o.getNode().getCurrentCapacity()-o.getCapacity());
			}
		}
	}

	/**
	 * increments the object in one day
	 *  
	 * @param o   SynergyDemo or ScheduledChange Object
	 */
	public static void incrementTimeUnit(Object o) {
		SynergyDemo sd;
		ScheduledChange sc;
		if(o instanceof SynergyDemo) {
			sd = (SynergyDemo) o;
		
			if(sd.getTimeUnit()==23) {
				sd.setTimeUnit(0);
				sd.setDay(sd.getDay()+1);
			}
			else {
				sd.setTimeUnit(sd.getTimeUnit()+1);
			}
		}
		else if(o instanceof ScheduledChange) {
			sc = (ScheduledChange) o;
			
			if(sc.getTimeUnit()==23) {
				sc.setTimeUnit(0);
				sc.setDay(sc.getDay()+1);
			}
			else {
				sc.setTimeUnit(sc.getTimeUnit()+1);
			}
		}
		else {
			log("Element cannot be incremented!");
		}
		
	}
	
	
	/**
	 * Decrement one unit of time for the object
	 * 
	 * @param o
	 */
	public static void decrementTimeUnit(Object o) {
		SynergyDemo sd;
		ScheduledChange sc;
		if(o instanceof SynergyDemo) {
			sd = (SynergyDemo) o;
		
			if(sd.getTimeUnit()==0) {
				sd.setTimeUnit(23);
				sd.setDay(sd.getDay()-1);
			}
			else {
				sd.setTimeUnit(sd.getTimeUnit()-1);
			}
		}
		else if(o instanceof ScheduledChange) {
			sc = (ScheduledChange) o;
			
			if(sc.getTimeUnit()==0) {
				sc.setTimeUnit(23);
				sc.setDay(sc.getDay()-1);
			}
			else {
				sc.setTimeUnit(sc.getTimeUnit()-1);
			}
		}
		else {
			log("Element cannot be incremented!");
		}
	}
	
	
	/**
	 * Copy proactive changes from one graph (copy) to the other (main)
	 * 
	 * @param sd
	 * @param sd_copy
	 * @param timeValue
	 * @param nodeMap
	 */
	public static void copyProactiveChanges(SynergyDemo sd, SynergyDemo sd_copy, int timeValue, HashMap<String, PowerNode> nodeMap) {
 		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		for(ScheduledChange change : sd_copy.getPowerPlan().getProactiveChanges()) {
				ScheduledChange proactive_change = factory.createScheduledChange();
				proactive_change.setNode(nodeMap.get(change.getNode().getName()));
				proactive_change.setTimeUnit(change.getTimeUnit());
				proactive_change.setDay(change.getDay());
				proactive_change.setActivationChange(false);
				proactive_change.setValue(change.getValue());
				proactive_change.setProactivePlan(sd.getPowerPlan());
		}
		 sd_copy.getPowerPlan().getProactiveChanges().clear();
	}
	
	
	/**
	 * Copies all reactive changes to the proactive changes (used in copies, because in copies the reactive changes are proactive changes in the main graph)
	 * 
	 * @param sd
	 * @param change
	 */
	public static void copyReactiveChangesToProactiveChanges(SynergyDemo sd, ScheduledChange change) {
		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		ScheduledChange proactive_change = factory.createScheduledChange();
		proactive_change.setNode(change.getNode());
		proactive_change.setTimeUnit(change.getTimeUnit());
		proactive_change.setDay(change.getDay());
		proactive_change.setActivationChange(false);
		proactive_change.setValue(change.getValue());
		proactive_change.setProactivePlan(sd.getPowerPlan());
	}
	
	private static void log(final String message) {
		Logger.getRootLogger().info("Optimization: " + message);
	}
	
	
	/**
	 * Returns the number of timesteps needed if a solution is found. -1 if there is no solution
	 * 
	 * @param sd               The System to perform the linear programming optimization
	 * @param maxTimesteps     Maximum number of timesteps of the solution
	 * @param fillPlan         Whether or not planning the proactive changes of the optimization.
	 * @param applyAllChanges  Whether or not applying directly the changes to the grid
	 * @param nodesToMatch     Additional Adjustments for the optimization to match certain values on certain nodes 
	 * @return                 Number of timesteps of solution if one is found. -1 if there is no solution.
	 * @throws GRBException
	 */
	public static int proactiveOptimizationGurobi(SynergyDemo sd, int maxTimesteps, boolean fillPlan, boolean applyAllChanges, HashMap<String, Double> nodesToMatch) throws GRBException {

		// Create empty Gurobi environment, set options, and start
		GRBEnv env = new GRBEnv(true);
	    env.set("logFile", "mip1.log");
	    env.set(GRB.IntParam.OutputFlag, 0);
	    env.set(GRB.IntParam.LogToConsole, 0);
	    env.start();
	
	    // Create empty Gurobi Model
	    GRBModel  model = new GRBModel(env);
	   
		// Get the grid from the System
		EnergyGrid grid = sd.getGrid();
	    
	    // Collect the contracts in a Map, to easy the access.
	    Map<PowerNode, Contract> producerContracts = new HashMap<>();
	    for(Contract c:sd.getMarket().getContracts()) {
	    	if(c.getDay() == sd.getDay()) {
		    	if(sd.getTimeUnit()>=c.getStartTime()&&sd.getTimeUnit()<=c.getEndTime()&&sd.getDay()==c.getDay()) {
		    		producerContracts.put(c.getNode(), c);
		    	}
	    	}
	    }
		
	
		
		// Balance should be 0 when planning.
	    // Everything produced is consumed. Because of the contracts
		// But if it is impossible because of bottlenecks, some imbalance is tolerated
	    double balanceTolerance = sd.getGrid().getBalanceTolerance();
	    GRBVar bal = model.addVar(-balanceTolerance, balanceTolerance, 0, GRB.CONTINUOUS, "bal");
	    GRBVar balAbs = model.addVar(0, balanceTolerance, 0, GRB.CONTINUOUS, "balAbs");
	    model.addGenConstrAbs(balAbs, bal, "balAbsConst");
		model.update();
	    
	    // Objective Function expression
	    GRBLinExpr objectiveGRB = new GRBLinExpr();
	    objectiveGRB.addTerm(1000.0, model.getVarByName("balAbs"));	    
	
	    // Maps each node with its offerings for this timestep
	    HashMap<PowerNode,LinkedList<Offering>> mapOfferings = new HashMap<>();
	    
	    // If a offering is beeing used, another offering is placed to offer the stop of the offering.
	    // If the offering is from t = 5 to t = 10, the offering can be for example activated in t = 5 and desactivated in t = 7
	    HashMap<Offering, Offering> mapOfEquivalentOfferings = new HashMap<>();
	
	    for(Offering o : sd.getMarket().getOfferings()) {
	    	if(sd.getTimeUnit()>=o.getStartTime()&&sd.getTimeUnit()<=o.getEndTime()&&sd.getDay()==o.getDay()) {
				// If no active, can be activated.
	    		if(!o.isActive()) {
	    			// There can be more than one offering for the node and the timestep, so make a list
		    		LinkedList<Offering> offeringsForNode = mapOfferings.get(o.getNode());
		    		if(offeringsForNode==null) {
		    			offeringsForNode = new LinkedList<>();
		    		}
		    		offeringsForNode.add(o);
		    		mapOfferings.put(o.getNode(), offeringsForNode);
	    		}
	    		// If activated, can be deactivated
	    		else {
	    			LinkedList<Offering> offeringsForNode = mapOfferings.get(o.getNode());
		    		if(offeringsForNode==null) {
		    			offeringsForNode = new LinkedList<>();
		    		}
		    		
		    		// Create a offering for offering the stop of the offering. Set market = null to differenciate it from the normal ones
		    		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		    		Offering oBack = factory.createOffering();
		    		oBack.setCapacity(-o.getCapacity());
		    		oBack.setNode(o.getNode());
		    		oBack.setMarket(null);
		    		offeringsForNode.add(oBack);
		    		mapOfferings.put(o.getNode(), offeringsForNode);
		    		
		    		// Add the mapping with the activated optimization
		    		mapOfEquivalentOfferings.put(oBack, o);
	    		}
	    	}
	    }
	    
	    // For mapping the Gurobi Variable with the corresponding offer
	    HashMap<String,Offering> mapVariableOfferings = new HashMap<>();
	    	    
	    // Reference variable for the consumers
	    GRBVar cRef = model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "cRef");
	    
	    // How expensive is to use DSM. Small values priorize DSM Changes
		double demandSideImportance = 10; 
		// How important is to stay close to the contracts. Big values to stick to plan
		double planImportance = 10000.0; 
		// How important is to change the less possible. Big Values means trying to change less.
		double changeImportance = 100.0; // Make changes if necessary
		// Generic variable for the importance
		double importance;
	    
		// Go through all lines to make the variables and the constrains
		for(PowerLine line: grid.getLines()) {
			// Line must be activated, and both source and sink aswell. If not, Line doesnt transfer energy
			if(line.isActivated()&&line.getSource().isActivated()&&line.getSink().isActivated()) {
				// The lines that connect producers
				if(line.getSource() instanceof Producer)  {
					Producer producer = (Producer) line.getSource();
						double lb; // lower bound for Producer
						double ub; // upper bound for Producer
						GRBVar pi; // Value to calculate for the producer
						double piv; // Desired value of the producer. The value of the contract if there is one, if not, the current value.
						GRBVar aux; // piv - pi
						GRBVar aux2; // | piv - pi |
	
						// Calculate lower and upper bound for the producer
						double lbex = producer.getCurrentCapacity()-maxTimesteps*((Producer)producer).getPowerDownSpeed();
						double ubex = producer.getCurrentCapacity()+maxTimesteps*((Producer)producer).getPowerUpSpeed();
						lb = Math.max(0,lbex);
						ub = Math.min(producer.getMaxCapacity(),ubex);
						
						// declare variables with lower and upper bounds
						aux = model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "aux"+line.getId()) ;// auxiliary variable
						aux2 = model.addVar(0,Double.MAX_VALUE, 0, GRB.CONTINUOUS, "auxAbs"+line.getId()) ;// auxiliary variable absolute
						pi = model.addVar(lb, ub, 0, GRB.CONTINUOUS, line.getId());
						
						// if there are a contract for the producer, set piv
						if(producerContracts.containsKey(producer)) {
							Contract c = producerContracts.get(producer);
							
							// But if the producer has additional requirements, ignore the contract
							// This is used to solve a problem in the future. The contracts are no longer the priority
							if(nodesToMatch!=null && nodesToMatch.get(producer.getName())!=null) {
								piv = nodesToMatch.get(producer.getName());//),nodesToMatch.get(producer.getName()),0, GRB.CONTINUOUS, "O"+line.getId());
							}
							else {
								piv = c.getCapacity();//, c.getCapacity(),0, GRB.CONTINUOUS, "O"+line.getId());
							}
							importance = changeImportance;
						}
						else {
							// Producer has no contract... it must remain the same or match the desired
							if(nodesToMatch!=null && nodesToMatch.get(producer.getName())!=null) {
								piv = nodesToMatch.get(producer.getName());
								importance = planImportance;
							}
							else {
								piv = producer.getCurrentCapacity();
								importance = changeImportance;
							}
						}												  
						GRBLinExpr auxExpr = new GRBLinExpr();
						// aux = piv - pi
						auxExpr.addTerm(1, pi);
						auxExpr.addTerm(-1, aux);
						model.addConstr(auxExpr,GRB.EQUAL, piv, "aux"+line.getId());
						//aux2 = abs(aux)
						model.addGenConstrAbs(aux2, aux, "auxAbs"+line.getId());
						// How important is to change the less possible
						// How important is to change the EEG first								 
						double priceOfEnergy = ((Producer)producer).getPriceProMW();
						// Add terms to the objective function
						objectiveGRB.addTerm(importance, aux2);
						objectiveGRB.addTerm(1/priceOfEnergy, aux2);
				}
				
				// The lines that connect consumers
				else if(line.getSink() instanceof Consumer) {
					Consumer consumer = (Consumer) line.getSink();
					
					// We dont know the limits of the values. It could be infinity, if the production is infinity
					model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, line.getId());
					
					// if there is offerings for this consumer, add the variables to offer them
					if(mapOfferings.containsKey(consumer)) {
						int oNumber = 0;
						for(Offering o : mapOfferings.get(consumer)) {
							
							// add the mapping for later access
							mapVariableOfferings.put("zonr"+oNumber+line.getId(), o);
							
							double ub;
							// double.MAX_VALUE gives overflow problems
							ub = Math.pow(10,9); // Producers shouldnt go over 10^9 MW
							GRBVar zoiSigned = model.addVar(-Double.MAX_VALUE,Double.MAX_VALUE, 0, GRB.INTEGER, "zos"+oNumber+line.getId());
							GRBVar poi = model.addVar(0,Double.MAX_VALUE, 0, GRB.CONTINUOUS, "o"+oNumber+line.getId());
							GRBVar xoi = model.addVar(0, 1, 0, GRB.BINARY, "xo"+oNumber+line.getId());
							GRBVar zoi = model.addVar(0,Double.MAX_VALUE, 0, GRB.CONTINUOUS, "zo"+oNumber+line.getId());
							GRBVar zoni = model.addVar(-Math.abs(o.getCapacity()),Math.abs(o.getCapacity()), 0, GRB.CONTINUOUS, "zonr"+oNumber+line.getId());
							// zoi <= ub*xoi
							GRBLinExpr xoexpr1 = new GRBLinExpr();
							xoexpr1.addTerm(1, zoi);
							xoexpr1.addTerm(-ub,xoi);
							model.addConstr(xoexpr1,GRB.LESS_EQUAL, 0.0, "xoexpr1"+oNumber+line.getId());
							// zoi >= poi + ub*(xoi -1) 
							GRBLinExpr xoexpr2 = new GRBLinExpr();
							xoexpr2.addTerm(1, zoi);
							xoexpr2.addTerm(-1,poi);
							xoexpr2.addTerm(-ub,xoi);
							// zoi <= poi
							model.addConstr(xoexpr2,GRB.GREATER_EQUAL, -ub, "xoexpr2"+oNumber+line.getId());
							GRBLinExpr xoexpr3 = new GRBLinExpr();
							xoexpr3.addTerm(1, zoi);
							xoexpr3.addTerm(-1,poi);
							model.addConstr(xoexpr3,GRB.LESS_EQUAL, 0.0, "xoexpr3"+oNumber+line.getId());
							// without reference adjust
							// zoni = xoi*Con
							GRBLinExpr xoexpr4 = new GRBLinExpr();
							xoexpr4.addTerm(1, zoni);
							xoexpr4.addTerm(-o.getCapacity(),xoi);
							model.addConstr(xoexpr4,GRB.EQUAL, 0.0, "xoexpr4"+oNumber+line.getId());
							// zosign = sign(zoi)
							GRBLinExpr zosign = new GRBLinExpr();
							zosign.addTerm(1, zoi);
							zosign.addTerm(-Math.signum(o.getCapacity()), zoiSigned);
							model.addConstr(zosign,GRB.EQUAL, 0, "signumExp"+oNumber+line.getId());
							
							// using zoi has a negative impact on the objective function. This negative impact has a coefficient = demandSideImportance
							objectiveGRB.addTerm(demandSideImportance, zoi);
							oNumber++;
						}
					}
				}
				// lines that connect 2 Buses
				else {
					// If is a multiple connection, make sure the line is N-1 Secure
					if(line.isMultipleConnection()) {
						model.addVar(-line.getMaxCapacity()*0.795, line.getMaxCapacity()*0.795, 0, GRB.CONTINUOUS, line.getId());
					}
					// If not, just stay between the maximum values (if negative, change later the direction of the line)
					else {
						model.addVar(-line.getMaxCapacity(), line.getMaxCapacity(), 0, GRB.CONTINUOUS, line.getId());
					}
				}
			}
	
		}
		model.update();
		
		// Expression for produced = consumed + balance
		GRBLinExpr sumOfAllActivZero = new GRBLinExpr();
		double allConsumers = 0;
		
		for(PowerNode node: grid.getNodes()) {
			if(!node.isActivated()) {
				continue;
			}
			// if bus, apply kirchhoff
			if(node instanceof Bus) {
				// Variable for the Potential on the Buses
				model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "V"+node.getName());
				// Expression for the Kirchhoffs law in the Bus
				GRBLinExpr transformerGRB = new GRBLinExpr();
				for(PowerLine line: node.getIncomingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					// sum(incomming)
					transformerGRB.addTerm(1.0, model.getVarByName(line.getId()));
				}
				for(PowerLine line: node.getOutgoingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					// sum(outgoing)
					transformerGRB.addTerm(-1.0, model.getVarByName(line.getId()));
					
					// The offerings must be added to the Kirchhoffs sum
					if(line.getSink() instanceof Consumer) {
						int oNumber = 0;
						while (model.getVarByName("zos"+oNumber+line.getId())!=null) {
							GRBVar zovarsign = model.getVarByName("zos"+oNumber+line.getId());
							if(zovarsign != null) {
								transformerGRB.addTerm(1.0, zovarsign);
							}
							oNumber++;
						}
					}
					
				}
														 
				model.addConstr(transformerGRB,GRB.EQUAL, 0.0, "k"+node.getName());
			}
			// if not make formula: produced = consumed + balance
			else {
				for(PowerLine line: node.getOutgoingLines()) {
					if(line.isActivated()&&line.getSink().isActivated()) {
						sumOfAllActivZero.addTerm(1.0, model.getVarByName(line.getId()));
					}
				}
				for(PowerLine line: node.getIncomingLines()) {
					if(line.isActivated()&&line.getSource().isActivated()) {
						GRBVar c = model.getVarByName(line.getId());
						GRBLinExpr consumers = new GRBLinExpr();
						consumers.addTerm(1,c);	
						consumers.addTerm(node.getCurrentCapacity(),cRef);	
						model.addConstr(consumers, GRB.EQUAL, 0.0, "consumer"+node.getName());
						allConsumers-=node.getCurrentCapacity();
						
						// The offerings must be also added to the formula: produced = consumed + balance
						if(mapOfferings.containsKey(line.getSink())) {
							int oNumber = 0;
							for (Offering o : mapOfferings.get(line.getSink())) {
								
								GRBVar ovar = model.getVarByName("o"+oNumber+line.getId());
								GRBVar zonr = model.getVarByName("zonr"+oNumber+line.getId());
								GRBLinExpr consumerso = new GRBLinExpr();
								consumerso.addTerm(1,ovar);
								
								consumerso.addTerm(-Math.abs(o.getCapacity()),cRef);
								model.addConstr(consumerso, GRB.EQUAL, 0.0, "consumero"+oNumber+node.getName());
	
								sumOfAllActivZero.addTerm(1, zonr);
								oNumber++;
							}
						}
					}
				}
			}
		}
		model.update();
		sumOfAllActivZero.addTerm(-1.0, model.getVarByName("bal"));
		
		model.addConstr(sumOfAllActivZero, GRB.EQUAL, allConsumers, "sumOfAllActivZero");
		
		// finally, go again over the lines to make the Ohms law
		// this cannot be made in the first loop over the lines, because not all the variables were declared
		for(PowerLine line: grid.getLines()) {
			if(!line.isActivated()) {
				continue;
			}
			if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
				continue;
			}
			if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
	
				GRBLinExpr ohmLine = new GRBLinExpr();
				ohmLine.addTerm(-line.getReactance(), model.getVarByName(line.getId()));
				ohmLine.addTerm(1, model.getVarByName("V"+line.getSource().getName()));
				ohmLine.addTerm(-1, model.getVarByName("V"+line.getSink().getName()));
				model.addConstr(ohmLine, GRB.EQUAL, 0.0, "ohmLine"+line.getId());
			}
		}
		
		// Optimize and get the result
		model.setObjective(objectiveGRB, GRB.MINIMIZE);
		log("Connecting with Gurobi");
		long conn = System.nanoTime();
		model.optimize();
		log("Gurobi needed "+((System.nanoTime()-conn)/1000)+"us");
		int status = model.get(GRB.IntAttr.Status);
		
		// if no solution, return -1
		if(status == 3) {
			resetChanges(sd);
			model.dispose();
			env.dispose();
			return -1;
		}
		
		// If this reached. There is a solution. Plan or solve
		int timestepsNeeded = 0; 
		if(fillPlan) {
			timestepsNeeded = createPlan(model, sd, mapVariableOfferings, mapOfEquivalentOfferings); // Only one step of plan.
		}
		else {
			timestepsNeeded = solveNetwork(model, sd, 10, applyAllChanges, nodesToMatch, false);
			
			// no apply changes and no fillPlan is the first check of the proactive optimization
			// if the timestepsNeeded are only 1, make the plan directly here
			if(timestepsNeeded == 1 && !applyAllChanges) {
				timestepsNeeded = createPlan( model, sd, mapVariableOfferings, mapOfEquivalentOfferings); // Only one step of plan.
			}
		}
		// Dispose of model and environment
		model.dispose();
		env.dispose();
		
		return timestepsNeeded;
	   
	}
	
	/**
	 * Calculates the Entschädigung as the difference between contracts and actual  (or difference with last step if no contract)
	 * 
	 * @param sd
	 * @param lastValues
	 * @return
	 */
	public static int calculateEntschaedigung(SynergyDemo sd, HashMap<PowerNode, Double> lastValues) {
		
		HashMap<PowerNode, Double> contractMap = new HashMap<>();
		HashMap<PowerNode, Double> offeringMap = new HashMap<>();
		for(Contract c : sd.getMarket().getContracts()) {
			if(sd.getDay()==c.getDay()&&sd.getTimeUnit()>=c.getStartTime()&&sd.getTimeUnit()<=c.getEndTime()) {
				if(sd.getDay() == c.getDay()) {
					contractMap.put(c.getNode(), c.getCapacity());
				}
			}
		}
		
		for(Offering o : sd.getMarket().getOfferings()) {
			if(sd.getTimeUnit()>=o.getStartTime()&&sd.getTimeUnit()<=o.getEndTime()&&sd.getDay()==o.getDay()&&!o.isActive()) {
				if(sd.getDay() == o.getDay()) {
					offeringMap.put(o.getNode(), o.getPrice());
				}
			}
		}
		
		int entschaedigung = 0;
		for(PowerLine line: sd.getGrid().getLines()) {
			PowerNode source = line.getSource();
			if (source instanceof Producer) {
				if(contractMap.get(source)!=null) {
					entschaedigung+=Math.abs(source.getCurrentCapacity()-contractMap.get(source));
				}
				else {
					entschaedigung+=Math.abs(lastValues.get(source)-source.getCurrentCapacity());
				}
				
			}			
		}
		return entschaedigung;
	}
	
	/**
	 * Set the network to the first contracts. The network must match the contracts in day=0 t=0
	 * 
	 * @param sd
	 */
	public static void prepareGraph(SynergyDemo sd) {
		for(Contract c : sd.getMarket().getContracts()) {
			if(c.getStartTime()==0&&(c.getNode() instanceof Producer)) {
				c.getNode().setCurrentCapacity(c.getCapacity());
			}
		}
	}

	/**
	 * Performs the reactive optimization on the given graph. Very similar to proactive.
	 * 
	 * @param sd
	 * @param pl
	 * @param timesteps
	 * @param hops
	 * @param fillPlan
	 * @param balanceTolerance
	 * @param applyAllChanges
	 * @param print
	 * @return
	 * @throws GRBException
	 */
	public static int reactiveOptimizationGurobi(SynergyDemo sd, PowerLine pl, int timesteps, int hops, boolean fillPlan, double balanceTolerance, boolean applyAllChanges, boolean print) throws GRBException {

		// Create empty environment, set options, and start
		GRBEnv env = new GRBEnv(true);
	    env.set("logFile", "mip1.log");
	    env.set(GRB.IntParam.OutputFlag, 0);
	    env.set(GRB.IntParam.LogToConsole, 0);
	
	    env.start();
	
	    // Create empty model
	    GRBModel  model = new GRBModel(env);
		EnergyGrid grid = sd.getGrid();
	 
		//Get contracts for this timestep
	    Map<PowerNode, Contract> producerContracts = new HashMap<>();
	    for(Contract c:sd.getMarket().getContracts()) {
	    	if(c.getDay() == sd.getDay()) {
		    	if(sd.getTimeUnit()>=c.getStartTime()&&sd.getTimeUnit()<=c.getEndTime()&&c.getDay()==sd.getDay()) {
		    		producerContracts.put(c.getNode(), c);
		    	}
	    	}
	    }
																   
	    GRBVar bal = model.addVar(-balanceTolerance, balanceTolerance, 0, GRB.CONTINUOUS, "bal");
	    GRBVar balAbs = model.addVar(0, balanceTolerance, 0, GRB.CONTINUOUS, "balAbs");
	    model.addGenConstrAbs(balAbs, bal, "balAbsConst");
	    model.update();
	 
									 
	    GRBLinExpr objectiveGRB = new GRBLinExpr();
	 
	    //GRBLinExpr balanceGRB = new GRBLinExpr();
	    objectiveGRB.addTerm(1000.0, model.getVarByName("balAbs"));
	    //model.addConstr(balanceGRB, GRB.EQUAL, 0.0, "balS");
	    
	    // Nodes with Offerings in this timestep
	    HashSet<PowerNode> offeringsNow = new HashSet<>();
	    
	    // Set map of offerings
	    HashMap<PowerNode,LinkedList<Offering>> mapOfferings = new HashMap<>();
	    HashMap<String,Offering> mapVariableOfferings = new HashMap<>();
	    HashMap<Offering,Offering> mapOfEquivalentOfferings = new HashMap<>();
	    
	    for(Offering o : sd.getMarket().getOfferings()) {
	    	if(sd.getTimeUnit()>=o.getStartTime()&&sd.getTimeUnit()<=o.getEndTime()&&sd.getDay()==o.getDay()) {
	    		if(!o.isActive()) {
										 
		    		LinkedList<Offering> offeringsForNode = mapOfferings.get(o.getNode());
		    		if(offeringsForNode==null) {
		    			offeringsForNode = new LinkedList<>();
		    		}
		    		offeringsForNode.add(o);
		    		mapOfferings.put(o.getNode(), offeringsForNode);
	    		}
	    		else {
	    			// If activated, can be deactivated
	    			LinkedList<Offering> offeringsForNode = mapOfferings.get(o.getNode());
		    		if(offeringsForNode==null) {
		    			offeringsForNode = new LinkedList<>();
		    		}
		    		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		    		Offering oBack = factory.createOffering();
		    		oBack.setCapacity(-o.getCapacity());
		    		oBack.setNode(o.getNode());
		    		oBack.setMarket(null);
		    		offeringsForNode.add(oBack);
		    		mapOfferings.put(o.getNode(), offeringsForNode);
		    		mapOfEquivalentOfferings.put(oBack, o);
	    		}
	    	}
	    }										   
	    
	    // Reference variable for the consumers
	    GRBVar cRef = model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "cRef");
	    
		for(PowerLine line: grid.getLines()) {
			if(line.isActivated()&&line.getSource().isActivated()&&line.getSink().isActivated()) {
				if(line.getSource() instanceof Producer)  {
					Producer producer = (Producer) line.getSource();
					double lb;
					double ub;
					GRBVar v0;
					GRBVar v1;
					GRBVar aux;
					GRBVar aux2;
					// How important is to stay close to the market plan
					double planImportance = 10000.0;
					// How important is to change the less possible
					double changeImportance = 100.0;
					// How important is to maximize the clean energy
																					  
					double differenceImportance;
					double powerChanged = timesteps==0?0:((Producer)producer).getPowerChanged();

					if(producerContracts.containsKey(producer)) {
						Contract c = producerContracts.get(producer);
						lb = Math.max(0,producer.getCurrentCapacity()-timesteps*((Producer)producer).getPowerDownSpeed() - powerChanged);
						ub = Math.min(producer.getMaxCapacity(),producer.getCurrentCapacity()+timesteps*((Producer)producer).getPowerUpSpeed() - powerChanged);
						// v0 = capacity of producer to be determined
						// v1 = actual capacity of producer
						v0 = model.addVar(lb, ub, 0, GRB.CONTINUOUS, line.getId());
						v1 = model.addVar(c.getCapacity(), c.getCapacity(),0, GRB.CONTINUOUS, "O"+line.getId());
						aux = model.addVar(-(lb+c.getCapacity()), ub+c.getCapacity(), 0, GRB.CONTINUOUS, "aux"+line.getId()) ;// auxiliary variable
						aux2 = model.addVar(0,Math.max(lb+c.getCapacity(), ub+c.getCapacity()), 0, GRB.CONTINUOUS, "auxAbs"+line.getId()) ;// auxiliary variable absolute
						differenceImportance = planImportance;
					}
					else {
						double maxUpChange = Math.max(0,timesteps*((Producer)producer).getPowerUpSpeed() - powerChanged);
						double maxDownChange = timesteps*((Producer)producer).getPowerDownSpeed() + powerChanged;
						lb = Math.max(0,producer.getCurrentCapacity()-maxDownChange);
						ub = Math.min(producer.getMaxCapacity(),producer.getCurrentCapacity()+maxUpChange);
						
						// v0 = capacity of producer to be determined
						// v1 = actual capacity of producer
						v0 = model.addVar(lb, ub, 0, GRB.CONTINUOUS, line.getId());
						v1 = model.addVar(producer.getCurrentCapacity(), producer.getCurrentCapacity(),0, GRB.CONTINUOUS, "O"+line.getId());
														  
						aux = model.addVar(-maxDownChange, maxUpChange, 0, GRB.CONTINUOUS, "aux"+line.getId()) ;// auxiliary variable
						aux2 = model.addVar(0,Math.max(timesteps*((Producer)producer).getPowerDownSpeed() - powerChanged,timesteps*((Producer)producer).getPowerUpSpeed() + powerChanged), 0, GRB.CONTINUOUS, "auxAbs"+line.getId()) ;// auxiliary variable absolute
																			
						differenceImportance = changeImportance;
					}
					// v0 = capacity of producer to be determined
					// v1 = actual capacity of producer
					GRBLinExpr auxExpr = new GRBLinExpr();
					// aux = v0 - v1
					// aux is positive: Producer produces more: More expensive for environment
					// aux is negative: Producer produces less: Cheaper for environment
					auxExpr.addTerm(1, v0);
					auxExpr.addTerm(-1, v1);
					auxExpr.addTerm(-1, aux);
					model.addConstr(auxExpr,GRB.EQUAL, 0.0, "aux"+line.getId());
					//aux2 = abs(aux)
					model.addGenConstrAbs(aux2, aux, "auxAbs"+line.getId());
					// How important is to change the less possible
					// How important is to change the EEG first
					double priceOfEnergy = ((Producer)producer).getPriceProMW();
					objectiveGRB.addTerm(differenceImportance, aux2);
					objectiveGRB.addTerm(1/priceOfEnergy, aux2);
				}
				else if(line.getSink() instanceof Consumer) {
					Consumer consumer = (Consumer) line.getSink();
					double demandSiteImportance = 10;							   
	 
					model.addVar(-Double.MAX_VALUE, Double.MAX_VALUE, 0, GRB.CONTINUOUS, line.getId());
					if(mapOfferings.containsKey(consumer)) {
 						int oNumber = 0;
 						for(Offering o : mapOfferings.get(consumer)) {	
 							mapVariableOfferings.put("zonr"+oNumber+line.getId(), o);
							double ub;
							ub = 10000000;
							// zovar is the offering capacity or 0. 
							GRBVar zovarsign = model.addVar(-Double.MAX_VALUE,Double.MAX_VALUE, 0, GRB.INTEGER, "zos"+oNumber+line.getId());
							GRBVar ovar = model.addVar(0,Double.MAX_VALUE, 0, GRB.CONTINUOUS, "o"+oNumber+line.getId());
							GRBVar xovar = model.addVar(0, 1, 0, GRB.BINARY, "xo"+oNumber+line.getId());
							GRBVar zovar = model.addVar(0,Double.MAX_VALUE, 0, GRB.CONTINUOUS, "zo"+oNumber+line.getId());
							GRBVar zovarnr = model.addVar(-Math.abs(o.getCapacity()),Math.abs(o.getCapacity()), 0, GRB.CONTINUOUS, "zonr"+oNumber+line.getId());
							GRBLinExpr xoexpr1 = new GRBLinExpr();
							xoexpr1.addTerm(1, zovar);
							xoexpr1.addTerm(-ub,xovar);
							model.addConstr(xoexpr1,GRB.LESS_EQUAL, 0.0, "xoexpr1"+oNumber+line.getId());
							GRBLinExpr xoexpr2 = new GRBLinExpr();
							xoexpr2.addTerm(1, zovar);
							xoexpr2.addTerm(-1,ovar);
							xoexpr2.addTerm(-ub,xovar);
							model.addConstr(xoexpr2,GRB.GREATER_EQUAL, -ub, "xoexpr2"+oNumber+line.getId());
							GRBLinExpr xoexpr3 = new GRBLinExpr();
							xoexpr3.addTerm(1, zovar);
							xoexpr3.addTerm(-1,ovar);
							model.addConstr(xoexpr3,GRB.LESS_EQUAL, 0.0, "xoexpr3"+oNumber+line.getId());
							// without reference adjust
							GRBLinExpr xoexpr4 = new GRBLinExpr();
							xoexpr4.addTerm(1, zovarnr);
							xoexpr4.addTerm(-o.getCapacity(),xovar);
							model.addConstr(xoexpr4,GRB.EQUAL, 0.0, "xoexpr4"+oNumber+line.getId());
							
							GRBLinExpr zosign = new GRBLinExpr();
							zosign.addTerm(1, zovar);
							zosign.addTerm(-Math.signum(o.getCapacity()), zovarsign);
							model.addConstr(zosign,GRB.EQUAL, 0, "signumExp"+oNumber+line.getId());
							
							objectiveGRB.addTerm(demandSiteImportance, zovar);
							offeringsNow.add(o.getNode());
							oNumber++;
 						}
					}
				}
				else {
					if(line.isMultipleConnection()) {
						model.addVar(-line.getMaxCapacity()*0.795, line.getMaxCapacity()*0.795, 0, GRB.CONTINUOUS, line.getId());
					}
					else {
						model.addVar(-line.getMaxCapacity(), line.getMaxCapacity(), 0, GRB.CONTINUOUS, line.getId());
					}
				}
			}

		}
		model.update();
		model.setObjective(objectiveGRB, GRB.MINIMIZE);
	
		
		GRBLinExpr sumOfAllActivZero = new GRBLinExpr();
		double allConsumers = 0;
		
		for(PowerNode node: grid.getNodes()) {
			if(!node.isActivated()) {
				continue;
			}
			if(node instanceof Bus) {
				//Add the variable
				model.addVar(0, Double.MAX_VALUE, 0, GRB.CONTINUOUS, "V"+node.getName());
				GRBLinExpr transformerGRB = new GRBLinExpr();
				for(PowerLine line: node.getIncomingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					transformerGRB.addTerm(1.0, model.getVarByName(line.getId()));											 
					GRBVar o = model.getVarByName("o"+line.getId());
					if(o != null) {
						transformerGRB.addTerm(1.0, o);
					}
				}
				for(PowerLine line: node.getOutgoingLines()) {
					if(!line.isActivated()||!line.getSource().isActivated()||!line.getSink().isActivated()) {
						continue;
					}
					transformerGRB.addTerm(-1.0, model.getVarByName(line.getId()));
	 
					if(line.getSink() instanceof Consumer) {
						int oNumber = 0;
						while (model.getVarByName("zos"+oNumber+line.getId())!=null) {
																 
							GRBVar zovarsign = model.getVarByName("zos"+oNumber+line.getId());
							if(zovarsign != null) {
								transformerGRB.addTerm(1.0, zovarsign);
							}
							oNumber++;
						}
					}
				}
	
				if(grid.getReference() == null||!grid.getReference().isActivated()) {
					grid.setReference(node);
				}											 
				model.addConstr(transformerGRB,GRB.EQUAL, 0.0, "k"+node.getName());
			}
			else {
				for(PowerLine line: node.getOutgoingLines()) {
					if(line.isActivated()&&line.getSink().isActivated()) {
						sumOfAllActivZero.addTerm(1.0, model.getVarByName(line.getId()));
					}
				}
				for(PowerLine line: node.getIncomingLines()) {
					if(line.isActivated()&&line.getSource().isActivated()) {
						GRBVar c = model.getVarByName(line.getId());
						GRBLinExpr consumers = new GRBLinExpr();
						consumers.addTerm(1,c);	
						consumers.addTerm(node.getCurrentCapacity(),cRef);	
						model.addConstr(consumers, GRB.EQUAL, 0.0, "consumer"+node.getName());
						allConsumers-=node.getCurrentCapacity();
						
						if(mapOfferings.containsKey(line.getSink())) {
							int oNumber = 0;
							for (Offering o : mapOfferings.get(line.getSink())) {
								
								GRBVar ovar = model.getVarByName("o"+oNumber+line.getId());
								GRBVar zonr = model.getVarByName("zonr"+oNumber+line.getId());
								GRBLinExpr consumerso = new GRBLinExpr();
								consumerso.addTerm(1,ovar);
								
								consumerso.addTerm(-Math.abs(o.getCapacity()),cRef);
								model.addConstr(consumerso, GRB.EQUAL, 0.0, "consumero"+oNumber+node.getName());
								sumOfAllActivZero.addTerm(1, zonr);

								oNumber++;
							}
						}
					}
				
				}
 			}
		}
		model.update();
		sumOfAllActivZero.addTerm(-1.0, model.getVarByName("bal"));
		
		model.addConstr(sumOfAllActivZero, GRB.EQUAL, allConsumers, "sumOfAllActivZero");
		
		for(PowerLine line: grid.getLines()) {
			if(!line.isActivated()) {
				continue;
			}
			if(!line.getSource().isActivated()||!line.getSink().isActivated()) {
				continue;
			}
			if(line.getSource() instanceof Bus && line.getSink() instanceof Bus) {
	
				GRBLinExpr ohmLine = new GRBLinExpr();
				ohmLine.addTerm(-line.getReactance(), model.getVarByName(line.getId()));
				ohmLine.addTerm(1, model.getVarByName("V"+line.getSource().getName()));
				ohmLine.addTerm(-1, model.getVarByName("V"+line.getSink().getName()));
				model.addConstr(ohmLine, GRB.EQUAL, 0.0, "ohmLine"+line.getId());
	
				// N-1 Sicherheit
			}
		}
		
		log("Connecting with Gurobi");
		long conn = System.nanoTime();
		model.optimize();
		log("Gurobi needed "+((System.nanoTime()-conn)/1000)+"us");
		int status = model.get(GRB.IntAttr.Status);
		if(status == 3) {
			//resetChanges(sd);
			model.dispose();
			env.dispose();
			return -1;
		}
				  
		if(fillPlan) {
			createPlan(model, sd, mapVariableOfferings,mapOfEquivalentOfferings); // Only one step of plan.
   
		}
		else {
			solveNetwork(model, sd, 10, applyAllChanges, null, print);
		}

		 // Dispose of model and environment
		 model.dispose();
		 env.dispose();
												
		return timesteps;
   
	}
	
	/**
	 * Plans the changes of the optimization plan in the given System for the current timestep
	 * @param model                        solution of the optimization
	 * @param sd                           System to plan the changes
	 * @param mapVariableOfferings		   Map of the variables of the LP and the offerings
	 * @param mapOfEquivalentOfferings     Map of the Offerings with its equivalent to stop the offering
	 * @return                             1 if changes were planned. 0 if the result is the same as the current state of the network
	 * @throws GRBException
	 */
	private static int createPlan(GRBModel model, SynergyDemo sd, HashMap<String, Offering> mapVariableOfferings, HashMap<Offering, Offering> mapOfEquivalentOfferings) throws GRBException {
		EnergyGrid grid = sd.getGrid();
		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;

		// Change direction of the lines, if the capacity obtained is < 0
		for(PowerLine line : grid.getLines()) {
			GRBVar var = model.getVarByName(line.getId());
			if(var==null)continue;
			double capacity = Math.round(1000*var.get(GRB.DoubleAttr.X))/1000;
			if(capacity < 0.0) {
				Simulator.swapFlowDirection(line);
				capacity = -capacity;
			}
		}
		int nodesChanged = 0;

		// Search for all the changes 
		for(PowerNode node: grid.getNodes()) {
			GRBVar var1 = model.getVarByName("x"+node.getName());
			if(var1 != null) {
				double x = var1.get(GRB.DoubleAttr.X);
				// if a x variable exists and is 0, AbLaV was made, node must be powered down.
				if ((Math.round(x)==0)&&node.getCurrentCapacity()!=0) {
					ScheduledChange change;
					change = factory.createScheduledChange();
					change.setNode(node);
					change.setTimeUnit(sd.getTimeUnit());
					change.setDay(sd.getDay());
					change.setValue(-node.getCurrentCapacity());
					change.setActivationChange(false);
					sd.getPowerPlan().getReactiveChanges().add(change);
					if(sd.isCopy()) {
						copyReactiveChangesToProactiveChanges(sd, change);
					}
					nodesChanged++;
					continue;
				}
			}
			if(node instanceof Producer && node.getIncomingLines().size()>0) {
				Simulator.swapFlowDirection(node.getIncomingLines().get(0));
			}
			
			if(node instanceof Consumer){
				GRBVar var;
				int oNumber = 0;
				if(node.getIncomingLines()==null||node.getIncomingLines().size()==0) {
					continue;
				}
				String varName = "zonr"+oNumber+node.getIncomingLines().get(0).getId();
				// The zorn Variable is the DSM variable
				while((var = model.getVarByName(varName))!=null) {
					double capacity = 0;
					if(var!=null) {
						capacity = var.get(GRB.DoubleAttr.X);
					}
					// If the DSM is != 0, there is a DSM to add
					if(Math.round(capacity)!=0) {
						ScheduledChange change;
						change = factory.createScheduledChange();
						change.setNode(node);
						change.setTimeUnit(sd.getTimeUnit());
						change.setDay(sd.getDay());
						change.setValue(capacity);
						change.setActivationChange(false);
						sd.getPowerPlan().getReactiveChanges().add(change);
						nodesChanged++;
						Offering of = mapVariableOfferings.get(varName);
						// Reactive changes in a copy graph are the proactive changes that must be copied later to the main graph to make the proactive plan
						if(sd.isCopy()) {
							copyReactiveChangesToProactiveChanges(sd, change);
						}
					}
					oNumber++;
					varName = "zonr"+oNumber+node.getIncomingLines().get(0).getId();
				}
			}
			else if(node instanceof Producer) {
				if(node.isActivated()&&node.getOutgoingLines().get(0).isActivated()&&node.getOutgoingLines().get(0).getSink().isActivated())	{
					GRBVar var = model.getVarByName(node.getOutgoingLines().get(0).getId());
					double capacity = var.get(GRB.DoubleAttr.X);
					double nodecap = node.getCurrentCapacity();
					
					// if the obtained value doesnt match the current. a change must be done.
					if(Math.round(1000*node.getCurrentCapacity())/1000 != Math.round(1000*capacity)/1000 ) {
					
						double toChange = Math.round(1000*capacity)/1000;
						double fromChange = Math.round(1000*nodecap)/1000;
						ScheduledChange change;
						change = factory.createScheduledChange();
						change.setNode(node);
						change.setTimeUnit(sd.getTimeUnit());
						change.setDay(sd.getDay());				
						double limit = 0;
						
						double powerChanged = ((Producer)node).getPowerChanged();
						// If we already turned up the production, we have to take that into account
						if(toChange-fromChange>0) {
							limit=((Producer)node).getPowerUpSpeed() - powerChanged;
							
						}
						// If we turned it down too.
						else {
							limit=((Producer)node).getPowerDownSpeed() + powerChanged;

						}
						// Set the values for the change
						if((Math.abs(toChange-fromChange) > limit)){
							change.setValue(Math.signum(toChange-fromChange)*limit);
						}
						else {
							change.setValue(toChange-fromChange);
						}

						sd.getPowerPlan().getReactiveChanges().add(change);
						if(sd.isCopy()) {
							copyReactiveChangesToProactiveChanges(sd, change);
						}
						nodesChanged++;

					}
				}
					
			}
		}

		if(nodesChanged>0) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * Solves the network for the given model. Shows the steps to do it and applies it, if wanted.
	 * Very similar to createPlan. But no plan is created. only for the button Optimize of Interface
	 * 
	 * @param model
	 * @param sd
	 * @param maxTimesteps
	 * @param apply
	 * @param nodesToMatch
	 * @param print
	 * @return
	 * @throws GRBException
	 */
	public static int solveNetwork(GRBModel model, SynergyDemo sd, int maxTimesteps, boolean apply, HashMap<String, Double> nodesToMatch, boolean print) throws GRBException {
		EnergyGrid grid = sd.getGrid();
		HashMap<Integer, HashSet<ScheduledChange>> changes = new HashMap<>();
		
		for(PowerLine line : grid.getLines()) {
			GRBVar var = model.getVarByName(line.getId());
			double capacity = Math.round(1000*var.get(GRB.DoubleAttr.X))/1000;
			if(capacity < 0.0) {
				Simulator.swapFlowDirection(line);
				capacity = -capacity;
			}
		}
		HashSet<ScheduledChange> firstChanges = new HashSet<>();
		HashMap<PowerNode, Double> nodeChanged = new HashMap<>();
		HashMap<PowerNode, Double> nodeActual = new HashMap<>();
		for(PowerNode node: grid.getNodes()) {
			GRBVar var1 = model.getVarByName("x"+node.getName());
			if(var1 != null) {
				double x = var1.get(GRB.DoubleAttr.X);
				if ((Math.round(x)==0)&&node.getCurrentCapacity()!=0) {
					if(!apply) {
						SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
						ScheduledChange change;
						change = factory.createScheduledChange();
						change.setNode(node);
						change.setTimeUnit(sd.getTimeUnit());
						change.setDay(sd.getDay());
						change.setValue(-node.getCurrentCapacity());
						change.setActivationChange(false);
						firstChanges.add(change);
						continue;
					}
					else {
						node.setCurrentCapacity(0);
					}
				}
			}
			if(node instanceof Producer && node.getIncomingLines().size()>0) {
				Simulator.swapFlowDirection(node.getIncomingLines().get(0));
			}

			if(node instanceof Consumer){
				int oNumber = 0;
				GRBVar var;
				if(node.getIncomingLines()==null || node.getIncomingLines().size()==0) {
					continue;
				}
				while((var = model.getVarByName("zonr"+oNumber+node.getIncomingLines().get(0).getId()))!=null) {
					double capacity = 0;
					if(var!=null) {
						capacity = var.get(GRB.DoubleAttr.X);
					}
					if(capacity!=0) {
						nodeChanged.put(node, node.getCurrentCapacity()+capacity);
						nodeActual.put(node, node.getCurrentCapacity());
						if(nodesToMatch != null) {
							nodesToMatch.put(node.getName(), node.getCurrentCapacity()+capacity);
						}
					}
					oNumber++;
				}
			}
			if(node instanceof Producer&&node.isActivated()&&node.getOutgoingLines().get(0).isActivated()&&node.getOutgoingLines().get(0).getSink().isActivated()) {
				GRBVar var = model.getVarByName(node.getOutgoingLines().get(0).getId());
				double capacity = var.get(GRB.DoubleAttr.X);				
				
				var = model.getVarByName("o"+node.getOutgoingLines().get(0).getId());
				if(var!=null) {
					capacity += var.get(GRB.DoubleAttr.X);
				}
				if(Math.round(1000*node.getCurrentCapacity())/1000 != Math.round(1000*capacity)/1000 ) {
					//System.out.println("Change "+node.getName()+" from "+node.getCurrentCapacity()+" to "+capacity);
					nodeChanged.put(node, capacity);
					nodeActual.put(node, node.getCurrentCapacity());
					if(nodesToMatch!=null) {
						nodesToMatch.put(node.getName(), capacity);
					}
				}
			}
		}
		int step = 0;
		
		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		Set<ScheduledChange> changesScheduled = new HashSet<>();
		
		// Then we add all the new changes
		while(nodeChanged.size()>0) {
			//System.out.println( "Step "+step);
			Set<PowerNode> nodeSet = new HashSet<>(nodeChanged.keySet());
			HashSet<ScheduledChange> changesForStep = new HashSet<>();
			if(step == 0) {
				changesForStep.addAll(firstChanges);
			}
			
			for(PowerNode node : nodeSet) {
				double toChange = Math.round(1000*nodeChanged.get(node))/1000;
				double fromChange = Math.round(1000*nodeActual.get(node))/1000;
				ScheduledChange change;
				change = factory.createScheduledChange();
				change.setNode(node);
				change.setTimeUnit(sd.getTimeUnit());
				change.setDay(sd.getDay());
				for(int i=0; i<step; i++) {
					decrementTimeUnit(change);
				}
				double limit = 0;
				
				if(node instanceof Producer) {
					double powerChanged = ((Producer)node).getPowerChanged();
					if(toChange-fromChange>0) {
						
						// CHANGE THE LIMIT WITH THE TIMESTEP! 
						limit=((Producer)node).getPowerUpSpeed() - powerChanged;	
					}
					else {
						limit=((Producer)node).getPowerDownSpeed() + powerChanged;
					}
				
					if((Math.abs(toChange-fromChange) > limit)){
						change.setValue(Math.signum(toChange-fromChange)*limit);
						//System.out.println("Change "+change.getNode().getName()+" from "+fromChange+" to "+(fromChange + change.getValue()));
						nodeActual.put(node, fromChange+(Math.signum(toChange-fromChange))*limit);
					}
					else {
						change.setValue(toChange-fromChange);
						//System.out.println("Change "+node.getName()+" from "+fromChange+" to "+toChange);
						nodeChanged.remove(node);
					}
				}
				else {
					change.setValue(toChange-fromChange);
					//System.out.println("Change "+node.getName()+" from "+fromChange+" to "+toChange);
					nodeChanged.remove(node);
				}
				changesForStep.add(change);
				changesScheduled.add(change);
			}
			
			changes.put(step, changesForStep);
			
			step++;
			if(step>=maxTimesteps) {
				for(int i = 0; i<step; i++) {
					if(print)log("Step "+i+" of the solution");
					for(ScheduledChange change : changes.get(i)) {
						if(apply) {
							change.getNode().setCurrentCapacity(change.getNode().getCurrentCapacity()+change.getValue());
							if(print)log("Change "+change.getNode().getName()+" to "+change.getNode().getCurrentCapacity());
						}
						else if(print)log("Change "+change.getNode().getName()+" "+change.getValue());
					}
				}
				return step;
			}
		}
		for(int i = 0; i<step; i++) {
			if(print)log("Step "+i+" of the solution");
			for(ScheduledChange change : changes.get(i)) {
				if(apply) {
					change.getNode().setCurrentCapacity(change.getNode().getCurrentCapacity()+change.getValue());
				}
				if(print)log("Change "+change.getNode().getName()+" "+change.getValue());
			}
		}
		return step;
	}
	
	
	/**
	 * Resets the recorded changes made in the current timestep and 
	 * so prepares the graph for the next timestep
	 * 
	 * @param sd
	 */
	public static void resetChanges(SynergyDemo sd) {
		for(PowerNode n: sd.getGrid().getNodes()) {
			if(n instanceof Producer) {
				((Producer) n).setPowerChanged(0);
			}
		}
	}
}
