package org.daniel.synergydemo3.synergygui.rules;

import org.apache.log4j.Logger;

import Optimizer.Optimizer;
import Simulator.Simulator;
import SynergyDemo3.Consumer;
import SynergyDemo3.Offering;
import SynergyDemo3.PowerLine;
import SynergyDemo3.Producer;
import SynergyDemo3.ScheduledChange;
import SynergyDemo3.SynergyDemo;
import SynergyDemo3.WindPlant;
import SynergyDemo3Rules.GraphValidator;
import SynergyDemo3Rules.api.SynergyDemo3RulesAPI;
import SynergyDemo3Rules.api.matches.OverloadedLineMatch;
import gurobi.GRBException;

public class SynergyDemoRules{
	private SynergyDemo3RulesAPI api;
	private boolean madeChangesInTimestep = false;

	public SynergyDemoRules(SynergyDemo synergyDemo) {
		api = new GraphValidator(synergyDemo).initAPI();

		// Subscribe to appearing and disappearing matches 
		api.noChanges().subscribeAppearing(m -> registerNoMoreChanges(m.getSd()));
		api.multipleConnection().subscribeAppearing(m -> registerMultipleConnection(m.getLine()));
		api.activatedLine().subscribeDisappearing(m -> registerFailingLine(m.getLine(), m.getSd()));
		api.reactiveChangeNow().subscribeAppearing(m -> registerScheduledreactive(m.getChange(),m.getSd()));
		api.proactiveChangeNow().subscribeAppearing(m -> registerScheduledproactive(m.getChange(),m.getSd()));

	}

	/**
	 * Fired when there are no more changes planned for the timestep. 
	 * it can only fire after the reactive optimization (or the no-reactive optimization, if not needed)
	 * 
	 * @param sd
	 */
	private void registerNoMoreChanges(SynergyDemo sd) {
		if(madeChangesInTimestep) {
			log("All changes performed for this Timestep. Simulating the Network...");
			try {
				Simulator.GurobiSolve(sd.getGrid());
				madeChangesInTimestep=false;
			} catch (GRBException e) {
				e.printStackTrace();
			}
		}
		// Reset the recorded changes in the nodes. 
		Optimizer.resetChanges(sd);
	}

	private void log(final String message) {
		Logger.getRootLogger().info("Matches: " + message);
	}
	
	/**
	 * Methode triggered when a reactive change for the current timestep is detected 
	 * 
	 * @param change
	 * @param sd
	 */
	private void registerScheduledreactive(ScheduledChange change, SynergyDemo sd) {
		madeChangesInTimestep = true;
		if(change.getReactivePlan()!=null) {
			if(change.isActivationChange()) {
				log("Chop Node: "+change.getNode().getName());
				boolean active = change.getValue()==1;
				change.getNode().setActivated(active);
				change.getReactivePlan().getReactiveChanges().remove(change);
			}
			else {
				String eisman = "Reactive";
				if(sd.isCopy()) {
					eisman = "Scheduled proactive";
					if(change.getNode() instanceof WindPlant) {
						//log("STATISTICS;EEG;"+Math.abs(change.getValue()));
					}
					else {
						//log("STATISTICS;CONV;"+Math.abs(change.getValue()));
					}
					Offering activated = null;
					// If a DSM with the same amount in negative for this node was active
					// we switch it off. 
					// Is better to switch off a DSM that was on, than have 2 DSM on with the same amount (one positiv one negativ)
					if(change.getNode() instanceof Consumer) {
						for(Offering f : sd.getMarket().getOfferings()) {
							if(f.getNode() == change.getNode()) {
								if(change.getTimeUnit() >= f.getStartTime()&&change.getTimeUnit()<=f.getEndTime()) {
									if(change.getDay()==f.getDay()) {
										if(!f.isActive()&&change.getValue()==f.getCapacity()) {
											f.setActive(true);
											activated = f;
										}
										if(f.isActive()&&(-change.getValue()==f.getCapacity())) {
											f.setActive(false);
											if(activated!=null) {
												activated.setActive(false);
												break;
											}
										}
									}
								}
							}
						}
						
						
					}
				}
				String UpOrDown = eisman+ " Change: Power Up ";
				if(change.getValue()<0) {
					UpOrDown = eisman+" Change: Power Down ";
				}
				int newValue ;
				
				if(change.getNode() instanceof Producer) {
					newValue = (int) Math.round(Math.min(change.getNode().getMaxCapacity(), Math.max(0,change.getNode().getCurrentCapacity() + change.getValue())));
				}
				else {
					double now = change.getNode().getCurrentCapacity() + change.getValue();
					newValue = (int) Math.round(Math.max(change.getNode().getMaxCapacity(), Math.min(0,now)));
				}
				if(Math.round(newValue)==0) {
					UpOrDown = " Shut down Node ";
				}
				
				log(UpOrDown+change.getNode().getName()+" from "+(int)change.getNode().getCurrentCapacity()+" to "+newValue+" MW");
				change.getNode().setCurrentCapacity(newValue);
				change.getReactivePlan().getReactiveChanges().remove(change);
			}
			this.analyseGraph(sd);
		}
	}
	
	/**
	 * Methode triggered when a proactive change for the current timestep is detected 
	 * 
	 * @param change
	 * @param sd
	 */
	private void registerScheduledproactive(ScheduledChange change, SynergyDemo sd) {
		madeChangesInTimestep = true;
		if(change.getProactivePlan()!=null) {
			if(change.isActivationChange()) {
				log("Chop Node: "+change.getNode().getName());
				boolean active = change.getValue()==1;
				change.getNode().setActivated(active);
				change.getProactivePlan().getReactiveChanges().remove(change);
			}
			else {
				String UpOrDown = "Proactive change in Schedule!: Power Up ";
				if((change.getNode() instanceof Producer && change.getValue()<0)
						||(change.getNode() instanceof Consumer && change.getValue()>0)) {
					UpOrDown = "Proactive change in Schedule!: Power Down ";
				}
				int newValue;
				if(change.getNode() instanceof Producer) {
					newValue = (int)Math.round( Math.min(change.getNode().getMaxCapacity(), Math.max(0,change.getNode().getCurrentCapacity() + change.getValue())));
				}
				else {
					newValue = (int)Math.round( Math.max(change.getNode().getMaxCapacity(), Math.min(0,change.getNode().getCurrentCapacity() + change.getValue())));
				}
				if(Math.round(newValue)==0) {
					UpOrDown = " Shut down Node ";
				}
				log(UpOrDown+change.getNode().getName()+" from "+(int)change.getNode().getCurrentCapacity()+" to "+newValue+" MW");
				if(change.getNode() instanceof Producer) {
					((Producer)change.getNode()).setPowerChanged(change.getValue());
				}
				change.getNode().setCurrentCapacity(newValue);

				
				sd.getPowerPlan().getProactiveChanges().remove(change);
			}
			
			Offering activated = null;
			// if it is a DSM change, deactivate the offer in main graph (no longer available until is due)
			if(change.getNode() instanceof Consumer) {
				for(Offering f : sd.getMarket().getOfferings()) {
					if(f.getNode() == change.getNode()) {
						if(change.getTimeUnit() >= f.getStartTime()&&change.getTimeUnit()<=f.getEndTime()) {
							if(change.getDay()==f.getDay()) {
								if(!f.isActive()&&change.getValue()==f.getCapacity()) {
									f.setActive(true);
									activated = f;
								}
								if(f.isActive()&&(-change.getValue()==f.getCapacity())) {
									f.setActive(false);
									if(activated!=null) {
										activated.setActive(false);
										break;
									}
								}
							}
						}
					}
				}
				
				
			}
			this.analyseGraph(sd);
		}
	}

	/**
	 * Fires when a line fails. Resets the proactive plan because is no longer useful
	 * 
	 * @param powerLine
	 * @param sd
	 */
	private void registerFailingLine(PowerLine powerLine, SynergyDemo sd) {
		log(powerLine.getId()+" failed!");
		if(sd.getPowerPlan().getProactiveChanges().size()>0) {
			sd.getPowerPlan().getProactiveChanges().clear();
			log("Reset proactive Plan.");
			this.analyseGraph(sd);
		}
	}

	
	/**
	 * Fires when a multiple connection is detected. Sets the corresponding attribute 
	 * 
	 * @param powerLine
	 */
	private void registerMultipleConnection(PowerLine powerLine) {
		if(!powerLine.isMultipleConnection()) {
			log(powerLine.getId()+" belongs to a multiple connection");
			powerLine.setMultipleConnection(true);
		}
	}
	
	
	
	/**
	 * Prints the conflicts of the configuration and gives back if the configuration has conflicts
	 * 
	 * @param synergyDemo
	 * @return             true if the configuration has conflicts
	 */
	public boolean isConflictive(SynergyDemo synergyDemo) {
		if(1-api.balancedGrid().countMatches()>0) {
			log("There is Imbalance: "+synergyDemo.getGrid().getBalance());
		}
		if(api.overloadedLine().countMatches()>0) {
			log("There are overloaded lines:");
			for(OverloadedLineMatch matches :api.overloadedLine().findMatches()) {
				log("Overloaded Line "+matches.getLine().getId()+": "+matches.getLine().getCurrentCapacity()+"  "+matches.getLine().getMaxCapacity());
			}
		}
		if(api.nMinusOneInsecure().countMatches()>0) {
			log("There are n-1 insecure lines");
		}
		long conflicts = (1-api.balancedGrid().countMatches())+api.overloadedLine().countMatches()+api.nMinusOneInsecure().countMatches();
		if(conflicts==0) {
			log("There are no conflicts");
		}
		return (conflicts > 0);
	}

	public void analyseGraph(SynergyDemo synergyDemo) {
		api.updateMatches();
		
	}
}
