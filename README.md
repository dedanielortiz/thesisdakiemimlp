# ThesisDAKiEmIMLP

Master Thesis von Daniel Ortiz mit dem Titel:
Detektierung und Auflösung von Kapazitätsengpässen in Energienetzen mittels inkrementeller Mustererkennung und linearer Programmierung

For the installation and execution of this project, Java SE-15 is required.
To execute the Project run as Java Application - Search and execute "Controller - org.daniel.synergydemo3.synergygui.controller"
