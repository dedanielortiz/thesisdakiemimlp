package org.daniel.synergydemo3.synergygui.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.daniel.synergydemo3.synergygui.rules.SynergyDemoRules;
import org.daniel.synergydemo3.synergygui.view.View;
import org.moflon.core.utilities.eMoflonEMFUtil;

import Optimizer.Optimizer;
import Simulator.Simulator;
import gurobi.GRBException;
import randomizer.Randomizer;
import SynergyDemo3.City;
import SynergyDemo3.Consumer;
import SynergyDemo3.EnergyGrid;
import SynergyDemo3.EnergyMarket;
import SynergyDemo3.PowerLine;
import SynergyDemo3.PowerNode;
import SynergyDemo3.PowerPlan;
import SynergyDemo3.Producer;
import SynergyDemo3.SynergyDemo;
import SynergyDemo3.SynergyDemo3Factory;
import SynergyDemo3.SynergyDemo3Package;
import SynergyDemo3.Bus;

/**
 * This is the controller class which controls the board and view.
 * 
 */
public class Controller {

	private SynergyDemoRules synergyDemoRules;
	int counterNode = 0;
	public int counterLine = 0;
	
	SynergyDemo new_sd;
	Randomizer randomizer;
	
	public DefaultListModel<String> elements;
	public DefaultListModel<String> connectionList; 
	
	List<Character> producerTypes; 
	List<Character> consumerTypes;
	
	public String element[] = {"Wind", "Sun", "Coal", "Nuclear", "Gas","City","Fabric","Town","Bus","Hub"};
	String nodeType[] = {"w", "s", "k", "n", "a","c","f","o","t","h"};
	
	
	/**
	 * Main function or rather program entry point.
	 * 
	 * @param args Specifies the program arguments (or rather parameters).
	 */
	
	View view;
	public static void main(String[] args) {
		Logger.getRootLogger().setLevel(Level.INFO);
		new Controller();
		
	}

	public Controller() {

		
		view = new View(this);
		view.setController(this);
		
		producerTypes = new LinkedList<>();
		producerTypes.add('k'); //coal
		producerTypes.add('g'); //geo
		producerTypes.add('n'); // nuclear
		producerTypes.add('w'); // wind
		producerTypes.add('s'); // solar
		producerTypes.add('a'); // gas
		producerTypes.add('d'); // diesel
		
		consumerTypes = new LinkedList<>();
		consumerTypes.add('c'); // city
		consumerTypes.add('f'); // fabric
		consumerTypes.add('o'); // town

	}
	
	
	/**
	 * Add an element to the board
	 * 
	 * @param name
	 * @param combo_element
	 * @param power
	 * @param preis
	 * @param speed
	 * @param max_capacity
	 */
	public void add(JTextField name, JComboBox<String> combo_element, JTextField power, JTextField preis, JTextField speed, JTextField max_capacity ) {
		String new_node = "";
		if(name.getText().equals("")) {
		   name.setText(Integer.toString(counterNode));
		}
		switch((String)combo_element.getSelectedItem()) {
    		case "Wind":
    			new_node = new_node.concat("w"+name.getText()+":");
		    	new_node = power.getText().equals("")?new_node.concat("10000"+":"):new_node.concat(power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("0.002"+":"):new_node.concat(preis.getText()+":");
    			new_node = speed.getText().equals("")?new_node.concat("1000"+":"):new_node.concat(speed.getText()+":");
    			new_node = max_capacity.getText().equals("")?new_node.concat("100000"):new_node.concat(max_capacity.getText());
    			name.setText("");
    			break;
    		case "Sun":
    			new_node = new_node.concat("s"+name.getText()+":");
		    	new_node = power.getText().equals("")?new_node.concat("10000"+":"):new_node.concat(power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("0.01"+":"):new_node.concat(preis.getText()+":");
    			new_node = speed.getText().equals("")?new_node.concat("1000"+":"):new_node.concat(speed.getText()+":");
    			new_node = max_capacity.getText().equals("")?new_node.concat("100000"):new_node.concat(max_capacity.getText());
    			name.setText("");
    			break;
    		case "Coal":
    			new_node = new_node.concat("k"+name.getText()+":");
		    	new_node = power.getText().equals("")?new_node.concat("10000"+":"):new_node.concat(power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("40.0"+":"):new_node.concat(preis.getText()+":");
    			new_node = speed.getText().equals("")?new_node.concat("1000"+":"):new_node.concat(speed.getText()+":");
    			new_node = max_capacity.getText().equals("")?new_node.concat("100000"):new_node.concat(max_capacity.getText());
    			name.setText("");
    			break;
    		case "Nuclear":
    			new_node = new_node.concat("n"+name.getText()+":");
		    	new_node = power.getText().equals("")?new_node.concat("10000"+":"):new_node.concat(power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("9.0"+":"):new_node.concat(preis.getText()+":");
    			new_node = speed.getText().equals("")?new_node.concat("1000"+":"):new_node.concat(speed.getText()+":");
    			new_node = max_capacity.getText().equals("")?new_node.concat("100000"):new_node.concat(max_capacity.getText());
    			name.setText("");
    			break;
    		case "Gas":
    			new_node = new_node.concat("a"+name.getText()+":");
		    	new_node = power.getText().equals("")?new_node.concat("10000"+":"):new_node.concat(power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("100.0"+":"):new_node.concat(preis.getText()+":");
    			new_node = speed.getText().equals("")?new_node.concat("1000"+":"):new_node.concat(speed.getText()+":");
    			new_node = max_capacity.getText().equals("")?new_node.concat("100000"):new_node.concat(max_capacity.getText());
    			name.setText("");
    			break;
    		case "City":
    			new_node = new_node.concat("c"+name.getText()+":");
    			new_node = power.getText().equals("")?new_node.concat("-10000"+":"):new_node.concat("-"+power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("0.2"+":"):new_node.concat(preis.getText()+":");
    			new_node = new_node.concat("0:");
    			new_node = max_capacity.getText().equals("")?new_node.concat("-100000"):new_node.concat("-"+max_capacity.getText());
    			name.setText("");
    			break;
    		case "Fabric":
    			new_node = new_node.concat("f"+name.getText()+":");
    			new_node = power.getText().equals("")?new_node.concat("-10000"+":"):new_node.concat("-"+power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("0.2"+":"):new_node.concat(preis.getText()+":");
    			new_node = new_node.concat("0:");
    			new_node = max_capacity.getText().equals("")?new_node.concat("-100000"):new_node.concat("-"+max_capacity.getText());
    			name.setText("");
    			break;
    		case "Town":
    			new_node = new_node.concat("o"+name.getText()+":");
    			new_node = power.getText().equals("")?new_node.concat("-1000"+":"):new_node.concat("-"+power.getText()+":");
    			new_node = preis.getText().equals("")?new_node.concat("0.2"+":"):new_node.concat(preis.getText()+":");
    			new_node = new_node.concat("0:");
    			new_node = max_capacity.getText().equals("")?new_node.concat("-10000"):new_node.concat("-"+max_capacity.getText());
    			name.setText("");
    			break;
    		case "Bus":
    			new_node = new_node.concat("t"+name.getText()+":");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0");
    			name.setText("");
    			break;
    		default:
    			new_node = new_node.concat("h"+name.getText()+":");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0:");
    			new_node = new_node.concat("0");
    			name.setText("");
    			break;
		}
		elements.add(0, new_node);
		counterNode++;
		
	}

	/**
	 * Connects two nodes with a Line
	 * 
	 * @param source
	 * @param sink
	 * @param result
	 * @param reactance
	 * @param max_capacity
	 */
	public void connect(JList<String> source, JList<String> sink, JTextField result, JTextField reactance,  JTextField max_capacity) {
		if(source.getSelectedIndex()==sink.getSelectedIndex()) {
			result.setText("Cannot connect node with itself");
			return;
		}
		if(reactance.getText().equals("")) {
			reactance.setText("0.5");
		}
		
		connectionList.add(0,"l"+Integer.toString(counterLine)+source.getSelectedValue().split(":")[0]+sink.getSelectedValue().split(":")[0]+":"+source.getSelectedValue().split(":")[0]+":"+sink.getSelectedValue().split(":")[0]+":"+reactance.getText()+":"+max_capacity.getText());
		counterLine++;
		
	}

	
	/**
	 * Activates/Deactivates a node
	 * 
	 * @param connections
	 * @param source
	 * @param sink
	 */
	public void toggleActiv(JList<String> connections,JList<String> source, JList<String> sink) {
		int[] selConect = connections.getSelectedIndices();
    	for(int i =0; i<selConect.length; i++) {
    		String current = connectionList.get(selConect[i]);
    		if(current.charAt(current.length()-1)=='_') {
    			connectionList.set(selConect[i],current.substring(0,current.length()-2));
    		}
    		else {
    			connectionList.set(selConect[i],current.concat(":_"));
    		}
    	}
    	int[] selElem = source.getSelectedIndices();
    	for(int i =0; i<selElem.length; i++) {
    		String current = elements.get(selElem[i]);
    		if(current.charAt(current.length()-1)=='_') {
    			elements.set(selElem[i],current.substring(0,current.length()-2));
    		}
    		else {
    			elements.set(selElem[i],current.concat(":_"));
    		}
    	}
		
	}
	
	private static void log(final String message) {
		Logger.getRootLogger().info("Controller: " + message);
	}
	
	
	/**
	 * Simulates the network
	 * 
	 * @param grid
	 */
	public void simulate(EnergyGrid grid) {
		try {
			Simulator.GurobiSolve(grid);
		} catch (GRBException e) {
			log("problem with gurobi");
			e.printStackTrace();
		}
	}

	public void simulateButton(JLabel result) {
		if(new_sd == null) {
			result.setText("No graph to simulate");
			return;
		}
    	log("Simulating Network...");
		simulate(new_sd.getGrid());
		
	}

	
	public void drawButton(JLabel result, JLabel graphviz, JFrame f) {
		JFileChooser chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Directory to Save");

	    chooser.setAcceptAllFileFilterUsed(false);
	    //    
	    if(new_sd!=null) {
	    	//simulate(new_sd.getGrid());
	    	long drawTime = System.nanoTime();
	    	log("Drawing Network");
		    draw(new_sd.getGrid(), result, graphviz, f);
		    log("Network Drawn after "+(System.nanoTime()-drawTime)/1000+"us");
	    }
	    	else {
	    		result.setText("Create first!");
	    	}
		
	}
	
	/**
	 * Draws the network with graphviz. 
	 * 
	 * @param grid
	 * @param result
	 * @param graphviz
	 * @param f
	 */
	public void draw(EnergyGrid grid, JLabel result, JLabel graphviz, JFrame f){
		
		try {
			FileWriter myWriter = new FileWriter("graphs/last_simulation.dot");
		      myWriter.write(Simulator.getGraphvizCode(grid));
		      myWriter.close();
	    } 
		catch (IOException ex) {
		      result.setText("Error saving file.");	    		    
		}
		float width = 0;
		float height = 0; 
		for(PowerNode node : grid.getNodes()) {
			if(node.getXPos()>width) {
				width=node.getXPos();
			}
			if(node.getYPos()>height) {
				height=node.getYPos();
			 }
		}

		Process p;
	    try {
    	  //p = Runtime.getRuntime().exec("dot -Tgif graphs/last_simulation.dot -o graphs/last_simulation.gif");
	      if(width!=0&&height!=0) {
	    	  p = Runtime.getRuntime().exec("dot -Kneato -Tgif graphs/last_simulation.dot -o graphs/last_simulation.gif");
	      }
	      else {
	    	  p = Runtime.getRuntime().exec("dot -Tgif graphs/last_simulation.dot -o graphs/last_simulation.gif");
	      }
	      p.waitFor();
	      
	      BufferedImage img = null;
	      try {
	          img = ImageIO.read(new File("graphs/last_simulation.gif"));
	      } catch (IOException e) {
	          e.printStackTrace();
	      }
	      
	      Image dimg = null;
	      if(width!=0 && height!=0) {
	    	   if(width>height) {
	 	    	  dimg = img.getScaledInstance(graphviz.getWidth(), Math.min(graphviz.getHeight(),(int) (height/width*graphviz.getWidth())),
	 		    	        Image.SCALE_SMOOTH);
	 	      }
	 	      else {
	 	    	  dimg = img.getScaledInstance(Math.min(graphviz.getWidth(), (int) (width/height*graphviz.getHeight())), graphviz.getHeight(),
	 		    	        Image.SCALE_SMOOTH);
	 	      }
	      }
	      else {
	    	  if(img.getWidth()>graphviz.getWidth()||img.getHeight()>graphviz.getHeight()) {
	    		  dimg = img.getScaledInstance(graphviz.getWidth(), graphviz.getHeight(),
			    	        Image.SCALE_SMOOTH);
	    	  }
	    	  else{
		    	  dimg = img;
	    	  }
	      }
	   
	      ImageIcon ii = new ImageIcon(dimg);	      
	      ii.getImage().flush();
	      graphviz.setIcon(ii);
	      f.revalidate();
	    } catch (InterruptedException | IOException ex) {
	      ex.printStackTrace();
	    }
	}

	/**
	 * Takes the elements in the boards and create the synergydemo object.
	 * 
	 * @param time
	 * 
	 */
	public void createGraph(JButton time) {
		long createTime = System.nanoTime();
		System.out.println("************************************");
		System.out.println("");
		log("Creating Network...");
    	if(elements.isEmpty()) {
    		log("No network to create. Abort.");
    		return;
    	}
		SynergyDemo3Factory factory = SynergyDemo3Factory.eINSTANCE;
		EnergyGrid new_eg = factory.createEnergyGrid();
		PowerPlan plan = factory.createPowerPlan();
		EnergyMarket market = factory.createEnergyMarket();
		new_sd = factory.createSynergyDemo();
		new_sd.setPowerPlan(plan);
		new_eg.setBalance(0);
		new_sd.setGrid(new_eg);
		new_sd.setMarket(market);
		new_sd.setTimeUnit(-1);
		randomizer = null;
		
		Map<String,PowerNode> nodes = new HashMap<>();
		PowerNode []powernodes = new PowerNode[elements.size()];
		for(int i = 0; i<elements.size(); i++) {
			
			String []element = elements.elementAt(i).split(":");
			
			if(producerTypes.contains(element[0].charAt(0))) {
				
				switch(element[0].charAt(0)) {
				case 'k': 
	    			powernodes[i] = factory.createCoalPlant();
	    			break;
				case 'w': 
	    			powernodes[i] = factory.createWindPlant();
	    			break;
				case 's': 
	    			powernodes[i] = factory.createSolarPlant();
	    			break;
				case 'n': 
	    			powernodes[i] = factory.createNuclearPlant();
	    			break;
				case 'a': 
	    			powernodes[i] = factory.createGasPlant();
	    			break;
				case 'd': 
	    			powernodes[i] = factory.createDieselPlant();
	    			break;
				case 'g': 
	    			powernodes[i] = factory.createGeothermalPlant();
	    			break;
	    		default:
	    			break;
				}
    			powernodes[i].setGrid(new_eg);
				((Producer) powernodes[i]).setPowerUpSpeed(Double.parseDouble(element[3]));
    			((Producer) powernodes[i]).setMaxProduction(Double.parseDouble(element[4]));
    			((Producer) powernodes[i]).setPriceProMW(Double.parseDouble(element[2]));
    			((Producer) powernodes[i]).setPowerDownSpeed(Double.parseDouble(element[3]));
    			powernodes[i].setMaxCapacity(Double.parseDouble(element[4]));
			}
			else if(consumerTypes.contains(element[0].charAt(0))) {
    				
    				switch(element[0].charAt(0)) {
    				case 'c': 
		    			powernodes[i] = factory.createCity();
		    			break;
    				case 'o': 
		    			powernodes[i] = factory.createTown();
		    			break;
    				case 'f': 
		    			powernodes[i] = factory.createFactory();
		    			break;
		    		default:
		    			break;
    				}
    			powernodes[i].setGrid(new_eg);
    			((Consumer) powernodes[i]).setPriceProMW(Double.parseDouble(element[2]));
    			((Consumer) powernodes[i]).setMaxCapacity(Double.parseDouble(element[4]));
			}
			else if(element[0].charAt(0)=='t') {
				
    			powernodes[i] = factory.createBus();
    			powernodes[i].setGrid(new_eg);
			}
			else {
				System.out.println();
			}

			powernodes[i].setName(element[0]);
			powernodes[i].setCurrentCapacity(Double.parseDouble(element[1]));
			if(element[element.length-1].equals("_")) {
				powernodes[i].setActivated(false);
			}
			if (element.length>5&&!element[5].equals("_")) {
    			powernodes[i].setXPos(Integer.parseInt(element[5]));
    			powernodes[i].setYPos(Integer.parseInt(element[6]));
			}
			
			nodes.put(element[0], powernodes[i]);
			
		}
		
		PowerLine []powerlines = new PowerLine[connectionList.size()];
		for(int i = 0; i<connectionList.size(); i++) {
			String []connection = connectionList.elementAt(i).split(":");
			powerlines[i] = factory.createPowerLine();
			powerlines[i].setGrid(new_eg);
			powerlines[i].setId(connection[0]);
			powerlines[i].setSource(nodes.get(connection[1]));
			powerlines[i].setSink(nodes.get(connection[2]));
			powerlines[i].setReactance(Double.parseDouble(connection[3]));
			if(!(powerlines[i].getSource() instanceof Bus)) {
				powerlines[i].setMaxCapacity(powerlines[i].getSource().getMaxCapacity());
			}
			else if (!(powerlines[i].getSink() instanceof Bus)) {
				powerlines[i].setMaxCapacity(powerlines[i].getSink().getMaxCapacity());
			}
			else {
				powerlines[i].setMaxCapacity(Double.parseDouble(connection[4]));
			}
			
			if(connection[connection.length-1].equals("_")) {
				powerlines[i].setActivated(false);
			}

			
		}
		
		
		time.setText("-1");
		Optimizer.resetReactiveOptimizationCount();
		
		eMoflonEMFUtil.saveModel(new_sd,"C:\\Users\\dedan\\Documents\\Masterarbeit\\runtime-workspace\\SynergyGUI3\\temp\\temp_model.xmi" );
		synergyDemoRules = new SynergyDemoRules(new_sd);
		log("Network Created after "+(System.nanoTime()-createTime)/1000+"us");
		simulate(new_sd.getGrid());
		
		// Prepare graph for live execution: analyse and make plan for the day 
		double loadingTime = System.nanoTime();
		log("Analysing model...");
		synergyDemoRules.analyseGraph(new_sd);
		log("Model Analysed after "+(System.nanoTime()-loadingTime)/1000+"us");

	}

	/**
	 * Saves an xmi file with the model
	 * 
	 * @param result
	 */
	public void save(JLabel result) {
		JFileChooser chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File(".//xmi"));
	    chooser.setDialogTitle("Directory to Save");

	    chooser.setAcceptAllFileFilterUsed(false);
	    //    
	    if(new_sd!=null) {
		    if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
		    	String filePath = ""+chooser.getSelectedFile();
		    	
		    		eMoflonEMFUtil.saveModel(new_sd, filePath);
		    		result.setText("Saved");
		      }
		    else {
		    	result.setText("");
		      }
		    }
	    	else {
	    		result.setText("Simulate first!");
	    	}
		
	}

	
	/**
	 * Copies the synergydemo object to the board
	 * 
	 * 
	 */
	public void loadBoard() {
		EnergyGrid eg = new_sd.getGrid();
		 for(PowerNode pn : eg.getNodes()) {
				String activated = "";
				if(!pn.isActivated()) {
					activated = ":_";
				}
				if(pn instanceof Producer) {
					if(pn.getName().charAt(0)=='p') {
						pn.setName(nodeType[(int) (Math.random()*5)]+pn.getName().substring(1));
					}
					double price = ((Producer)pn).getPriceProMW();
					double speed = ((Producer)pn).getPowerUpSpeed();
					//To make a price
					if(price==0) {
						price = Math.round(50*Math.random()+100); // Normal price is 100
					}
					if(speed==0) {
						speed = Math.round(50*Math.random()+100);
					}
					elements.add(elements.getSize(), pn.getName()+":"+pn.getCurrentCapacity()+":"+price+":"+speed+":"+((Producer)pn).getMaxCapacity()+":"+pn.getXPos()+":"+pn.getYPos()+activated);
				}
				else if(pn instanceof Consumer) {
					if((pn.getName().charAt(0)=='c')&&(!(pn instanceof City))) {
						pn.setName(nodeType[(int) ((Math.random()*3+5))]+pn.getName().substring(1));
					}
					double price = ((Consumer)pn).getPriceProMW();
					//To make a price
					if(price==0) {
						price = Math.round(50*Math.random()+100); // Normal price is 100
					}
					elements.add(elements.getSize(), pn.getName()+":"+pn.getCurrentCapacity()+":"+price+":0:"+(pn.getMaxCapacity()==0?"-1000":pn.getMaxCapacity())+":"+pn.getXPos()+":"+pn.getYPos()+activated);
				}
				else {
					elements.add(elements.getSize(), pn.getName()+":"+pn.getCurrentCapacity()+":0:0:0"+":"+pn.getXPos()+":"+pn.getYPos()+activated);
				}
			}
			for(PowerLine pl: eg.getLines()) {
				String activated = "";
				if(!pl.isActivated()) {
					activated = ":_";
				}
				connectionList.add(0, pl.getId()+":"+pl.getSource().getName()+":"+pl.getSink().getName()+":"+pl.getReactance()+":"+pl.getMaxCapacity()+activated);
			}
			counterNode = elements.getSize();
			counterLine = connectionList.getSize();
		
	}

	public void loadXmi(JButton time, JLabel result) {
		JFileChooser chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File(".//xmi//GOOD//"));
	    chooser.setDialogTitle("Load xmi");

	    chooser.setAcceptAllFileFilterUsed(false);
	   			
	    if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
	    	System.out.println("************************************");
    		System.out.println("");
	    	String filePath = ""+chooser.getSelectedFile();
	    	
	    	SynergyDemo3Package.eINSTANCE.getClass();
	    	long loadingTime = System.nanoTime();
	    	log("Loading Model mit Emoflon...");
	    	new_sd = (SynergyDemo) eMoflonEMFUtil.loadModel(filePath);
	    	log("Model Loaded after "+(System.nanoTime()-loadingTime)/1000+"us");
	    	elements.clear();
	    	connectionList.clear();		    		
    		
    		// Reset the time and plan
    		time.setText("-1");
    		new_sd.setTimeUnit(-1);
    		new_sd.getPowerPlan().getReactiveChanges().clear();
    		Optimizer.resetReactiveOptimizationCount();
    		
    		//draw(new_sd.getGrid());
    		//Reset the randomizer
    		 randomizer=null;
    		// Simulate the model
    		try {
				Simulator.GurobiSolve(new_sd.getGrid());
			} catch (GRBException e) {
				log("problem with gurobi");
				e.printStackTrace();
			}
    		synergyDemoRules = new SynergyDemoRules(new_sd);
    		loadingTime = System.nanoTime();
    		log("Analysing model... ");
    		synergyDemoRules.analyseGraph(new_sd);
    		log("Model Analysed after "+(System.nanoTime()-loadingTime)/1000+"us");
    		result.setText("Loaded "+chooser.getSelectedFile().getName());
	      }
	    else {
	    	result.setText("Not Loaded");
	      }
		
	}

	/**
	 * Parser for AUX Files
	 * 
	 * @param time
	 */
	public void loadAux(JButton time) {
		//synergyDemoRules = new SynergyDemoRules(new_sd);
		JFileChooser chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Load epc");

	    chooser.setAcceptAllFileFilterUsed(false);
	    
	    HashSet<String> buses = new HashSet<>();
	    HashSet<String> branches = new HashSet<>();
	    HashSet<String> generators = new HashSet<>();
	    HashSet<String> loads = new HashSet<>(); 
	    List<String[]> transformerMap = new LinkedList<>(); 
	    int elementCounter = 0;
	    if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
	    	String filePath = chooser.getSelectedFile().toString();
	    	
	    	//create Input Stream
	    	FileInputStream fIn = null;
			try {
				fIn = new FileInputStream(filePath);
			} catch (FileNotFoundException e1) {
				log("ERROR: FILE NOT FOUND");
				e1.printStackTrace();
			}
	    	
	    	
	    	new LinkedList<>();
	    	List<String> branchLines = getElements("Branch", fIn);
	    	List<String> transformerLines = getElements("Bus", fIn);
	    	List<String> busLines = getElements("Bus", fIn);
	    	List<String> generatorLines = getElements("Gen", fIn);
	    	List<String> loadLines = getElements("Load", fIn);
	    	List<String> breakerLines = getElements("Breaker", fIn);
	    	long loadingTime = System.nanoTime();
	    	log("Loading EPC");
	    	
	    	
	    	
	    	
	    	// Process each Line type
	    	for(String currentLine : busLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String bus = "t"+splitedLine[0].replace(" ", "")+":0:0:0:0";
    			buses.add(bus);
    			//System.out.println(bus);
	    	}
	    	
	    	int branchCounter = 0;
	    	for(String currentLine: branchLines) {
	    		String[] splitedLine = currentLine.split(" ");
	    		List<String> splitedLineUpdated = new ArrayList<String>(Arrays.asList(splitedLine));
	    		splitedLineUpdated.removeIf(n -> (n.equals("")));
	    		splitedLine = splitedLineUpdated.toArray(splitedLine);
    			String branch;
    			String source = "t"+splitedLine[0];
    			String sink = "t"+splitedLine[1];	
    			String reactance =splitedLine[10];
    			String max = splitedLine[16];
    			// If there is no max capacity, set a default

    			if(Double.parseDouble(max)==0.0) {
    				max = "1000.0";
    			}

    			branch = "l"+branchCounter+source+sink+":"+source+":"+sink+":"+reactance+":"+max;
    			//System.out.println(branch);
    			
    			branches.add(branch);
    			branchCounter++;
	    	}
	    	
	    	for(String currentLine : transformerLines) {
	    		String[] splitedLine = currentLine.split(" ");
	    		List<String> splitedLineUpdated = new ArrayList<String>(Arrays.asList(splitedLine));
	    		splitedLineUpdated.removeIf(n -> (n.equals("")));
	    		splitedLine = splitedLineUpdated.toArray(splitedLine);
    			String node1 = "t"+splitedLine[0];
    			String node2 = "t"+splitedLine[1];		
    		
    			transformerMap.add(new String[]{node1,node2});
	    	}
	    	
	    	for(String currentLine : generatorLines) {
	    		String[] splitedLine = currentLine.split(" ");
	    		List<String> splitedLineUpdated = new ArrayList<String>(Arrays.asList(splitedLine));
	    		splitedLineUpdated.removeIf(n -> (n.equals("")));
	    		splitedLine = splitedLineUpdated.toArray(splitedLine);
	    		
	    		currentLine.replace(" ","");
	    		String[] splitedLineWithoutSpace = currentLine.split("\"");
	    		
    			String branch;
    			String generator;
    			splitedLineWithoutSpace[61].replace("Natural ", "");
    			
    			String source = nodeType[(int) (Math.random()*5)]+splitedLineWithoutSpace[61].replace("\"", "")+elementCounter;
    			String sink = "t"+splitedLine[0];	

    			// Warum gibt es negative Powers? Macht keinen Sinn
    			String power = splitedLine[8];
    			String maxGen = splitedLine[9];

    			// Speedup of producers fixed
    			
    			branch = "l"+source+sink+":"+source+":"+sink+":0:0";
    			generator = source+":"+power+":0.04:100:"+maxGen+":0:0";
    			
    			branches.add(branch);
    			generators.add(generator);
    			elementCounter++;
	    	}
	    	
	    	for(String currentLine : loadLines) {
	    		String[] splitedLine = currentLine.split(" ");
	    		List<String> splitedLineUpdated = new ArrayList<String>(Arrays.asList(splitedLine));
	    		splitedLineUpdated.removeIf(n -> (n.equals("")));
	    		splitedLine = splitedLineUpdated.toArray(splitedLine);
    			String branch;
    			String load;
    			
    			String sink = "c"+elementCounter;
    			String source = "t"+splitedLine[0];	
    			
    			// Warum gibt es positive Zahlen hier? das macht keinen Sinn...
    			String power = "-"+Math.abs(Double.parseDouble(splitedLine[5]));
    			branch = "l"+source+sink+":"+source+":"+sink+":0:0";
    			load = sink+":"+power+":0:0:0";
    			//System.out.println(branch);
    			//System.out.println(load);
    			
    			branches.add(branch);
    			loads.add(load);
    			elementCounter++;
	    	}
	    	
	    	new HashMap<>();
	    	if(breakerLines != null) {
		    	for(String currentLine : breakerLines) {
		    		String[] splitedLine = currentLine.split("\"");
	    			
	    			String node1 = "t"+splitedLine[1].replace(" ", "");
	    			String node2 = "t"+splitedLine[3].replace(" ", "");	
	    		
	    			transformerMap.add(new String[]{node1,node2});
		    	}
	    	}


	    	    HashSet<String> allnodesT = new HashSet<>();
	    	    for(String s : buses) {
	    	    	allnodesT.add(s.split(":")[0]);
	    	    }
	    	    // All buses connected through a transformer/break are just one transformer in this architecture
	    	    
	    	    HashMap<String, String> transformerConversion = new HashMap<>();
	    	    for(String[] nodePair : transformerMap) {
	    	    	String node1 = nodePair[0];
	    	    	String node2 = nodePair[1];
	    	    	String conversion;
	    	    	if(transformerConversion.keySet().contains(node1)) {
	    	    		conversion = transformerConversion.get(node1);
	    	    		if(node2.equals(conversion)) {
	    	    			continue;
	    	    		}
	    	    		if(transformerConversion.keySet().contains(node2)) {
	    	    			// Update values of two transformers that go together
	    	    			String toUpdate = transformerConversion.get(node2);
	    	    			if(!toUpdate.equals(conversion)) {
		    	    			HashSet<String> toRemove = new HashSet<>();
		    	    			for(Entry<String,String> entry : transformerConversion.entrySet()) {
		    	    				if(entry.getValue().equals(toUpdate)) {
		    	    					toRemove.add(entry.getKey());
		    	    				}
		    	    			}
		    	    			for(String tr : toRemove) {
		    	    				transformerConversion.remove(tr);
	    	    					transformerConversion.put(tr, conversion);
		    	    			}
		    	    			branches = updateBranchesAux(branches, node1, toUpdate, conversion);
		    	    			for(String s : buses) {
		    	    				String []split = s.split(":");
		    	    				if(split[0].equals(toUpdate)) {
		    	    					buses.remove(s);
		    	    					break;	
		    	    				}
		    	    			}
	    	    			}
	    	    		}
	    	    		else {
	    	    			transformerConversion.put(node2, conversion);
	    	    		}
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node2)) {
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    	    	else if(transformerConversion.keySet().contains(node2)) {
	    	    		conversion = transformerConversion.get(node2);
	    	    		if(node1.equals(conversion)) {
	    	    			continue;
	    	    		}
	    	    		transformerConversion.put(node1, conversion);
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node1)) {
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    	    	else {
	    	    		conversion = node1;
	    	    		transformerConversion.put(node1, conversion);
	    	    		transformerConversion.put(node2, conversion);
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node2)) {
    	    					//System.out.println(node2);
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    			branches = updateBranches(branches, node1, node2, conversion);
	    	    }
	    	    
	    	    // Branches invalid cause of transformers
	    	   
	    	    HashSet<String> branchesToRemove = new HashSet<>();
	    	    for(String s : branches) {
    				String []split = s.split(":");
    				String source = split[1];
    				String sink = split[2];
    				if(transformerConversion.keySet().contains(source)&&!transformerConversion.values().contains(source)) {
    					branchesToRemove.add(s);
    				}
    				if(transformerConversion.keySet().contains(sink)&&!transformerConversion.values().contains(sink)) {
    					branchesToRemove.add(s);
    				}
    			}
	    	    branches.removeAll(branchesToRemove);
	    	    
	    	    // This is a test
	    	    
	    	    allnodesT = new HashSet<>();
	    	    for(String s : buses) {
	    	    	allnodesT.add(s.split(":")[0]);
    				//System.out.println(s.split(":")[0]);
	    	    }
	    	    for(String s : branches) {
    				String []split = s.split(":");
    				String node1 = split[1];
    				String node2 = split[2];
    				if(node1.charAt(0)!= 'p' && node1.charAt(0)!='c')
    					if(!allnodesT.contains(node1)) {
    			    	    //System.out.println();
    					}    	    				
    				if(node2.charAt(0)!= 'p' && node2.charAt(0)!='c')
    					if(!allnodesT.contains(node2)) {
    			    	    //System.out.println();
    					}
	    	    }
	    	    
	    	elements.clear();
	    	connectionList.clear();
	    	for(String s : buses) {
	    		elements.add(0, s);
	    	}
	    	for(String s : generators) {
	    		elements.add(0, s);
	    	}
	    	for(String s : loads) {
	    		elements.add(0, s);
	    	}
	    	for(String s : branches) {
	    		connectionList.add(0, s);
	    	}
	    	
	    	// Reset the time
    		time.setText("-1");
	    	
	    	log("EPC Loaded to board after "+(System.nanoTime()-loadingTime)/1000+"us");

	    }
		
	 }
	
	private List<String> getElements(String type, FileInputStream fIn){
		
		try {
			fIn.getChannel().position(0);
		} catch (IOException e) {
			log("problem with GET ELEMENTS");
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(fIn));
		String typeInFile = type;
		if(type.equals("Transformer")) {
			typeInFile = "Branch";
		}
		if(type.equals("TRANSFORMER")) {
			typeInFile = "BRANCH";
		}
		String lineOfFile = "";
    	// Load the lines in memory
		List<String> elements = new LinkedList<>();
    	try {
    		lineOfFile = br.readLine();
    		while(lineOfFile!=null) {
	    		while(!lineOfFile.contains("DATA ("+typeInFile)){ //DATA (Branch
		    		lineOfFile = br.readLine();
		    		if(lineOfFile==null) {
		    			break;
		    		}
	    		}
	    		if(!(lineOfFile==null)) {
	    			
		    		while(!(lineOfFile = br.readLine()).contains("{")) {
	    				
	    			}
		    		while(!(lineOfFile = br.readLine()).contains("}")) {
		    			if((type.equals("Transformer") && !lineOfFile.contains("Transformer"))||(type.equals("TRANSFORMER") && !lineOfFile.contains("Transformer"))) {
		    				continue;
		    			}				    			
		    			else if((type.equals("Branch") && !lineOfFile.contains("Line"))||(type.equals("BRANCH") && !lineOfFile.contains("Line"))) {
		    				continue;
		    			}
		    			else {
		    				elements.add(lineOfFile);
		    			}
		    			
		    		}
	    		}
    		}
    		
    	}
    	catch(Exception ex){
    		System.out.println("ERROR BY PARSING");
    		System.out.println(ex.getMessage());
    		return null;
    	}
    	
    	if(elements.isEmpty()) {
    		if(!type.equals(type.toUpperCase())) {
    			elements = getElements(type.toUpperCase(), fIn);
    		}
    	}
    	return elements;
	}
	
	// With the new conversion for a pair of nodes, update all the branches to match the conversion.
	private HashSet<String> updateBranchesAux(HashSet<String> branches, String node1, String node2, String conversion) {
		HashSet<String> branchesUpdated = new HashSet<>();
		for(String s : branches) {
			String []split = s.split(":");
			String newSource = split[1];
			String newSink = split[2];
			String newBranch = "";
			if(split[1].equals(node2)) {
				newSource = conversion;
			}
			if (split[2].equals(node2)) {
				newSink = conversion;	
			}
			if(split[1].equals(node1)) {
				newSource = conversion;
			}
			if (split[2].equals(node1)) {
				newSink = conversion;
			}
			if(newSource.equals(newSink)) {
				continue;
			}
			newBranch = split[0]+":"+newSource+":"+newSink+":"+split[3]+":"+split[4];
			branchesUpdated.add(newBranch);
		}
		return branchesUpdated;
		
	}

	/**
	 * Parser for EPC Files
	 * 
	 * @param result
	 * @param connections
	 * @param time
	 */
	public void loadEpc(JLabel result, JList<String> connections, JButton time) {
		//synergyDemoRules = new SynergyDemoRules(new_sd);
		JFileChooser chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Load epc");

	    chooser.setAcceptAllFileFilterUsed(false);
	    
	    HashSet<String> buses = new HashSet<>();
	    HashSet<String> branches = new HashSet<>();
	    HashSet<String> generators = new HashSet<>();
	    HashSet<String> loads = new HashSet<>(); 
	    List<String[]> transformerMap = new LinkedList<>(); 
	    int elementCounter = 0;
	    if (chooser.showOpenDialog(new JPanel()) == JFileChooser.APPROVE_OPTION) { 
	    	String filePath = chooser.getSelectedFile().toString();
	    	BufferedReader br = null;
	    	String lineOfFile = null;
	    	new LinkedList<>();
	    	List<String> branchLines = new LinkedList<>();
	    	List<String> transformerLines = new LinkedList<>();
	    	List<String> busLines = new LinkedList<>();
	    	List<String> generatorLines = new LinkedList<>();
	    	List<String> loadLines = new LinkedList<>();
	    	List<String> breakerLines = new LinkedList<>();
	    	long loadingTime = System.nanoTime();
	    	log("Loading EPC");
	    	// Load the lines in memory
	    	try {
	    		br = new BufferedReader(new FileReader(filePath));
	    		/*while ((lineOfFile = br.readLine()) != null) {
	    			linesOfFile.add(lineOfFile);
	    		}
	    		linesOfFile.add(null);*/
	    		while(!(lineOfFile = br.readLine()).contains("bus data")) {
	    			
	    		}
	    		while(!(lineOfFile = br.readLine()).contains("branch data")) {
	    			busLines.add(lineOfFile);
	    		}
	    		while(!(lineOfFile = br.readLine()).contains("transformer data")) {
	    			branchLines.add(lineOfFile+br.readLine());
	    		}
	    		while(!(lineOfFile = br.readLine()).contains("generator data")) {
	    			transformerLines.add(lineOfFile+br.readLine()+br.readLine()+br.readLine());
	    		}
	    		while(!(lineOfFile = br.readLine()).contains("load data")) {
	    			generatorLines.add(lineOfFile+br.readLine());
	    		}
	    		while(!(lineOfFile = br.readLine()).contains("shunt data")) {
	    			loadLines.add(lineOfFile);
	    		}
	    		while(((lineOfFile = br.readLine())!=null) && !lineOfFile.contains("breaker")) {
	    			
	    		}
	    		if(lineOfFile != null) {
	    			while(!(lineOfFile = br.readLine()).contains("injgroup data")) {
		    			breakerLines.add(lineOfFile);
		    		}
	    		}
	    		
	    	}
	    	catch(Exception ex){
	    		System.out.println("ERROR BY PARSING");
	    		System.out.println(ex.getMessage());
	    	}
	    	
	    	
	    	// Process each Line type
	    	for(String currentLine : busLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String bus = "t"+splitedLine[0].replace(" ", "")+":0:0:0:0";
    			buses.add(bus);
    			//System.out.println(bus);
	    	}
	    	
	    	int branchCounter = 0;
	    	for(String currentLine: branchLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String branch;
    			
    			
    			String source = "t"+splitedLine[0].replace(" ", "");
    			
    			String []thirdElement = splitedLine[2].split(" ");
    			List<String> restList = new LinkedList<String>(Arrays.asList(thirdElement));
    			restList.removeAll(Collections.singleton(""));
    			thirdElement = restList.toArray(new String[0]);
    			
    			String sink = "t"+thirdElement[1];	
    			
    			String []rest = splitedLine[8].split(" ");
    			restList = new ArrayList<String>(Arrays.asList(rest));
    			restList.removeAll(Collections.singleton(""));
    			rest = restList.toArray(new String[0]);
    			
    			
    			String reactance = rest[3].replace(" ", "");
    			String max = rest[5].replace(" ", "");
    			// If there is no max capacity, set a default
    			if(Double.parseDouble(max)==0.0) {
    				max = "1000.0";
    			}

    			branch = "l"+branchCounter+source+sink+":"+source+":"+sink+":"+reactance+":"+max;
    			//System.out.println(branch);
    			
    			branches.add(branch);
    			branchCounter++;
	    	}
	    	
	    	for(String currentLine : transformerLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String node1 = "t"+splitedLine[0].replace(" ", "");
    
    			String []thirdElement = splitedLine[2].split(" ");
    			List<String> restList = new ArrayList<String>(Arrays.asList(thirdElement));
    			restList.removeAll(Collections.singleton(""));
    			thirdElement = restList.toArray(new String[0]);
    			
    			String node2 = "t"+thirdElement[1];		
    		
    			transformerMap.add(new String[]{node1,node2});
	    	}
	    	
	    	for(String currentLine : generatorLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String branch;
    			String generator;
    			String speedup;
    			//String[] producers = {"k", "g", "n", "w", "s", "a", "d"};
    			Character producerType = producerTypes.get((int)(7*Math.random()));
    			String source = producerType+""+elementCounter;
    			String sink = "t"+splitedLine[0].replace(" ", "");	
    			
    			String []rest = splitedLine[8].split(" ");
    			List<String> restList = new ArrayList<String>(Arrays.asList(rest));
    			restList.removeAll(Collections.singleton(""));
    			rest = restList.toArray(new String[0]);
    			
    			// Warum gibt es negative Powers? Macht keinen Sinn
    			double power = Double.parseDouble(rest[5].replace(" ", ""));
    			String maxGen = rest[6].replace(" ", "");

    			// Speedup of producers fixed
    			
    			speedup = "10000";
    			branch = "l"+source+sink+":"+source+":"+sink+":0:0";
    			generator = source+":"+power+":0.04:"+speedup+":"+maxGen+":0:0";
    			
    			branches.add(branch);
    			generators.add(generator);
    			elementCounter++;
	    	}
	    	
	    	for(String currentLine : loadLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			String branch;
    			String load;
    			
    			//String[] producers = {"k", "g", "n", "w", "s", "a", "d"};
    			Character consumerType = consumerTypes.get((int)(3*Math.random()));
    			String sink = consumerType+""+elementCounter;
    			String source = "t"+splitedLine[0].replace(" ", "");	
    			
    			String []rest = splitedLine[6].split(" ");
    			List<String> restList = new ArrayList<String>(Arrays.asList(rest));
    			restList.removeAll(Collections.singleton(""));
    			rest = restList.toArray(new String[0]);
    			
    			// Warum gibt es positive Zahlen hier? das macht keinen Sinn...
    			String power = "-"+Math.abs(Double.parseDouble(rest[2].replace(" ", "")));
    			branch = "l"+source+sink+":"+source+":"+sink+":0:0";
    			load = sink+":"+power+":0.2:0:100";
    			//System.out.println(branch);
    			//System.out.println(load);
    			
    			branches.add(branch);
    			loads.add(load);
    			elementCounter++;
	    	}
	    	
	    	new HashMap<>();
	    	for(String currentLine : breakerLines) {
	    		String[] splitedLine = currentLine.split("\"");
    			
    			String node1 = "t"+splitedLine[1].replace(" ", "");
    			String node2 = "t"+splitedLine[3].replace(" ", "");	
    		
    			transformerMap.add(new String[]{node1,node2});
	    	}
	    	
	    	    
	    	    
	    	    // test

	    	    HashSet<String> allnodesT = new HashSet<>();
	    	    for(String s : buses) {
	    	    	allnodesT.add(s.split(":")[0]);
	    	    }
	    	    
	    	    // All buses connected through a transformer/break are just one transformer in this architecture
	    	    
	    	    HashMap<String, String> transformerConversion = new HashMap<>();
	    	    for(String[] nodePair : transformerMap) {
	    	    	String node1 = nodePair[0];
	    	    	String node2 = nodePair[1];
	    	    	String conversion;
	    	    	if(transformerConversion.keySet().contains(node1)) {
	    	    		conversion = transformerConversion.get(node1);
	    	    		if(node2.equals(conversion)) {
	    	    			continue;
	    	    		}
	    	    		if(transformerConversion.keySet().contains(node2)) {
	    	    			// Update values of two transformers that go together
	    	    			String toUpdate = transformerConversion.get(node2);
	    	    			if(!toUpdate.equals(conversion)) {
		    	    			HashSet<String> toRemove = new HashSet<>();
		    	    			for(Entry<String,String> entry : transformerConversion.entrySet()) {
		    	    				if(entry.getValue().equals(toUpdate)) {
		    	    					toRemove.add(entry.getKey());
		    	    				}
		    	    			}
		    	    			for(String tr : toRemove) {
		    	    				transformerConversion.remove(tr);
	    	    					transformerConversion.put(tr, conversion);
		    	    			}
		    	    			branches = updateBranches(branches, node1, toUpdate, conversion);
		    	    			for(String s : buses) {
		    	    				String []split = s.split(":");
		    	    				if(split[0].equals(toUpdate)) {
		    	    					buses.remove(s);
		    	    					break;	
		    	    				}
		    	    			}
	    	    			}
	    	    		}
	    	    		else {
	    	    			transformerConversion.put(node2, conversion);
	    	    		}
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node2)) {
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    	    	else if(transformerConversion.keySet().contains(node2)) {
	    	    		conversion = transformerConversion.get(node2);
	    	    		if(node1.equals(conversion)) {
	    	    			continue;
	    	    		}
	    	    		transformerConversion.put(node1, conversion);
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node1)) {
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    	    	else {
	    	    		conversion = node1;
	    	    		transformerConversion.put(node1, conversion);
	    	    		transformerConversion.put(node2, conversion);
	    	    		for(String s : buses) {
    	    				String []split = s.split(":");
    	    				if(split[0].equals(node2)) {
    	    					//System.out.println(node2);
    	    					buses.remove(s);
    	    					break;	
    	    				}
    	    			}
	    	    	}
	    			branches = updateBranches(branches, node1, node2, conversion);
	    	    }
	    	    
	    	    // Branches invalid cause of transformers
	    	   
	    	    HashSet<String> branchesToRemove = new HashSet<>();
	    	    for(String s : branches) {
    				String []split = s.split(":");
    				String source = split[1];
    				String sink = split[2];
    				if(transformerConversion.keySet().contains(source)&&!transformerConversion.values().contains(source)) {
    					branchesToRemove.add(s);
    				}
    				if(transformerConversion.keySet().contains(sink)&&!transformerConversion.values().contains(sink)) {
    					branchesToRemove.add(s);
    				}
    			}
	    	    branches.removeAll(branchesToRemove);
	    	   
	    	    
	    	elements.clear();
	    	connectionList.clear();
	    	for(String s : buses) {
	    		elements.add(0, s);
	    	}
	    	for(String s : generators) {
	    		elements.add(0, s);
	    	}
	    	for(String s : loads) {
	    		elements.add(0, s);
	    	}
	    	for(String s : branches) {
	    		connectionList.add(0, s);
	    	}
	    	
	    	// Reset the time
    		time.setText("-1");
    		
	    	log("EPC Loaded to board after "+(System.nanoTime()-loadingTime)/1000+"us");

	    }
		
	 }
	
	// With the new conversion for a pair of nodes, update all the branches to match the conversion.
	private HashSet<String> updateBranches(HashSet<String> branches, String node1, String node2, String conversion) {
		HashSet<String> branchesUpdated = new HashSet<>();
		for(String s : branches) {
			String []split = s.split(":");
			String newSource = split[1];
			String newSink = split[2];
			String newBranch = "";
			if(split[1].equals(node2)) {
				newSource = conversion;
			}
			if (split[2].equals(node2)) {
				newSink = conversion;	
			}
			if(split[1].equals(node1)) {
				newSource = conversion;
			}
			if (split[2].equals(node1)) {
				newSink = conversion;
			}
			if(newSource.equals(newSink)) {
				continue;
			}
			newBranch = split[0]+":"+newSource+":"+newSink+":"+split[3]+":"+split[4];
			branchesUpdated.add(newBranch);
		}
		return branchesUpdated;		
	}
	
	

	/**
	 * Remove element from the board
	 * 
	 * @param source
	 * @param sink
	 */
	public void remove(JList<String> source, JList<String> sink) {
		if(source.isSelectionEmpty()) {
			return;
		}
		
		for(int i = 0; i<connectionList.size(); i++) {
			if(connectionList.elementAt(i).split(":")[1].equals(source.getSelectedValue().split(":")[0])
					||connectionList.elementAt(i).split(":")[2].equals(source.getSelectedValue().split(":")[0])) {
				connectionList.remove(i);
				i--;
			}
		}
		elements.remove(source.getSelectedIndex());
		source.setSelectedIndex(0);
		sink.setSelectedIndex(0);
		
	}

	/**
	 * in-Betrieb Simulation. 
	 * 
	 * @param time
	 */
	public void time(JButton time) {
		new HashMap<>();
		if(randomizer == null) {
			log("Creating randomizer...");
    		randomizer = new Randomizer(10,24,new_sd.getMarket(), new_sd.getDay());
    		log("Randomizer created");
		}
		int timeValue = Integer.parseInt(time.getText());
		if(timeValue==-1) {
			if(new_sd == null) {
				log("There is no graph. Abort.");
				return;
			}
			// Set the initial values of the contracts if there are some.
			if(!new_sd.getMarket().getContracts().isEmpty()) {
				Optimizer.prepareGraph(new_sd);
			}
			Optimizer.calculateDayPlan(new_sd, randomizer);
			new_sd.setDay(0);
		}
		else if(timeValue==15) {
			
			Optimizer.calculateDayPlan(new_sd, randomizer);
		}
		else if(timeValue==23) {
			new_sd.setDay(new_sd.getDay()+1);
			randomizer.getSimulator().updateEnvironment();
		}
		timeValue=(timeValue+1)%24;
		
		

		if(synergyDemoRules == null) {
			synergyDemoRules = new SynergyDemoRules(new_sd);
		}
		new_sd.setTimeUnit(timeValue);
		time.setText(Integer.toString(timeValue));
		System.out.println();
		log("Time Unit: "+new_sd.getTimeUnit());
		
		log("Reset due Offerings");
		Optimizer.resetOfferings(new_sd, timeValue);
		
		
		log("Randomize");
    	randomizer.randomize(new_sd,timeValue, new_sd.getDay());
    	
    	// Save the values before the possible optimization to calculate Entschaedigungen
		HashMap<PowerNode, Double> lastValues = new HashMap<>();
		for(PowerNode node : new_sd.getGrid().getNodes()) {
			lastValues.put(node, node.getCurrentCapacity());
		}
    	
		//log("Analysing model...");
		synergyDemoRules.analyseGraph(new_sd);
		simulate(new_sd.getGrid());

		log("Analysing model...");
		synergyDemoRules.analyseGraph(new_sd);
		Optimizer.checkOptimization(synergyDemoRules, new_sd);
		
		// Calculate Entschaedigungen
		log("Entschädigung Cost was: "+Optimizer.calculateEntschaedigung( new_sd, lastValues)); 
	}
	
	

	public void optimize() {
		//Optimizer.calculateOptimizationPlan(new_sd);
		if(synergyDemoRules == null) {
			synergyDemoRules = new SynergyDemoRules(new_sd);
		}
		//synergyDemoRules.analyseGraph(new_sd);
		//simulate(new_sd.getGrid());
		//draw(new_sd.getGrid());
		
		long time = System.nanoTime();
		Optimizer.performReactiveOptimization(new_sd, false,10,true,true);
		log("Optimization time: "+(System.nanoTime()-time)/1000+"us");
		//Optimizer.performProactiveOptimization(new_sd, 10,false,false,null);
		log("Analysing model...");
		synergyDemoRules.analyseGraph(new_sd);
		
	}

	
	/**
	 * Performs 2 Days of in-Betrieb-Simulation
	 * 
	 * @param time
	 * @param loadXMI
	 */
	public void start(JButton time, JButton loadXMI) {
		int timestepsToSimulate = 48;
		for(int i =0; i<timestepsToSimulate; i++) {
			time.doClick();
		}
		System.out.println();
		log("Reactive optimizations: "+Optimizer.reactiveOptimizationCount+"/"+timestepsToSimulate);
		loadXMI.doClick();
	}

	public void removeConnection(JList<String> connections) {
		if(connections.isSelectionEmpty()) {
			return;
		}


		connectionList.remove(connections.getSelectedIndex());
		connections.setSelectedIndex(0);
		
	}
	
}


