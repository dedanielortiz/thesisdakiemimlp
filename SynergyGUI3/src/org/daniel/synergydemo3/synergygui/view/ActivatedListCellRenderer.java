package org.daniel.synergydemo3.synergygui.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

public class ActivatedListCellRenderer extends DefaultListCellRenderer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        String val = (String)value;
        if (val.charAt(val.length()-1)=='_') {
            c.setBackground(Color.YELLOW);
            if(isSelected) {
            	c.setBackground(Color.getHSBColor(53, 68, 50));
            }
        }
        else {
        	c.setBackground(Color.WHITE);
        	if(isSelected) {
            	c.setBackground(Color.LIGHT_GRAY);
            }
        }
        
        return c;
    }
}