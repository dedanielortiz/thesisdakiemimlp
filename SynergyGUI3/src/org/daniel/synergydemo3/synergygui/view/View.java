package org.daniel.synergydemo3.synergygui.view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

import org.daniel.synergydemo3.synergygui.controller.Controller;

public class View implements KeyListener{
	
	JFrame f;
	
	JButton add;  
	JButton toggleActiv;  
	JButton connect;  
	JButton save;  
	JButton loadXmi;  
	JButton loadBoard;  
	JButton loadEpc;  
	JButton loadAux;  
	JButton remove;  
	JButton remove_con;  
	JButton createGraph;  
	JButton simulate;  
	JButton draw;  
	JButton time;  
	JButton optimize;  
	JButton start;  
    
	JComboBox<String> combo_element;
	
	
    JLabel select;
    JLabel powerL;
    JLabel reactanceL;
    JLabel nameL;
    JLabel preisL;
    JLabel speedL;
    JLabel max_capacityL;
    JLabel result;
    JLabel xPosL;
    JLabel yPosL;
    JLabel timeL;
    JLabel priceResultL;
    
    JTextField power;
    JTextField reactance;
    JTextField max_capacity;
    JTextField name;
    JTextField preis;
    JTextField speed;
    JTextField xPos;
    JTextField yPos;
    
    JLabel graphviz;
    
    
    JList<String> source;
	JList<String> sink;
    
    JList<String> connections; 

	public Controller getController() {
		return controller;
	}

	public void setController(Controller c) {
		this.controller = c;
	}

	protected Controller controller;
	
	public View(Controller c) {
		
		
	    f = new JFrame();
	    //         
	    //label.setHorizontalAlignment(JLabel.CENTER);  
	    //label.setPreferredSize(new Dimension(400,100)); 
		
	    controller = c;
		combo_element=new JComboBox<>(controller.element); 
		
		add=new JButton("Add"); 
		toggleActiv=new JButton("Act/Deact"); 
		remove=new JButton("Remove");  
		connect=new JButton("Connect");  
		save=new JButton("Save");  
		loadXmi=new JButton("xmi");  
		loadBoard=new JButton("Board");  
		loadEpc=new JButton("EPC");  
		loadAux=new JButton("AUX");  
		createGraph=new JButton("Create");  
		draw=new JButton("Draw");  
		simulate=new JButton("Simulate");  
	    remove_con=new JButton("Remove");
	    time=new JButton("-1");
	    optimize=new JButton("Optimize");
	    start=new JButton("START");
	    
	    select = new JLabel("Select");
	    powerL = new JLabel("Power");
	    reactanceL = new JLabel("React.");
	    nameL = new JLabel("Name");
	    preisL = new JLabel("€/KW/u ");
	    speedL = new JLabel("Max. KW/u");
	    max_capacityL = new JLabel("Max Cap.");
	    xPosL = new JLabel("X");
	    yPosL = new JLabel("Y");
	    result = new JLabel("");
	    timeL = new JLabel("Time");
	    priceResultL = new JLabel("");
	    
	    power = new JTextField();
	    reactance = new JTextField();
	    max_capacity = new JTextField();
	    name = new JTextField();
	    preis = new JTextField();
	    speed = new JTextField();
	    xPos = new JTextField();
	    yPos = new JTextField();
	    graphviz = new JLabel();
	    
	    
	    controller.elements = new DefaultListModel<>(); 
	    controller.connectionList = new DefaultListModel<>(); 
	    
        connections = new JList<>(controller.connectionList); 
        source = new JList<>(controller.elements); 
		sink = new JList<>(controller.elements);
		
		
		
		connections.setCellRenderer(new ActivatedListCellRenderer());
		source.setCellRenderer(new ActivatedListCellRenderer());
		sink.setCellRenderer(new ActivatedListCellRenderer());
		
		source.addMouseListener(new MouseAdapter() { 
	          public void mousePressed(MouseEvent me) { 
	        	  connections.clearSelection(); 
	          } 
	        }); 
		sink.addMouseListener(new MouseAdapter() { 
	          public void mousePressed(MouseEvent me) { 
	        	  connections.clearSelection(); 
	          } 
	        }); 
		connections.addMouseListener(new MouseAdapter() { 
	          public void mousePressed(MouseEvent me) { 
	        	  source.clearSelection(); 
	        	  sink.clearSelection(); 
	          } 
	        }); 
		
		add.addKeyListener(this);
		remove.addKeyListener(this);
		connect.addKeyListener(this);
		save.addKeyListener(this);
		createGraph.addKeyListener(this);
		draw.addKeyListener(this);
		simulate.addKeyListener(this);
		remove_con.addKeyListener(this);
		select.addKeyListener(this);
		powerL.addKeyListener(this);
		reactanceL.addKeyListener(this);
		nameL.addKeyListener(this);
		max_capacityL.addKeyListener(this);
		power.addKeyListener(this);
		preis.addKeyListener(this);
		speed.addKeyListener(this);
		reactance.addKeyListener(this);
		max_capacity.addKeyListener(this);
		connections.addKeyListener(this);
		source.addKeyListener(this);
		sink.addKeyListener(this);
		xPos.addKeyListener(this);
		yPos.addKeyListener(this);
		graphviz.addKeyListener(this);
		
		powerL.setLocation(167,12);
		powerL.setSize(67,25);
		f.getContentPane().add(powerL);

		combo_element.setLocation(19,37);
		combo_element.setSize(145,25);
		combo_element.setEditable(false );
		f.getContentPane().add(combo_element);

		power.setLocation(167,37);
		power.setSize(77,25);
		f.getContentPane().add(power);

		name.setLocation(251,37);
		name.setSize(85,25);
		name.setText("");
		f.getContentPane().add(name);

		nameL.setLocation(251,12);
		nameL.setSize(75,25);
		f.getContentPane().add(nameL);
		
		preis.setLocation(335,37);
		preis.setSize(85,25);
		f.getContentPane().add(preis);

		preisL.setLocation(335,12);
		preisL.setSize(75,25);
		f.getContentPane().add(preisL);
		
		speed.setLocation(419,37);
		speed.setSize(85,25);
		f.getContentPane().add(speed);

		speedL.setLocation(419,12);
		speedL.setSize(75,25);
		f.getContentPane().add(speedL);
		
		int xShift = 168;
		
		xPosL.setLocation(xShift+345,12);
		xPosL.setSize(75,25);
		f.getContentPane().add(xPosL);
		
		yPosL.setLocation(xShift+379,12);
		yPosL.setSize(75,25);
		f.getContentPane().add(yPosL);

		xPos.setLocation(xShift+345,37);
		xPos.setSize(25,25);
		f.getContentPane().add(xPos);
		
		yPos.setLocation(xShift+379,37);
		yPos.setSize(25,25);
		f.getContentPane().add(yPos);
		
		add.setLocation(xShift+413,37);
		add.setSize(60,25);
		f.getContentPane().add(add);
		
		toggleActiv.setLocation(xShift+482,37);
		toggleActiv.setSize(76,25);
		f.getContentPane().add(toggleActiv);

		source.setLocation(20,70);
		source.setSize(201,870);
		f.getContentPane().add(source);

		sink.setLocation(245,71);
		sink.setSize(201,870);
		f.getContentPane().add(sink);

		connections.setLocation(503,69);
		connections.setSize(270,870);
		f.getContentPane().add(connections);
		
		graphviz.setLocation(780,69);
		graphviz.setSize(1000,870);
		f.getContentPane().add(graphviz);

		reactance.setLocation(xShift+567,37);
		reactance.setSize(76,25);
		reactance.setText("0.5");
		f.getContentPane().add(reactance);
		
		max_capacity.setLocation(xShift+652,37);
		max_capacity.setSize(76,25);
		max_capacity.setText("1500");
		f.getContentPane().add(max_capacity);
		
		max_capacityL.setLocation(xShift+652,12);
		max_capacityL.setSize(76,25);
		max_capacityL.setText("Max Capacity");
		f.getContentPane().add(max_capacityL);

		connect.setLocation(xShift+737,37);
		connect.setSize(78,25);
		f.getContentPane().add(connect);
		
		createGraph.setLocation(xShift+824,37);
		createGraph.setSize(78,25);
		f.getContentPane().add(createGraph);
		
		draw.setLocation(xShift+824,7);
		draw.setSize(78,25);
		f.getContentPane().add(draw);
		
		save.setLocation(xShift+911,37);
		save.setSize(78,25);
		f.getContentPane().add(save);
		
		simulate.setLocation(xShift+911,7);
		simulate.setSize(78,25);
		f.getContentPane().add(simulate);
		
		loadXmi.setLocation(xShift+998,37);
		loadXmi.setSize(78,25);
		f.getContentPane().add(loadXmi);
		
		loadBoard.setLocation(xShift+998,7);
		loadBoard.setSize(78,25);
		f.getContentPane().add(loadBoard);
		
		time.setLocation(xShift+1085,37);
		time.setSize(78,25);
		f.getContentPane().add(time);
		
		priceResultL.setLocation(xShift+1172,11);
		priceResultL.setSize(200,25);
		f.getContentPane().add(priceResultL);
		
		timeL.setLocation(xShift+1085,11);
		timeL.setSize(78,25);
		f.getContentPane().add(timeL);
		
		optimize.setLocation(xShift+1172,37);
		optimize.setSize(78,25);
		f.getContentPane().add(optimize);
		
		loadEpc.setLocation(xShift+1259,37);
		loadEpc.setSize(78,25);
		f.getContentPane().add(loadEpc);
		
		loadAux.setLocation(xShift+1259,7);
		loadAux.setSize(78,25);
		f.getContentPane().add(loadAux);
		
		start.setLocation(xShift+1346,37);
		start.setSize(78,25);
		f.getContentPane().add(start);

		reactanceL.setLocation(xShift+567,11);
		reactanceL.setSize(66,25);
		f.getContentPane().add(reactanceL);

		remove.setLocation(21,940);
		remove.setSize(88,25);
		f.getContentPane().add(remove);

		remove_con.setLocation(503,940);
		remove_con.setSize(88,25);
		remove_con.setPreferredSize(new Dimension(88,25));
		f.getContentPane().add(remove_con);
		
		result.setLocation(19,971);
		result.setSize(800,25);
		f.getContentPane().add(result);
		
		JLabel emptyLabel = new JLabel(); 
		f.getContentPane().add(emptyLabel);

		f.setTitle("Network Creator");
			    	    
	    //Listeners
	    add.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.add(name, combo_element, power, preis, speed, max_capacity);	    		
	    	}  
	    });   
	    
	    connect.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.connect(source, sink, name, reactance, max_capacity);
	    	}  
	    });  
	    
	    
	    toggleActiv.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
	    		controller.toggleActiv(connections, source, sink);
	    		
	    		
	    	}  
	    });  
	    
	    simulate.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.simulateButton(result);
	    	}  
	    }); 
	    
	    draw.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.drawButton(result, graphviz,f);
			    }
				
			});
	    
	    createGraph.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		
	    		controller.createGraph(time);
	    	}  
	    	
	    });  
	    
	    save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.save(result);
			    }
				
			});
	    loadBoard.addActionListener(new ActionListener() {
            //= {"Wind", "Sun", "Coal", "Nuclear", "Gas","City","Fabric","Town","Bus","Hub"};
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.loadBoard();
    			
				}
		}); 
	    
	   
	    
	    loadXmi.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.loadXmi(time, result);
			    }
		      	
			    
	    		//synergyDemoRules = (SynergyDemoRules) eMoflonEMFUtil.loadModel("../SynergyDemo3/instances/first_instance.xmi");
	    		//SynergyDemoRules synergyDemoRules = new SynergyDemoRules(new_sd);
	    		//synergyDemoRules.analyseGraph(new_sd);
    			

			     });
	    
	    loadAux.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				controller.loadAux(time);
				
			}
	    });
	   
	    loadEpc.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				controller.loadEpc(result,connections, time);
				
			}
	    });

	    
	    remove.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.remove(source,sink);
	    	}  
	    }); 
	    
	    time.addActionListener(new ActionListener(){  
	    	
	    	public void actionPerformed(ActionEvent e){
	    		
	    		controller.time(time);
	    		
	    	}  
	    }); 
	    
	    
	    optimize.addActionListener(new ActionListener(){  
			@Override
			public void actionPerformed(ActionEvent e) {
				
				controller.optimize();
			}
	    });
	    
	    start.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.start(time, loadXmi);
	    		
	    		
	    		
	    	}  
	    });
	    
	    remove_con.addActionListener(new ActionListener(){  
	    	public void actionPerformed(ActionEvent e){
	    		controller.removeConnection(connections);
	    	}  
	    });

	    
	    f.pack();	    
	    //f.setSize(1070, 1040);
	    
	    f.setExtendedState(f.getExtendedState() | JFrame.MAXIMIZED_BOTH);
	    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    f.setMinimumSize(new Dimension( Toolkit.getDefaultToolkit().getScreenSize().width,100));
	    f.setVisible(true);	        
	}
	
	
	
	
	
	
	@Override
	public void keyTyped(KeyEvent e) {
		
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		//System.out.println(e.getKeyChar());
		switch(e.getKeyCode()) {
		case KeyEvent.VK_P:
			combo_element.setSelectedIndex(0);
			power.requestFocus();
			break;
		case KeyEvent.VK_C:
			combo_element.setSelectedIndex(1);
			power.requestFocus();
			max_capacity.setText("100000");
			break;
		case KeyEvent.VK_T:
			combo_element.setSelectedIndex(2);
			power.requestFocus();
			break;
		case KeyEvent.VK_H:
			combo_element.setSelectedIndex(3);
			power.requestFocus();
			break;
		case KeyEvent.VK_ESCAPE:
			source.clearSelection();
			sink.clearSelection();
			connections.clearSelection();
			break;
		case KeyEvent.VK_A:
			add.doClick();
			break;
		case KeyEvent.VK_SHIFT:
			toggleActiv.doClick();
			break;
		case KeyEvent.VK_ENTER:
			System.out.println("-------------------------");
			for(int i=0; i<controller.elements.getSize(); i++) {
				System.out.println(controller.elements.get(i));
			}
			for(int i=0; i<controller.connectionList.getSize(); i++) {
				System.out.println(controller.connectionList.get(i));
			}
			System.out.println("-------------------------");
			break;
		default:
			break;
		}
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}  
	
	

}
